<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMicroRegiaoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('microRegiao', function (Blueprint $table) {
            $table->foreign('regiao_id', 'fk_microRegiao_regiao')
                ->references('id')
                ->on('regiao')->onUpdate('NO ACTION')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('microRegiao', function (Blueprint $table) {
            $table->dropForeign('fk_microRegiao_regiao');
        });
    }

}
