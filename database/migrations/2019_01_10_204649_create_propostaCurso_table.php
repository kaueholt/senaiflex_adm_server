<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropostaCursoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propostaCurso', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('proposta_id')->index('fk_propostaCurso_proposta1_idx');
            $table->integer('curso_id')->index('fk_propostaCurso_curso1_idx');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('propostaCurso');
    }

}