<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCoordenadorRegiaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('coordenadorRegiao', function(Blueprint $table)
		{
			$table->foreign('coordenador_pessoa_id', 'fk_coordenador2')->references('pessoa_id')->on('coordenador')->onUpdate('NO ACTION')->onDelete('RESTRICT');
			$table->foreign('regiao_id', 'fk_regiao1')->references('id')->on('regiao')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('coordenadorRegiao', function(Blueprint $table)
		{
			$table->dropForeign('fk_coordenador2');
			$table->dropForeign('fk_regiao1');
		});
	}

}
