<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVideoDelimitadorTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videoDelimitador', function (Blueprint $table) {
            $table->integer('id', true);
            $table->date('dataVigenciaInicial');
            $table->date('dataVigenciaFinal')->nullable();
            $table->integer('quantMinimaDelimitadoraVideo')->nullable();
            $table->integer('quantMaximaDelimitadoraVideo')->nullable();
            $table->integer('cargaHorariaDelimitador')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videoDelimitador');
    }

}