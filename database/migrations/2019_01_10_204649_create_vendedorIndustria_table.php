<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVendedorIndustriaTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendedorIndustria', function (Blueprint $table) {
            $table->integer('vendedor_pessoa_id')->index('fk_vendedor1_idx');
            $table->integer('industria_pessoa_id')->index('fk_industria1_idx');
            $table->date('dataVigenciaIncial');
            $table->date('dataVigenciaFinal')->nullable();
            $table->primary(['vendedor_pessoa_id', 'industria_pessoa_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vendedorIndustria');
    }

}