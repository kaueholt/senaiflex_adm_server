<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVendedorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vendedor', function(Blueprint $table)
		{
			$table->foreign('pessoa_id', 'fk_vendedor_pessoa1')->references('id')->on('pessoa')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vendedor', function(Blueprint $table)
		{
			$table->dropForeign('fk_vendedor_pessoa1');
		});
	}

}
