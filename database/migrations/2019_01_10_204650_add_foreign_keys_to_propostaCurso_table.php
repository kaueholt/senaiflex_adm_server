<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPropostaCursoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('propostaCurso', function(Blueprint $table)
		{
			$table->foreign('curso_id', 'fk_propostaCurso_curso1')->references('id')->on('curso')->onUpdate('NO ACTION')->onDelete('RESTRICT');
			$table->foreign('proposta_id', 'fk_propostaCurso_proposta1')->references('id')->on('proposta')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('propostaCurso', function(Blueprint $table)
		{
			$table->dropForeign('fk_propostaCurso_curso1');
			$table->dropForeign('fk_propostaCurso_proposta1');
		});
	}

}
