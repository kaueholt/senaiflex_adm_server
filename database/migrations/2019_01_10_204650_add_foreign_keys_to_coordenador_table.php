<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCoordenadorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('coordenador', function(Blueprint $table)
		{
			$table->foreign('pessoa_id', 'fk_coordenador_pessoa1')->references('id')->on('pessoa')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('coordenador', function(Blueprint $table)
		{
			$table->dropForeign('fk_coordenador_pessoa1');
		});
	}

}
