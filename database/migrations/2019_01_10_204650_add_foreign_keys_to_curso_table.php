<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCursoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('curso', function(Blueprint $table)
		{
			$table->foreign('area_id', 'fk_curso_area1')->references('id')->on('area')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('curso', function(Blueprint $table)
		{
			$table->dropForeign('fk_curso_area1');
		});
	}

}
