<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoordenadorRegiaoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coordenadorRegiao', function (Blueprint $table) {
            $table->integer('coordenador_pessoa_id')->index('fk_coordenador2_idx');
            $table->integer('regiao_id')->index('fk_regiao1_idx');
            $table->date('dataVigenciaInicial');
            $table->date('dataVigenciaFinal')->nullable();
            $table->primary(['coordenador_pessoa_id', 'regiao_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coordenadorRegiao');
    }

}
