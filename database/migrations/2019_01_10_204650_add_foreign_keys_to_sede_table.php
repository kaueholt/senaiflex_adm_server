<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSedeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sede', function(Blueprint $table)
		{
			$table->foreign('cidade_id', 'fk_sede_cidade1')->references('id')->on('cidade')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sede', function(Blueprint $table)
		{
			$table->dropForeign('fk_sede_cidade1');
		});
	}

}
