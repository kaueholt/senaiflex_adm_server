<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVendedorIndustriaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vendedorIndustria', function(Blueprint $table)
		{
			$table->foreign('industria_pessoa_id', 'fk_industria1')->references('pessoa_id')->on('industria')->onUpdate('NO ACTION')->onDelete('RESTRICT');
			$table->foreign('vendedor_pessoa_id', 'fk_vendedor1')->references('pessoa_id')->on('vendedor')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vendedorIndustria', function(Blueprint $table)
		{
			$table->dropForeign('fk_industria1');
			$table->dropForeign('fk_vendedor1');
		});
	}

}
