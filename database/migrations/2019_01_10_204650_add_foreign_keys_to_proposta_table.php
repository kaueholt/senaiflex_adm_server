<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPropostaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('proposta', function(Blueprint $table)
		{
			$table->foreign('industria_pessoa_id', 'fk_proposta_industria1')->references('pessoa_id')->on('industria')->onUpdate('NO ACTION')->onDelete('RESTRICT');
			$table->foreign('valorVideo_id', 'fk_proposta_valorVideo1')->references('id')->on('videoValor')->onUpdate('NO ACTION')->onDelete('RESTRICT');
			$table->foreign('videoMinimoMaximo_id', 'fk_proposta_videoMinimoMaximo1')->references('id')->on('videoMinimoMaximo')->onUpdate('NO ACTION')->onDelete('RESTRICT');
			$table->foreign('videoQuantidadeMaxima_id', 'fk_proposta_videoQuantidadeMaxima1')->references('id')->on('videoQuantidadeMaxima')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('proposta', function(Blueprint $table)
		{
			$table->dropForeign('fk_proposta_industria1');
			$table->dropForeign('fk_proposta_valorVideo1');
			$table->dropForeign('fk_proposta_videoMinimoMaximo1');
			$table->dropForeign('fk_proposta_videoQuantidadeMaxima1');
		});
	}

}
