<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePessoaTable extends Migration
{

    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('pessoa', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('nome', 255);
            $table->string('cnpjCpf',255);
            $table->integer('cep');
            $table->string('logradouro', 255);
            $table->string('numero', 15);
            $table->string('bairro', 50);
            $table->string('complemento', 20)->nullable();
            $table->char('tipoPessoa', 1)->comment('Coordenador
            Vendedor
            Industria
            Colaborador
            ');
            $table->char('viewVideo', 1)->default('N')->comment('N - Nao
            S - Sim');
            $table->string('password', 45)->nullable();
            $table->integer('cidade_id')->index('fk_pessoa_cidade1_idx');
            $table->timestamps();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::drop('pessoa');
    }

}
