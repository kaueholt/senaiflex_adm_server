<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVideoMinimoMaximoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videoMinimoMaximo', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('quantidadeMinimaVideo');
            $table->integer('quantidadeMaximaVideo');
            $table->integer('cargaHoraria');
            $table->date('dataVigenciaInicial');
            $table->date('dataVigenciaFinal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videoMinimoMaximo');
    }

}