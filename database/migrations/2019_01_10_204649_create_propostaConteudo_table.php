<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropostaConteudoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propostaConteudo', function (Blueprint $table) {
            $table->integer('proposta_id')->index('fk_propostaConteudo_proposta1_idx');
            $table->integer('categoriaConteudo_id')->index('fk_propostaConteudo_categoriaConteudo1_idx');
            $table->primary(['proposta_id', 'categoriaConteudo_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('propostaConteudo');
    }

}