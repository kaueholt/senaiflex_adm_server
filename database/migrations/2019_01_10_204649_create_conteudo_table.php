<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConteudoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conteudo', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('nome', 45);
            $table->date('dataVigenciaInicial');
            $table->date('dataVigenciaFinal');
            $table->integer('curso_id')->index('fk_conteudo_curso1_idx');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('conteudo');
    }

}