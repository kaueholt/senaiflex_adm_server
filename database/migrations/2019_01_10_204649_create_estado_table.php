<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estado', function (Blueprint $table) {
            $table->char('uf', 2)->primary();
            $table->string('nome', 20)->nullable();
            $table->integer('pais_id')->index('fk_estado_pais1_idx');
            $table->timestamps();

        });
        // executar o seeder, populando a tabela de estados
        Artisan::call('db:seed', array('--class' => 'ufSeeder'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('estado');
    }

}