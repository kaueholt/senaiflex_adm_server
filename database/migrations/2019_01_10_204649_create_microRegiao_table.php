<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMicroRegiaoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('microRegiao', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('nome', 20)->nullable();
            $table->integer('regiao_id')->index('fk_microRegiao_regiao_idx');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('microRegiao');
    }

}
