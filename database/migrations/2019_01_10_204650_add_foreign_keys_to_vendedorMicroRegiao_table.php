<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVendedorMicroRegiaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vendedorMicroRegiao', function(Blueprint $table)
		{
			$table->foreign('microRegiao_id', 'fk_vendedorMicroRegiao_microRegiao1')->references('id')->on('microRegiao')->onUpdate('NO ACTION')->onDelete('RESTRICT');
			$table->foreign('vendedor_pessoa_id', 'fk_vendedorMicroRegiao_vendedor1')->references('pessoa_id')->on('vendedor')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vendedorMicroRegiao', function(Blueprint $table)
		{
			$table->dropForeign('fk_vendedorMicroRegiao_microRegiao1');
			$table->dropForeign('fk_vendedorMicroRegiao_vendedor1');
		});
	}

}
