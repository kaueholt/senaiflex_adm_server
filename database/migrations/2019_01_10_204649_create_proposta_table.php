<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropostaTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposta', function (Blueprint $table) {
            $table->integer('id', true);
            $table->char('status', 1);
            $table->integer('numeroProfissionais');
            $table->integer('localCurso')->comment('Unidade SENAI Indústria Pareria Local');
            $table->string('necessidade');
            $table->char('insereVideo', 1)->default('S')->comment('Sim Não');
            $table->integer('totalCargaHoraria');
            $table->integer('totalQuantidadeVideos')->nullable()->default(0);
            $table->integer('totalValorVideos')->nullable()->default(0);
            $table->float('totalValorAulas', 10, 0)->nullable()->default(0);
            $table->integer('valorVideo_id')->index('fk_proposta_valorVideo1_idx');
            $table->integer('videoMinimoMaximo_id')->index('fk_proposta_videoMinimoMaximo1_idx');
            $table->integer('videoQuantidadeMaxima_id')->index('fk_proposta_videoQuantidadeMaxima1_idx');
            $table->integer('industria_pessoa_id')->index('fk_proposta_industria1_idx');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proposta');
    }

}
