<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConteudoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('conteudo', function(Blueprint $table)
		{
			$table->foreign('curso_id', 'fk_conteudo_curso1')->references('id')->on('curso')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('conteudo', function(Blueprint $table)
		{
			$table->dropForeign('fk_conteudo_curso1');
		});
	}

}
