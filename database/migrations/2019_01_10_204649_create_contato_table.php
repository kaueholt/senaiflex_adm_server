<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContatoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('nome', 45);
            $table->string('fone', 12);
            $table->string('email')->nullable();
            $table->integer('pessoa_id')->index('fk_contato_pessoa1_idx');
            $table->char('pricipalContato', 1)->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contato');
    }

}