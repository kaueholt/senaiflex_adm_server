<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCategoriaConteudoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('categoriaConteudo', function(Blueprint $table)
		{
			$table->foreign('categoria_id', 'fk_categoria_has_conteudo_categoria1')->references('id')->on('categoria')->onUpdate('NO ACTION')->onDelete('RESTRICT');
			$table->foreign('conteudo_id', 'fk_categoria_has_conteudo_conteudo1')->references('id')->on('conteudo')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('categoriaConteudo', function(Blueprint $table)
		{
			$table->dropForeign('fk_categoria_has_conteudo_categoria1');
			$table->dropForeign('fk_categoria_has_conteudo_conteudo1');
		});
	}

}
