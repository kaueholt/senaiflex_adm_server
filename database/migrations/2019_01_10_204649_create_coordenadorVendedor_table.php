<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoordenadorVendedorTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coordenadorVendedor', function (Blueprint $table) {
            $table->date('dataVigenciaInicial');
            $table->date('dataVigenciaFinal')->nullable();
            $table->integer('coord_id')->index('fk_coord_idx');
            $table->integer('vendedor_id')->index('fk_vendedor1_idx');
            $table->primary(['coord_id', 'vendedor_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coordenadorVendedor');
    }

}