<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriaConteudoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoriaConteudo', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('categoria_id')->index('fk_categoria_has_conteudo_categoria1_idx');
            $table->integer('conteudo_id')->unique('conteudo_id_UNIQUE');
            $table->date('dataVigenciaInicial');
            $table->date('dataVidenciaFinal')->nullable();
            $table->integer('cargaHoraria');
            $table->float('valorHora', 10, 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categoriaConteudo');
    }

}