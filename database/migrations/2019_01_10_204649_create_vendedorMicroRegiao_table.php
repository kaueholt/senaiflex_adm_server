<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVendedorMicroRegiaoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendedorMicroRegiao', function (Blueprint $table) {
            $table->integer('microRegiao_id');
            $table->integer('vendedor_pessoa_id')->index('fk_vendedorMicroRegiao_vendedor1_idx');
            $table->primary(['microRegiao_id', 'vendedor_pessoa_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vendedorMicroRegiao');
    }

}