<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropostaConteudoAvulsoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propostaConteudoAvulso', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('conhecimento');
            $table->text('justificativa', 65535);
            $table->integer('cargaHoraria');
            $table->integer('proposta_id')->index('fk_propostaConteudoAvulso_proposta1_idx');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('propostaConteudoAvulso');
    }

}