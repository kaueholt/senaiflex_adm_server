<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaisTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pais', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nome', 45);
            $table->string('reduzido', 2);
            $table->integer('codigo');
            $table->timestamps();
        });
        // Inserir dados padrões no banco
        Artisan::call('db:seed', array('--class' => 'PaisSeeder'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pais');
    }

}