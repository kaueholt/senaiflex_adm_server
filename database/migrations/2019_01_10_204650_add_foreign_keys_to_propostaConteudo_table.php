<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPropostaConteudoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('propostaConteudo', function(Blueprint $table)
		{
			$table->foreign('categoriaConteudo_id', 'fk_propostaConteudo_categoriaConteudo1')->references('id')->on('categoriaConteudo')->onUpdate('NO ACTION')->onDelete('RESTRICT');
			$table->foreign('proposta_id', 'fk_propostaConteudo_proposta1')->references('id')->on('proposta')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('propostaConteudo', function(Blueprint $table)
		{
			$table->dropForeign('fk_propostaConteudo_categoriaConteudo1');
			$table->dropForeign('fk_propostaConteudo_proposta1');
		});
	}

}
