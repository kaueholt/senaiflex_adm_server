<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEstadoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('estado', function(Blueprint $table)
		{
			$table->foreign('pais_id', 'fk_estado_pais1')->references('id')->on('pais')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('estado', function(Blueprint $table)
		{
			$table->dropForeign('fk_estado_pais1');
		});
	}

}
