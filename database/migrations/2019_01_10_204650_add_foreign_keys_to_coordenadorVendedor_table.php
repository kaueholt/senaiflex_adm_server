<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCoordenadorVendedorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('coordenadorVendedor', function(Blueprint $table)
		{
			$table->foreign('coord_id', 'fk_cv_coordenador1')->references('pessoa_id')->on('coordenador')->onUpdate('NO ACTION')->onDelete('RESTRICT');
			$table->foreign('vendedor_id', 'fk_cv_vendedor1')->references('pessoa_id')->on('vendedor')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('coordenadorVendedor', function(Blueprint $table)
		{
			$table->dropForeign('fk_cv_coordenador1');
			$table->dropForeign('fk_cv_vendedor1');
		});
	}

}
