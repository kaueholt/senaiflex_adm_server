<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIndustriaTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('industria', function (Blueprint $table) {
            $table->char('status', 1);
            $table->string('nomeFantasia', 25);
            $table->integer('numeroFuncionarios');
            $table->integer('pessoa_id')->primary();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('industria');
    }

}