<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIndustriaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('industria', function(Blueprint $table)
		{
			$table->foreign('pessoa_id', 'fk_industria_pessoa1')->references('id')->on('pessoa')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('industria', function(Blueprint $table)
		{
			$table->dropForeign('fk_industria_pessoa1');
		});
	}

}
