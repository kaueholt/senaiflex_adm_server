<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPessoaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pessoa', function(Blueprint $table)
		{
			$table->foreign('cidade_id', 'fk_pessoa_cidade1')->references('id')->on('cidade')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pessoa', function(Blueprint $table)
		{
			$table->dropForeign('fk_pessoa_cidade1');
		});
	}

}
