<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRegiaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('regiao', function(Blueprint $table)
		{
			$table->foreign('sede_id', 'fk_regiao_sede1')->references('id')->on('sede')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('regiao', function(Blueprint $table)
		{
			$table->dropForeign('fk_regiao_sede1');
		});
	}

}
