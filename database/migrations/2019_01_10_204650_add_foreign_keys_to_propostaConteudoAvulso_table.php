<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPropostaConteudoAvulsoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('propostaConteudoAvulso', function(Blueprint $table)
		{
			$table->foreign('proposta_id', 'fk_propostaConteudoAvulso_proposta1')->references('id')->on('proposta')->onUpdate('NO ACTION')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('propostaConteudoAvulso', function(Blueprint $table)
		{
			$table->dropForeign('fk_propostaConteudoAvulso_proposta1');
		});
	}

}
