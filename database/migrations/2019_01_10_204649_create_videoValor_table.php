<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVideoValorTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videoValor', function (Blueprint $table) {
            $table->integer('id', true);
            $table->float('valorVideo', 10, 0);
            $table->date('dataVigenciaInicial');
            $table->date('dataVigenciaFinal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videoValor');
    }

}