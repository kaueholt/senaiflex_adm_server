<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCidadeTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cidade', function (Blueprint $table) {
            $table->foreign('estado_uf', 'fk_cidade_estado1')
                ->references('uf')
                ->on('estado')
                ->onUpdate('NO ACTION')->onDelete('RESTRICT');
            // $table->foreign('microRegiao_id', 'fk_cidade_microRegiao1')
            //     ->references('id')
            //     ->on('microRegiao')
            //     ->onUpdate('NO ACTION')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cidade', function (Blueprint $table) {
            $table->dropForeign('fk_cidade_estado1');
            // $table->dropForeign('fk_cidade_microRegiao1');
        });
    }

}