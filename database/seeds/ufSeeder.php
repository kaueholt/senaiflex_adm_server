<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ufSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('estado')->insert(
            [
                0 => array(
                    'nome' => 'Acre',
                    'uf' => 'AC',
                    'pais_id' => '55',
                ),
                1 => array(
                    'nome' => 'Alagoas',
                    'uf' => 'AL',
                    'pais_id' => '55',
                ),
                2 => array(
                    'nome' => 'Amazonas',
                    'uf' => 'AM',
                    'pais_id' => '55',
                ),
                3 => array(
                    'nome' => 'Amapá',
                    'uf' => 'AP',
                    'pais_id' => '55',
                ),
                4 => array(
                    'nome' => 'Bahia',
                    'uf' => 'BA',
                    'pais_id' => '55',
                ),
                5 => array(
                    'nome' => 'Ceará',
                    'uf' => 'CE',
                    'pais_id' => '55',
                ),
                6 => array(
                    'uf' => 'DF',
                    'nome' => 'Dristrito Federal',
                    'pais_id' => '55',
                ),
                7 => array(
                    'nome' => 'Espírito Santo',
                    'uf' => 'ES',
                    'pais_id' => '55',
                ),
                8 => array(
                    'nome' => 'Goiás',
                    'uf' => 'GO',
                    'pais_id' => '55',
                ),
                9 => array(
                    'nome' => 'Maranhão',
                    'uf' => 'MA',
                    'pais_id' => '55',
                ),
                10 => array(
                    'nome' => 'Minas Gerais',
                    'uf' => 'MG',
                    'pais_id' => '55',
                ),
                11 => array(
                    'nome' => 'Mato Grosso do Sul',
                    'uf' => 'MS',
                    'pais_id' => '55',
                ),
                12 => array(
                    'nome' => 'Mato Grosso',
                    'uf' => 'MT',
                    'pais_id' => '55',
                ),
                13 => array(
                    'nome' => 'Pará',
                    'uf' => 'PA',
                    'pais_id' => '55',
                ),
                14 => array(
                    'nome' => 'Paraiba',
                    'uf' => 'PB',
                    'pais_id' => '55',
                ),
                15 => array(
                    'nome' => 'Pernambuco',
                    'uf' => 'PE',
                    'pais_id' => '55',
                ),
                16 => array(
                    'nome' => 'Piauí',
                    'uf' => 'PI',
                    'pais_id' => '55',
                ),
                17 => array(
                    'nome' => 'Paraná',
                    'uf' => 'PR',
                    'pais_id' => '55',
                ),
                18 => array(
                    'nome' => 'Rio de Janeiro',
                    'uf' => 'RJ',
                    'pais_id' => '55',
                ),
                19 => array(
                    'nome' => 'Rio Grande do Norte',
                    'uf' => 'RN',
                    'pais_id' => '55',
                ),
                20 => array(
                    'nome' => 'Rondônia',
                    'uf' => 'RO',
                    'pais_id' => '55',
                ),
                21 => array(
                    'nome' => 'Roraima',
                    'uf' => 'RR',
                    'pais_id' => '55',
                ),
                22 => array(
                    'nome' => 'Rio Grande do Sul',
                    'uf' => 'RS',
                    'pais_id' => '55',
                ),
                23 => array(
                    'nome' => 'Santa Catarina',
                    'uf' => 'SC',
                    'pais_id' => '55',
                ),
                24 => array(
                    'nome' => 'Sergipe',
                    'uf' => 'SE',
                    'pais_id' => '55',
                ),
                25 => array(
                    'nome' => 'São Paulo',
                    'uf' => 'SP',
                    'pais_id' => '55',
                ),
                26 => array(
                    'nome' => 'Tocantins',
                    'uf' => 'TO',
                    'pais_id' => '55',
                ),
            ]
        );
    }
}
