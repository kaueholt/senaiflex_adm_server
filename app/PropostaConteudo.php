<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropostaConteudo extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'propostaConteudo';
    protected $fillable = ['id', 'proposta_id', 'categoriaConteudo_id']; // Liberação para gravação

}