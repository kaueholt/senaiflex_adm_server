<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoDelimitador extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'videoDelimitador';
    protected $fillable = ['id', 'dataVigenciaInicial', 'dataVigenciaFinal', 'quantidadeMinimaDelimitadoraVideo', 'quantidadeMaximaDelimitadoraVideo', 'cargaHorariaDelimitador']; // Liberação para gravação

}