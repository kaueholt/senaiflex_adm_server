<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoQuantidadeMaxima extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'videoQuantidadeMaxima';
    protected $fillable = ['id', 'quantidadeMinimaVideo', 'dataVigenciaInicial', 'dataVigenciaFinal']; // Liberação para gravação

}