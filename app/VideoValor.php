<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoValor extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'videoValor';
    protected $fillable = ['id', 'valorVideo', 'dataVigenciaInicial', 'dataVigenciaFinal']; // Liberação para gravação

}