<?php

namespace App\Http\Controllers;

use App\Coordenador;
use Illuminate\Http\Request;

class CoordenadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Coordenador::select('pessoa.*')
            ->join('pessoa', 'pessoa.id', '=', 'coordenador.pessoa_id')
            ->where('pessoa.tipoPessoa', '2') //tipoPessoa2 é vendedor
            ->get();
    }

    public function index_2()
    {
        return Coordenador::orderBy('pessoa.nome')
            ->join('pessoa', 'pessoa.id', '=', 'coordenador.pessoa_id')
            ->where('pessoa.tipoPessoa', '3')
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Coordenador::create($request->all());
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Coordenador  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Coordenador::where('id', $id)->first();
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coordenador  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Coordenador::where('id', $id)
            ->update($request->all());
    }
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Coordenador::where('id', $id)->delete();
    }
}
