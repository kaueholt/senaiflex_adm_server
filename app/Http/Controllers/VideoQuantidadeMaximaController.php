<?php

namespace App\Http\Controllers;

use App\VideoQuantidadeMaxima;
use Illuminate\Http\Request;

class VideoQuantidadeMaximaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return VideoQuantidadeMaxima::orderBy('quantidadeMaximaVideo')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return VideoQuantidadeMaxima::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $VideoQuantidadeMaxima
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return VideoQuantidadeMaxima::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VideoQuantidadeMaxima  $VideoQuantidadeMaxima
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return VideoQuantidadeMaxima::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return VideoQuantidadeMaxima::where('id', $id)->delete();
    }
}