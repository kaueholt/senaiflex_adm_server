<?php

namespace App\Http\Controllers;

use App\Sede;
use App\Regiao;
use Illuminate\Http\Request;
use App\Http\Controllers\Utils\Handles;

class SedeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = [];
        // $headQuarters = Sede::all();
        $headQuarters = Sede::orderby('nome','asc')->get();

        foreach ($headQuarters as $headQuarter) {
            $city = new CidadeController();
            $cityData = $city->show($headQuarter->cidade_id);
            $headQuarter['cidade'] = $cityData == null ? ['err' => 'notFound'] : $cityData;
            $response[] = $headQuarter;
        }

        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Sede::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sede  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Sede::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sede  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payload = $request->all();
        unset($payload['cidade']);

        return Sede::where('id', $id)
            ->update($payload);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return !Regiao::where('sede_id',$id)->first() ? Sede::where('id', $id)->delete() : Handles::jsonResponse('true', 'error', 'Esta sede possui regiões vinculadas.', 'Erro de chave externa.', 400);
    }
}