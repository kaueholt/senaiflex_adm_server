<?php

namespace App\Http\Controllers;

// Models
use App\Http\Controllers\MailRulesController;
use App\Http\Controllers\Utils\Handles;
// Libs
use App\Cidade;
use App\Contato;
use App\Coordenador;
use App\CoordenadorVendedor;
use App\Industria;
use App\Pessoa;
use App\Vendedor;
use App\VendedorIndustria;
// Controllers
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PessoaAdminController extends Controller
{
    private $Handles;
    public function __construct()
    {
        $this->Handles = new Handles(\App\Pessoa::class);
        $this->Mailer = new MailRulesController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $params = $request->all();
        $cidade =  array_key_exists('cidade', $params) ? $params['cidade'] : '';
        $name =  array_key_exists('name', $params) ? $params['name'] : '';
        $vendedor =  array_key_exists('vendedor', $params) ? $params['vendedor'] : '';
        $coordenador =  array_key_exists('coordenador', $params) ? $params['coordenador'] : '';
        $filtroVendedoresAtivo =  array_key_exists('filtroVendedoresAtivo', $params) ? $params['filtroVendedoresAtivo'] : '';
        $filtroCoordenadoresAtivo =  array_key_exists('filtroCoordenadoresAtivo', $params) ? $params['filtroCoordenadoresAtivo'] : '';
        $query = Pessoa::orderBy('nome', 'asc');
            strlen($vendedor) ? $query->leftjoin('vendedor as v', 'v.pessoa_id', '=','pessoa.id')->leftjoin('industria as i', 'i.pessoa_id', '=','pessoa.id')->leftjoin('vendedorIndustria as vi', 'vi.industria_pessoa_id', '=', 'i.pessoa_id') : null;
            strlen($coordenador) ? $query->leftjoin('coordenador as c','c.pessoa_id','=','pessoa.id')->leftjoin('industria as ic','ic.pessoa_id','=','pessoa.id')->leftjoin('vendedorIndustria as vic','vic.industria_pessoa_id','=','ic.pessoa_id')->leftjoin('coordenadorVendedor as cv','cv.vendedor_id','=','vic.vendedor_pessoa_id') : null;
            $query->where("pessoa.nome",'like', "%" . $name ."%");
            strlen($filtroVendedoresAtivo) ? $query->where("pessoa.tipoPessoa",2) : null;            
            strlen($filtroCoordenadoresAtivo) ? $query->where("pessoa.tipoPessoa",3) : null;            
            strlen($cidade) ? $query->where('cidade_id', $cidade) : null;
            strlen($vendedor) ? strlen($coordenador) ? $query->where('cv.coord_id', $coordenador)->where('vi.vendedor_pessoa_id', $vendedor)->orwhere('pessoa.id',$vendedor) : $query->where('vi.vendedor_pessoa_id', $vendedor)->orwhere('pessoa.id',$vendedor) : null;
            strlen($coordenador) && !strlen($vendedor) ? $query->where('cv.coord_id', $coordenador)->orwhere('pessoa.id',$coordenador) : null;
        return $query->paginate();
    }

    public function indexUnpaginated(){
        // ini_set('memory_limit', '-1');
        return Pessoa::all();
        // $sql = "select * from pessoa";
        // return DB::select($sql);
        // return Pessoa::where('id','<',120000)->get();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function storeAdmin(Request $request)
    {
        $industryResponse = '';
        $vendorResponse = '';
        $coordenadorVendedorResponse = '';
        $coordenadorResponse = '';
        $vendedorIndustriaResponse = '';
        try {
            $payload = $request->all();
            $coordenadorVendedor = $payload['coordenadorVendedor']; // Save coordenadorVendedor to later
            $vendedorIndustria = $payload['vendedorIndustria']; // Save coordenadorVendedor to later
            $industry = $payload['industry']; // Save industry to later
            unset($payload['industry']); // remove industry from person array
            $idContato = isset($payload['contact']['id']) ? $payload['contact']['id'] : 0;
            $cidade = $payload['cidade']; // Save cidade to later
            $contact = $payload['contact']; // Save contact to later
            unset($payload['contact']); // remove contact from person array
            
            $vendor = $payload['vendor']; // Save  to later
            unset($payload['vendor']); // remove  from  array

            $coordenador = $payload['coordenador']; // Save  to later
            unset($payload['coordenador']); // remove  from  array

            // verifica se já existe cidade cadastrada
            $cityResponse = Cidade::where(isset($payload['person']['cidade_id']) ? 'id' : 'nome', isset($payload['person']['cidade_id']) ? $payload['person']['cidade_id'] : $cidade)->first();
            if (empty($cityResponse)) {
                $city['nome'] = $cidade;                
                $city['estado_uf'] = $payload['estado_UF'];
                // $city['microRegiao_uf'] = 0;
                $cityResponse = Cidade::create($city); // Store person into database
            }            
            $payload['person']['cidade_id'] = $cityResponse['id'];
            $payload['person']['password'] = isset($payload['person']['password']) ? $payload['person']['password'] : '123';

            unset($payload['person']['estado_uf']); // remove uf from person array


            // verifica se já existe email cadastrado
            $contactResponse = Contato::where('email', $contact['email'])
                ->where('principalContato', 'S')->first();
            $contatoFormExatamenteIgualContatoBD = false;
            if(!empty($contactResponse)){
                $sameName = isset($contactResponse['nome']) ? $contactResponse['nome'] == $contact['nome'] : false;
                $sameFone = isset($contactResponse['fone']) ? $contactResponse['fone'] == $contact['fone'] : false;
                $sameEmail = isset($contactResponse['email']) ? $contactResponse['email'] == $contact['email'] : false;
                $contatoFormExatamenteIgualContatoBD = ($sameName && $sameFone && $sameEmail);
                ($contatoFormExatamenteIgualContatoBD && !$idContato && isset($contactResponse['id'])) ? $idContato = $contactResponse['id'] : null;
            }
            if (empty($contactResponse) || $idContato) {
                $idPessoa = isset($payload['person']['id']) ? $payload['person']['id'] : 0;                
                unset($payload['person']['id']);
                $person = $payload['person']; // Person data withou contact and industry
                $personResponse = $idPessoa ? Pessoa::whereId($idPessoa)->update($person) : Pessoa::create($person); // Store person into database
                // $personResponse = Pessoa::create($person); // Store person into database
                $idPessoa = isset($personResponse->id) ? $personResponse->id : false;
                if ( $idPessoa ) {
                    $contact['principalContato'] = 'S';
                    $contact['pessoa_id'] = $idPessoa;
                    $industry['pessoa_id'] = $idPessoa;                    
                    $coordenadorVendedor['vendedor_id'] = $idPessoa;
                    $vendedorIndustria['industria_pessoa_id'] = $idPessoa;
                    $coordenadorVendedor['dataVigenciaInicial'] = $vendor['dataVigenciaInicial'];
                    $vendedorIndustria['dataVigenciaIncial'] = $vendor['dataVigenciaInicial'] ? $vendor['dataVigenciaInicial'] : date("Y-m-d");
                    // Insert contact
                    $contactResponse = $idContato ? Contato::whereId($idContato)->update($contact) : Contato::create($contact);

                    // 1->industria; 
                    // 2->vendedor; 
                    // 3->coordenador;
                    $tipoPessoa = $person['tipoPessoa'] ? $person['tipoPessoa'] : $personResponse ? $personResponse->tipoPessoa ? $personResponse->tipoPessoa : '404' : '404';
                    switch ($tipoPessoa) {
                        case '1':                            
                            $industryResponse = Industria::create($industry);
                            $vendedorIndustria['vendedor_pessoa_id'] ? $vendedorIndustriaResponse = VendedorIndustria::create($vendedorIndustria) : null;
                            break;
                        case '2':
                            $vendor['pessoa_id'] = $idPessoa;
                            $vendorResponse = Vendedor::create($vendor);
                            $coordenadorVendedorResponse = CoordenadorVendedor::create($coordenadorVendedor);
                            break;
                        case '3':
                            $coordenador['pessoa_id'] = $idPessoa;
                            $coordenadorResponse = Coordenador::create($coordenador);
                            break;
                        case '404':
                            throw new \Exception('Tipo pessoa inválido -> ' .  $person['tipoPessoa']);
                            break;
                    }                
                }        
                // Save response data
                $response = array(
                    'PersonResponse' => $personResponse,
                    'ContactResponse' => $contactResponse,
                    'CoordenadorResponse' => $coordenadorResponse,
                    'IndustryResponse' => $industryResponse,
                    'VendorResponse' => $vendorResponse,
                    'VendedorIndustriaResponse' => $vendedorIndustriaResponse,
                    'coordenadorVendedorResponse' => $coordenadorVendedorResponse,
                );
                // send email to user
                // $statusEmail = $this->Mailer->sendMailRegister($contact['email'], $personResponse->id); // Sugestão: Criar um middleware que fique responsável pelo envio. Assim não dá erro se o e-mail não for enviado
                return Handles::jsonResponse('false', 'sucess', 'Cadastro realizado com sucesso!', $response);
            }else{
                throw new \Exception('E-mail já cadastrado em nossa base!');
                
                //return Handles::jsonResponse('true', 'error', 'E-mail já cadastrado em nossa base!', '');
            }
        } catch (Exception $e) {
            print_r($e);
            // tratar erro de envio de e-mail
            //print_r('passou erro');
            return  response()->json(
                array(
                    'code' => 60204,
                    'message' => 'Pessoa não cadastrada.'));
        }
    }
    public function updateAdmin(Request $request)
    {
        $industryResponse = '';
        $vendorResponse = '';
        $coordenadorVendedorResponse = '';
        $coordenadorResponse = '';
        $vendedorIndustriaResponse = '';
        try {
            $payload = $request->all();
            //print_r($payload);
            // var_dump($payload);
            $coordenadorVendedor = $payload['coordenadorVendedor']; // Save coordenadorVendedor to later
            $vendedorIndustria = $payload['vendedorIndustria']; // Save coordenadorVendedor to later
            $industry = $payload['industry']; // Save industry to later
            unset($payload['industry']); // remove industry from person array
            $idContato = isset($payload['contact']['id']) ? $payload['contact']['id'] : 0;
            unset($payload['contact']['id']); // remove from array
            $contact = $payload['contact']; // Save contac to later
            unset($payload['contact']); // remove from array
            
            $cidade = $payload['cidade']; // Save cidade to later
            $vendor = $payload['vendor']; // Save  to later
            unset($payload['vendor']); // remove  from  array

            $coordenador = $payload['coordenador']; // Save  to later
            unset($payload['coordenador']); // remove  from  array

            // verifica se já existe cidade cadastrada
            $cityResponse = Cidade::where(isset($payload['person']['cidade_id']) ? 'id' : 'nome', isset($payload['person']['cidade_id']) ? $payload['person']['cidade_id'] : $cidade)->first();
            if (empty($cityResponse)) {
                $city['nome'] = $cidade;                
                $city['estado_uf'] = $payload['estado_UF'];
                // $city['microRegiao_uf'] = 0;
                $cityResponse = Cidade::create($city); // Store person into database
            }            
            $payload['person']['cidade_id'] = $cityResponse['id'];
            
            unset($payload['person']['estado_uf']); // remove uf from person array
            
            $idPessoa = isset($payload['person']['id']) ? $payload['person']['id'] : 0;                
            unset($payload['person']['id']);
            $senhaFormulario = isset($payload['person']['password']) ? $payload['person']['password'] : null;
            !$senhaFormulario ? $pessoa = Pessoa::whereId($idPessoa)->first() : null;
            $senhaPessoa = $senhaFormulario ? $senhaFormulario : $pessoa['password'];

            $payload['person']['password'] = $senhaPessoa ? $senhaPessoa : '123';
            $person = $payload['person']; // Person data withou contact and industry
            $personResponse = Pessoa::whereId($idPessoa)->update($person);
            // $personResponse = Pessoa::create($person); // Store person into database
            if ( $personResponse ) {
                $contact['principalContato'] = 'S';
                $contact['pessoa_id'] = $idPessoa;                
                $industry['pessoa_id'] = $idPessoa;                    
                $coordenadorVendedor['vendedor_id'] = $idPessoa;
                $vendedorIndustria['industria_pessoa_id'] = $idPessoa;
                $coordenadorVendedor['dataVigenciaInicial'] = $vendor['dataVigenciaInicial'];
                $vendedorIndustria['dataVigenciaIncial'] = $vendor['dataVigenciaInicial'] ? $vendor['dataVigenciaInicial'] : date("Y-m-d");
                // Insert contact
                $contactResponse = Contato::whereId($idContato)->update($contact);
                $tipoPessoa = $person['tipoPessoa'] ? $person['tipoPessoa'] : '404';
                switch ($tipoPessoa) {
                    case '1':                            
                        $industryResponse = Industria::where('pessoa_id',$idPessoa)->first();
                        $industryResponse = $industryResponse ? Industria::where('pessoa_id',$idPessoa)->update($industry) : Industria::create($industry);
                        if($vendedorIndustria['vendedor_pessoa_id']){}
                            $vendedorIndustriaResponse = VendedorIndustria::where('industria_pessoa_id',$idPessoa)->where('vendedor_pessoa_id',$vendedorIndustria['vendedor_pessoa_id'])->first();
                            $vendedorIndustriaResponse = $vendedorIndustriaResponse ? 0 : VendedorIndustria::create($vendedorIndustria);
                        break;
                    case '2':
                        $vendor['pessoa_id'] = $idPessoa;
                        $vendorResponse = Vendedor::where('pessoa_id',$idPessoa)->first();
                        $vendorResponse = $vendorResponse ? Vendedor::where('pessoa_id',$idPessoa)->update($vendor) : Vendedor::create($vendor);
                        $coordenadorVendedorResponse = CoordenadorVendedor::where('vendedor_id',$idPessoa)->where('coord_id',$coordenadorVendedor['coord_id'])->first();
                        $coordenadorVendedorResponse = $coordenadorVendedorResponse ? 0 : CoordenadorVendedor::create($coordenadorVendedor);
                        break;
                    case '3':
                        $coordenador['pessoa_id'] = $idPessoa;
                        $coordenadorResponse = Coordenador::where('pessoa_id',$idPessoa)->first();
                        $coordenadorResponse = $coordenadorResponse ? Coordenador::where('pessoa_id',$idPessoa)->update($coordenador) : Coordenador::create($coordenador);
                        break;
                    case '404':
                        throw new \Exception('Tipo pessoa inválido -> ' .  $person['tipoPessoa']);
                        break;
                }                    
            }        
            // Save response data
            $response = array(
                'PersonResponse' => $personResponse,
                'ContactResponse' => $contactResponse,
                'CoordenadorResponse' => $coordenadorResponse,
                'IndustryResponse' => $industryResponse,
                'VendorResponse' => $vendorResponse,
                'VendedorIndustriaResponse' => $vendedorIndustriaResponse,
                'coordenadorVendedorResponse' => $coordenadorVendedorResponse,
            );
            // send email to user
            // $statusEmail = $this->Mailer->sendMailRegister($contact['email'], $personResponse->id); // Sugestão: Criar um middleware que fique responsável pelo envio. Assim não dá erro se o e-mail não for enviado
            return Handles::jsonResponse('false', 'sucess', 'Cadastro realizado com sucesso!', $response);
           
        } catch (Exception $e) {
            print_r($e);
            // tratar erro de envio de e-mail
            //print_r('passou erro');
            return  response()->json(
                array(
                    'code' => 60204,
                    'message' => 'Pessoa não cadastrada.'));
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $payload = $request->all();
            $payload['contact']['principalContato'] = 'S';

            // print_r($payload);
            $industry = $payload['industry']; // Save industry to later
            unset($payload['industry']); // remove industry from person array7

            $contact = $payload['contact']; // Save contac to later
            unset($payload['contact']); // remove industry from person array

            // verifica se já existe email cadastrado
            $contactResponse = Contato::where('email', $contact['email'])
                ->where('principalContato', 'S')->first();

            if (empty($contactResponse)) {
                $person = $payload['person']; // Person data withou contact and industry
                // verifica se existe cnpj
                $personResponse = Pessoa::where('cnpjCpf', $person['cnpjCpf'])->first();
                if (empty($personResponse)) {
                    $personResponse = Pessoa::create($person); // Store person into database
        
                    if ($personResponse->id) {
                        $contact['pessoa_id'] = $personResponse->id;
                        $contact['principalContato'] = 'S';
                        $industry['pessoa_id'] = $personResponse->id;
                        // Insert contact and industry
                        // print_r($contact);
                        $contactResponse = Contato::create($contact);
                        $industryResponse = Industria::create($industry);
                    }
            
                    // Save response data
                    $response = array(
                        'personResponse' => $personResponse,
                        'ContactResponse' => $contactResponse,
                        'IndustryResponse' => $industryResponse,
                    );
                    // send email to user
                    $statusEmail = $this->Mailer->sendMailRegister($contact['email'], $personResponse->id); // Sugestão: Criar um middleware que fique responsável pelo envio. Assim não dá erro se o e-mail não for enviado
                    return Handles::jsonResponse('false', 'sucess', 'Indústria cadastrada com sucesso!', $response);
                }
                else {
                    return Handles::jsonResponse('true', 'error', 
                    'Seu CNPJ já existe em nosso banco de cadastro. Caso não tenha a senha, clique em ESQUECI MINHA SENHA. ', '');
                }
            }
            else {
                     return Handles::jsonResponse('true', 'error', 
                    'Seu E-MAIL já existe em nosso banco de cadastro. Caso não tenha a senha, clique em ESQUECI MINHA SENHA. ', '');
            }


        } catch (Exception $e) {
            print_r($e);
            // tratar erro de envio de e-mail
            return Handles::jsonResponse('true', 'error', 'Cadastro não realizado!', '');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function show(Int $id)
    {

        $pessoa = Pessoa::where('id', $id)->first();
        $contato = Contato::where('pessoa_id', $id)
            ->where('principalContato', 'S')
                ->first();
        $industria = Industria::where('pessoa_id', $id)->first();
        $vendedorIndustria = VendedorIndustria::where('industria_pessoa_id', $id)->orderby('dataVigenciaIncial','desc')->first();
        $vendedor = Vendedor::where('pessoa_id', $id)->first();
        $coordenadorVendedor = CoordenadorVendedor::where('vendedor_id', $id)->orderby('dataVigenciaInicial','desc')->first();
        $coordenador = Coordenador::where('pessoa_id', $id)->first();
        unset($pessoa['password']);
        $response = [
            "person" => $pessoa,
            "contact" => $contato,
            "industry" => $industria,
            "vendedor" => $vendedor,
            "vendedorIndustria" => $vendedorIndustria,
            "coordenadorVendedor" => $coordenadorVendedor,
            "coordenador" => $coordenador,
        ];
        return $this->Handles->handleList($response);

    }
    /**
     * Display the specified person description.
     *
     * @param  Array  $param
     * @return \Illuminate\Http\Response
     */
    public function findBySearch(Request $request)
    {

    $arrSearch = $request->all();
    $nome = $arrSearch['nome'];
    // Busca pela pessoa que tenha parte do nome encontrado na razão social
    $search = Pessoa::select('id', 'nome as descricao')
        ->where("nome", 'like', '%'.$nome.'%')
        ->orderby('nome')
        ->get();

    // retornar somete um resultado
    return $search;
}

    /**
     * Display the specified resource.
     *
     * @param  Int  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function findByEmail(String $email)
    {

        // Busca pela pessoa que tenha id igual a valor da coluna pessoa_id em contato
        $response =  DB::table('contato')
            ->select('pessoa.*', 'contato.email')
            ->join('pessoa', 'contato.pessoa_id', '=', 'pessoa.id')
            ->where('email', $email)
            ->where('contato.principalContato', 'S')
            ->take(1)
            ->get();

        // print_r($response);
        return $response;
    }

/**
 * Display the specified cnpjCpf.
 *
 * @param  Int  $cnpjCpf
 * @return \Illuminate\Http\Response
 */
    public function findByCnpjCpf(String $cnpjCpf)
    {

        // Busca pela pessoa que tenha cnpjCpf igual a valor da por parametro cnpjCpf
        $person = Pessoa::where("cnpjCpf", $cnpjCpf)->first();
        $industry = Industria::where('pessoa_id', $person->id)->first();
        $contact = Contato::where('pessoa_id', $person->id)->first();

        $response = array(
            'person' => $person,
            'contact' => $contact,
            'industry' => $industry,
        );
        // retornar somete um resultado
        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pessoa  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $payload = $request->all();

            $payload['contact']['pessoa_id'] = $id;
            $payload['contact']['principalContato'] = 'S';
            // print_r($payload);

            unset($payload['person']['id']);

            $industry = $payload['industry']; // Save industry to later
            unset($payload['industry']); // remove industry from person array

            $contact = $payload['contact']; // Save contac to later
            unset($payload['contact']); // remove industry from person array

            $person = $payload['person']; // Person data withou contact and industry
            $person['cep'] =  str_replace('-', '', $person['cep']);

            $personResponse = Pessoa::where('id', $id)
                ->update($person); // Store person into database

            $industryResponse = Industria::where('pessoa_id', $id)->update($industry);

            // $contactResponse = Contato::where('pessoa_id', $id)->update($contact);
            

            // verifica se já existe email cadastrado
            // $contactResponse = Contato::where('email', $contact['email'])
            //     ->where('principalContato', 'S')
            //     ->where('pessoa_id', '<>', $id)
            //     ->first();

            // if (empty($contactResponse)) {

            // update contact, if error insert
            $contactResponse = Contato::where('pessoa_id', $id)
            ->where('principalContato', 'S')->first();

            if (empty($contactResponse)) {
                $contactResponse = Contato::create($contact);
            } else {
                $contactResponse->nome = $contact['nome'];
                $contactResponse->fone = $contact['fone'];
                $contactResponse->email = $contact['email'];
                $contactResponse->principalContato = 'S';
                $contactResponse->pessoa_id = $id;
    
                $contactResponse->save();
                // $contactResponse = Contato::where('pessoa_id', $id)->update($contact);
            }

            // }
            // else {
            //     $contactResponse = 0;
            // }

            // Save response data
            $response = array(
                'personResponse' => $personResponse,
                'contactResponse' => $contactResponse,
                'industryResponse' => $industryResponse,
            );

            return Handles::jsonResponse('false', 'sucess', 'Indústria atualizada com sucesso!', $response);
        } catch (Exception $e) {
            print_r($e);
            // tratar erro de envio de e-mail
            return Handles::jsonResponse('true', 'error', 'Cadastro não realizado!', '');
        }
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function removeNewAccount(Int $id)
    {
        $person = array("newAccount" => 'N');
        return Pessoa::where('id', $id)->update($person);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $id)
    {
        return $this->Handles->handleDelete($id);
    }
    public function destroyPaginated(Int $id)
    {
        try {
            Contato::where('pessoa_id', $id)->delete();
            VendedorIndustria::where('industria_pessoa_id', $id)->delete();
            Industria::where('pessoa_id', $id)->delete();
            Pessoa::where('id', $id)->delete();
            // return $this->Handles->handleDelete($id);
            return $query = Pessoa::orderBy('nome', 'asc')->paginate();
        }catch(\Illuminate\Database\QueryException $e){
            return Handles::jsonResponse('true', 'error', 'Esta pessoa possui vínculos.', $e->errorInfo[2], 400);
        }
    }
    
}
