<?php

namespace App\Http\Controllers;

use App\videoDelimitador;
use Illuminate\Http\Request;

class videoDelimitadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return videoDelimitador::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return videoDelimitador::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $videoDelimitador
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return videoDelimitador::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\videoDelimitador  $videoDelimitador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return videoDelimitador::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return videoDelimitador::where('id', $id)->delete();
    }
}