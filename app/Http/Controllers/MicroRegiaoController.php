<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CidadeController;
use App\MicroRegiao;
use Illuminate\Http\Request;

class MicroRegiaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = [];
        // $microRegions = MicroRegiao::all();
        $microRegions = MicroRegiao::select('nome','regiao_id','id')->orderby('nome','asc')->get();
        foreach ($microRegions as $microRegion) {
            $region = new RegiaoController();
            // Buscar a cidade/ Região pelo id da cidade
            $regionData = $region->show($microRegion->regiao_id);
            // Adiciona os dados da cidade e região dentro do array
            $microRegion['regiao'] =  $regionData == null ? ['err' => 'notFound'] : $regionData;
            // adiciona a $response a microregião com os dados da cidade e região buscadas
            $response[] = $microRegion;
        }
        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        return MicroRegiao::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MicroRegiao  $microRegion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return MicroRegiao::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MicroRegiao  $microRegion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $payload = $request->all();
        // Remove os campos cidade e região pois eles não são intâncias de microregiao
        unset($payload['regiao']);

        return MicroRegiao::where('id', $id)
            ->update($payload);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return MicroRegiao::where('id', $id)->delete();
    }
}
