<?php

namespace App\Http\Controllers;

use App\Industria;
use Illuminate\Http\Request;
use App\Pessoa;

class IndustriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Industria::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Industria::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Industria  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Industria::where('id', $id)->first();
    }

    /**
     * Display the specified industry name.
     *
     * @param  Array  $param
     * @return \Illuminate\Http\Response
     */
    public function findBySearch(Request $request)
    {

        $arrSearch = $request->all();
        $nome = $arrSearch['nome'];
        // Busca pela pessoa que tenha parte do nome encontrado na razão social
        $person = Pessoa::select('id', 'nome as descricao')
            ->leftjoin('industria', 'industria.pessoa_id', '=', 'pessoa.id')
            ->where("nome", 'like', '%'.$nome.'%')
            ->orderby('nome')
            ->get();
        // retornar somete um resultado
        return $person;
    }

    public function findBySearch_2(Request $request){
        $arrSearch = $request->all();
        $nome = $arrSearch['nome'];
        // Busca pela industria que tenha parte do nome encontrado no nome fantasia
        $person = Industria::select('pessoa_id', 'nomeFantasia')
            ->where("nomeFantasia", 'like', '%'.$nome.'%')
            ->orderby('nomeFantasia')
            ->limit(10)
            ->get();
        // retornar somente um resultado
        return $person;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Industria  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Industria::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Industria::where('id', $id)->delete();
    }
}
