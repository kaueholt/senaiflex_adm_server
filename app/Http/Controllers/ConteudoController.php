<?php

namespace App\Http\Controllers;

use App\Conteudo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Utils\Handles;

class ConteudoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $params = $request->all();
        $nome =  array_key_exists('nome', $params) ? $params['nome'] : '';
        // return Conteudo::select('dbo.conteudo.id','dbo.conteudo.nome','dbo.conteudo.dataVigenciaInicial','dbo.conteudo.dataVigenciaFinal','dbo.conteudo.curso_id','dbo.conteudo.created_at','dbo.conteudo.updated_at','dbo.conteudo.idImport','dbo.area.id as area_id','dbo.area.descricao as area_descricao','dbo.area.status as area_status','dbo.curso.id as curso_id','dbo.curso.descricao as curso_descricao','dbo.curso.dataVigenciaInicial as curso_dataVigenciaInicial','dbo.curso.dateVigenciaFinal as curso_dateVigenciaFinal','dbo.curso.area_id as curso_area_id','dbo.curso.created_at as curso_created_at','dbo.curso.updated_at as curso_updated_at')
        return Conteudo::select('dbo.conteudo.id','dbo.conteudo.nome as nome','dbo.conteudo.dataVigenciaInicial','dbo.conteudo.dataVigenciaFinal','dbo.area.descricao as area_descricao','dbo.curso.descricao as curso_descricao','dbo.curso.dataVigenciaInicial as curso_dataVigenciaInicial','dbo.curso.dateVigenciaFinal as curso_dateVigenciaFinal','dbo.curso.area_id as curso_area_id','dbo.curso.created_at as curso_created_at','dbo.curso.updated_at as curso_updated_at')
            ->leftJoin('dbo.curso','dbo.conteudo.curso_id','dbo.curso.id')
            ->leftJoin('dbo.area','dbo.curso.area_id','dbo.area.id')
            ->where("dbo.conteudo.nome", 'like', $nome.'%')
            ->orderBy('dbo.conteudo.nome')
            ->paginate();
        // return Conteudo::orderBy('nome')
        //     ->where("nome", 'like', $nome.'%')
        //     ->paginate();

    }
    public function indexUnpaginated(){
        return Conteudo::select('dbo.conteudo.id','dbo.conteudo.nome','dbo.conteudo.dataVigenciaInicial','dbo.conteudo.dataVigenciaFinal','dbo.conteudo.curso_id','dbo.conteudo.created_at','dbo.conteudo.updated_at','dbo.conteudo.idImport','dbo.area.id as area_id','dbo.area.descricao as area_descricao','dbo.area.status as area_status','dbo.curso.id as curso_id','dbo.curso.descricao as curso_descricao','dbo.curso.dataVigenciaInicial as curso_dataVigenciaInicial','dbo.curso.dateVigenciaFinal as curso_dateVigenciaFinal','dbo.curso.area_id as curso_area_id','dbo.curso.created_at as curso_created_at','dbo.curso.updated_at as curso_updated_at')
            ->leftJoin('dbo.curso','dbo.conteudo.curso_id','dbo.curso.id')
            ->leftJoin('dbo.area','dbo.curso.area_id','dbo.area.id')
            ->orderBy('dbo.conteudo.nome')
            ->get();
    }
        

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Conteudo::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $Conteudo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Conteudo::where('id', $id)->first();
    }
    /**
     * Display the specified resource description.
     *
     * @param  Array  $param
     * @return \Illuminate\Http\Response
     */
    public function findBySearch(Request $request)
    {

    $arrSearch = $request->all();
    $nome = $arrSearch['nome'];
    // Busca pela pessoa que tenha parte do nome encontrado na razão social
    $search = Conteudo::select('id', 'nome as descricao')
        ->where("nome", 'like', '%'.$nome.'%')
        ->orderby('nome')
        ->get();

    // retornar somete um resultado
    return $search;
}

    /**
     * Display the specified resource.
     *
     * @param  Int  $Curso
     * @return \Illuminate\Http\Response
     */
    public function getByCurso($cursoId)
    {
        // return Conteudo::where('curso_id', $cursoId)->get();
        $curso = explode(',', $cursoId);

        $query = DB::select(DB::raw(
            "SELECT c.id, c.nome, c.dataVigenciaInicial, c.dataVigenciaFinal, c.curso_id,
                (select concat(t.id,'|', g.descricao,'|',t.valorHora, '|', t.cargaHoraria)
                from categoriaConteudo t
                inner join categoria g on g.id=t.categoria_id and t.categoria_id=1
                where t.conteudo_id=c.id) as categoria1,
                (select concat(t.id,'|', g.descricao,'|',t.valorHora, '|', t.cargaHoraria)
                from categoriaConteudo t
                inner join categoria g on g.id=t.categoria_id and t.categoria_id=2
                where t.conteudo_id=c.id) as categoria2,
                (select concat(t.id,'|', g.descricao,'|',t.valorHora, '|', t.cargaHoraria)
                from categoriaConteudo t
                inner join categoria g on g.id=t.categoria_id and t.categoria_id=3
                where t.conteudo_id=c.id) as categoria3
                FROM conteudo c
                WHERE curso_id = ${curso[0]} or curso_id = ${curso[1]} or curso_id = ${curso[2]} order by c.nome"
        ));
        return $query;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Conteudo  $Conteudo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Conteudo::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Conteudo::where('id', $id)->delete();
    }

    public function destroyPaginated($id)
    {
        try {
            Conteudo::where('id', $id)->delete();
            return Conteudo::orderBy('nome')->paginate();
        }catch(\Illuminate\Database\QueryException $e){
            return Handles::jsonResponse('true', 'error', 'Este conteúdo possui categorias vinculadas.', $e->errorInfo[2], 400);
            // return response()->json(
            //     array(
            //         'err' => true,
            //         'type' => 'error',
            //         'message' => 'teste',
            //         'data' => $e->data
            //     )
            // );
        }
    }
}