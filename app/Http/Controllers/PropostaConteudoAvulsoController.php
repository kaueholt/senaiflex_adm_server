<?php

namespace App\Http\Controllers;

use App\PropostaConteudoAvulso;
use Illuminate\Http\Request;

class PropostaConteudoAvulsoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PropostaConteudoAvulso::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return PropostaConteudoAvulso::create($request->all());
        return PropostaConteudoAvulso::insert($request->all());        
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $PropostaConteudoAvulso
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return PropostaConteudoAvulso::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PropostaConteudoAvulso  $PropostaConteudoAvulso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return PropostaConteudoAvulso::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return PropostaConteudoAvulso::where('id', $id)->delete();
    }
}