<?php

namespace App\Http\Controllers;

use App\Mail\ContactUs;
use App\Mail\OrderRequested;
use App\Mail\RegisterCompleted;
use App\Mail\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailRulesController extends Controller
{
    /**
     * @description Send an email when user is registered
     * @param $userMail
     */
    public function sendMailRegister($userMail, $userId)
    {
        $isMailSended = Mail::to($userMail)
            ->locale('pt-br')
            // ->bcc(['carla.bailer@sistemafiep.org.br', 'daniel.hella@sistemafiep.org.br'])
            ->send(new RegisterCompleted($userId));

        return $isMailSended;
    }

    /**
     * @param $userMail
     * @return mixed
     */
    public function sendMailResetPassword($userMail, $userId)
    {
        return Mail::to($userMail)
            // ->bcc(['carla.bailer@sistemafiep.org.br', 'daniel.hella@sistemafiep.org.br'])
            ->locale('pt-br')
            ->send(new ResetPassword($userId));
    }

    /**
     * @param $userMail
     * @return mixed
     */
    public function sendMailContactUs(Request $request)
    {
        $data = $request->all();

        //  $isMailSended = Mail::to('carla.bailer@sistemafiep.org.br') // Trocar para o e-mail correto
        $isMailSended = Mail::to('carla.bailer@sistemafiep.org.br')
            ->locale('pt-br')
            ->send(new ContactUs($data['name'], $data['message'], $data['cnpj'], $data['emailContact'], $data['telefoneContact'], $data['nameBusiness'], $data['cityBusiness'], $data['telephoneBusiness']));
        return $isMailSended;
    }

    /**
     * @param $userMail
     * @return mixed
     */
    public function sendMailOrderRequested($numOrder, $order, $email)
    {
        // print_r('passou');
        //        $isMailSended = Mail::to('carla.bailer@sistemafiep.org.br') // Trocar para o e-mail correto
        if ($email == ''){
            $email = 'carla.bailer@sistemafiep.org.br';
        }
        $isMailSended = Mail::to($email) // Trocar para o e-mail correto
            ->bcc(['carla.bailer@sistemafiep.org.br'])
            // ->bcc(['daniel.hella@sistemafiep.org.br'])
            ->locale('pt-br')
            ->send(new OrderRequested($numOrder, $order));
        return $isMailSended;
    }

}