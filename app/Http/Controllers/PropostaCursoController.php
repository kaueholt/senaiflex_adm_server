<?php

namespace App\Http\Controllers;

use App\PropostaCurso;
use Illuminate\Http\Request;

class PropostaCursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Responsekkk
     */
    public function index()
    {
        return PropostaCurso::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return PropostaCurso::create($request->all());
//        dd($request);
        return PropostaCurso::insert($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $PropostaCurso
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return PropostaCurso::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PropostaCurso  $PropostaCurso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return PropostaCurso::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return PropostaCurso::where('id', $id)->delete();
    }
}