<?php

namespace App\Http\Controllers;

use App\CategoriaConteudo;
use Illuminate\Http\Request;

class CategoriaConteudoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CategoriaConteudo::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return CategoriaConteudo::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $CategoriaConteudo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return CategoriaConteudo::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoriaConteudo  $CategoriaConteudo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return CategoriaConteudo::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return CategoriaConteudo::where('id', $id)->delete();
    }
}