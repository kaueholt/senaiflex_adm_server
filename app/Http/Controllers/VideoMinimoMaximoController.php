<?php

namespace App\Http\Controllers;

use App\VideoMinimoMaximo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class VideoMinimoMaximoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return VideoMinimoMaximo::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return VideoMinimoMaximo::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $VideoMinimoMaximo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return VideoMinimoMaximo::where('id', $id)->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $VideoMinimoMaximo
     * @return \Illuminate\Http\Response
     */
    public function showByWorload($ch)
    {
        $query = VideoMinimoMaximo::where('cargaHoraria', $ch)->first();
        if ($query == null) {
            $query = DB::select(DB::raw(
                "SELECT id ,quantidadeMinimaVideo, quantidadeMaximaVideo, cargaHoraria, dataVigenciaInicial, dataVigenciaFinal FROM VideoMinimoMaximo
                    where id = (select max(id) from VideoMinimoMaximo)"
            ));
            return Response::json($query[0]);
        }
        return $query;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VideoMinimoMaximo  $VideoMinimoMaximo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return VideoMinimoMaximo::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return VideoMinimoMaximo::where('id', $id)->delete();
    }
}