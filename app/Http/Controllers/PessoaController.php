<?php

namespace App\Http\Controllers;

// Models
use App\Http\Controllers\MailRulesController;
use App\Http\Controllers\Utils\Handles;
// Libs
use App\Pessoa;
use App\Industria;
use App\Contato;
// Controllers
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PessoaController extends Controller
{
    private $Handles;
    public function __construct()
    {
        $this->Handles = new Handles(\App\Pessoa::class);
        $this->Mailer = new MailRulesController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_2(Request $request){
        $params = $request->all();
        $cidade =  array_key_exists('cidade', $params) ? $params['cidade'] : '';
        $name =  array_key_exists('name', $params) ? $params['name'] : '';
        $vendedor =  array_key_exists('vendedor', $params) ? $params['vendedor'] : '';
        $coordenador =  array_key_exists('coordenador', $params) ? $params['coordenador'] : '';        
        $query = Pessoa::orderBy('nome', 'asc');
            $vendedor ? $query->rightjoin('industria as i', 'i.pessoa_id', '=','pessoa.id')->rightjoin('vendedorIndustria as vi', 'vi.industria_pessoa_id', '=', 'i.pessoa_id') : null;
            $coordenador ?  $query->rightjoin('industria as ic','ic.pessoa_id','=','pessoa.id')->rightjoin('vendedorIndustria as vic','vic.industria_pessoa_id','=','ic.pessoa_id')->rightjoin('coordenadorVendedor as cv','cv.vendedor_id','=','vic.vendedor_pessoa_id') : null;
            $vendedor ? $query->where("i.numeroFuncionarios", '0') : null;
            $query->where("pessoa.nome",'like', "%" . $name);
            $cidade ? $query->where('cidade_id', $cidade) : null;
            $vendedor ? $query->where('vi.vendedor_pessoa_id', $vendedor) : null;
            $coordenador ? $query->where('cv.coord_id', $coordenador) : null;
        return $query->paginate();
    }

    public function index(Request $request)
    {

        $params = $request->all();
        $name =  array_key_exists('name', $params) ? $params['name'] : '';
        $vendedor =  array_key_exists('vendedor', $params) ? $params['vendedor'] : '';
        $coordenador =  array_key_exists('coordenador', $params) ? $params['coordenador'] : '';
        if ($vendedor != '') {
            return Pessoa::orderBy('nome')
            ->join('industria', 'industria.pessoa_id', '=', 'pessoa.id')
            ->join('vendedorIndustria', 'vendedorIndustria.industria_pessoa_id', '=', 'industria.pessoa_id')
            ->where("nome", 'like', $name.'%')
            ->where("vendedorIndustria.vendedor_pessoa_id", $vendedor)
            ->paginate();
        }
        else {
            if ($coordenador != '') {
                return Pessoa::orderBy('nome')
                ->join('industria', 'industria.pessoa_id', '=', 'pessoa.id')
                ->join('vendedorIndustria', 'vendedorIndustria.industria_pessoa_id', '=', 'industria.pessoa_id')
                ->join('coordenadorVendedor', 'coordenadorVendedor.vendedor_id', '=', 'vendedorIndustria.vendedor_pessoa_id')
                ->where("nome", 'like', $name.'%')
                ->where("coordenadorVendedor.coord_id", $coordenador)
                ->paginate();
            }
            else {
                return Pessoa::orderBy('nome')
                ->where("nome", 'like', $name.'%')
                ->paginate();
            }
    
        }

    }

    public function storeAdmin(Request $request)
    {
        try {
            $payload = $request->all();

            $industry = $payload['industry']; // Save industry to later
            unset($payload['industry']); // remove industry from person array7

            $contact = $payload['contact']; // Save contac to later
            unset($payload['contact']); // remove industry from person array

            $vendor = $payload['vendor']; // Save contac to later
            unset($payload['vendor']); // remove industry from person array

            $manager = $payload['manager']; // Save contac to later
            unset($payload['manager']); // remove industry from person array

            // verifica se já existe email cadastrado
            $contactResponse = Contato::where('email', $contact['email'])
                ->where('principalContato', 'S')->first();

            if (empty($contactResponse)) {
                $person = $payload['person']; // Person data withou contact and industry
                $personResponse = Pessoa::create($person); // Store person into database
        
                if ($personResponse->id) {
                    $contact['pessoa_id'] = $personResponse->id;
                    $industry['pessoa_id'] = $personResponse->id;
                    $vendor['pessoa_id'] = $personResponse->id;
                    $manager['pessoa_id'] = $personResponse->id;

                    // Insert contact
                    $contactResponse = Contato::create($contact);

                    // 1 - industria 2 - vendedor 3 - coordenador
                    switch ($personResponse->tipoPessoa) {
                        case 1:
                            $industryResponse = Industria::create($industry);
                            break;
                        case 2:
                            $vendorResponse = Vendedor::create($vendor);
                            break;
                        case 3:
                            $managerResponse = Coordenador::create($manager);
                            break;
                    }                
                }
        
                // Save response data
                $response = array(
                    'personResponse' => $personResponse,
                    'ContactResponse' => $contactResponse,
                    'IndustryResponse' => $industryResponse,
                    'VendorResponse' => $vendorResponse,
                    'CoordenadorResponse' => $managerResponse,
                );
                // send email to user
                // $statusEmail = $this->Mailer->sendMailRegister($contact['email'], $personResponse->id); // Sugestão: Criar um middleware que fique responsável pelo envio. Assim não dá erro se o e-mail não for enviado
                return Handles::jsonResponse('true', 'sucess', 'Indústria cadastrada com sucesso!', $response);
            }
            else {
                return Handles::jsonResponse('false', 'error', 'E-mail já cadastrado em nossa base!', '');
            }


        } catch (\Throwable $th) {
            // tratar erro de envio de e-mail
            print_r('passou erro');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $payload = $request->all();
            $payload['contact']['principalContato'] = 'S';

            // print_r($payload);
            $industry = $payload['industry']; // Save industry to later
            unset($payload['industry']); // remove industry from person array7

            $contact = $payload['contact']; // Save contac to later
            unset($payload['contact']); // remove industry from person array

            // verifica se já existe email cadastrado
            $contactResponse = Contato::where('email', $contact['email'])
                ->where('principalContato', 'S')->first();

            if (empty($contactResponse)) {
                $person = $payload['person']; // Person data withou contact and industry
                // verifica se existe cnpj
                $personResponse = Pessoa::where('cnpjCpf', $person['cnpjCpf'])->first();
                if (empty($personResponse)) {
                    $personResponse = Pessoa::create($person); // Store person into database
        
                    if ($personResponse->id) {
                        $contact['pessoa_id'] = $personResponse->id;
                        $contact['principalContato'] = 'S';
                        $industry['pessoa_id'] = $personResponse->id;
                        // Insert contact and industry
                        // print_r($contact);
                        $contactResponse = Contato::create($contact);
                        $industryResponse = Industria::create($industry);
                    }
            
                    // Save response data
                    $response = array(
                        'personResponse' => $personResponse,
                        'ContactResponse' => $contactResponse,
                        'IndustryResponse' => $industryResponse,
                    );
                    // send email to user
                    $statusEmail = $this->Mailer->sendMailRegister($contact['email'], $personResponse->id); // Sugestão: Criar um middleware que fique responsável pelo envio. Assim não dá erro se o e-mail não for enviado
                    return Handles::jsonResponse('false', 'sucess', 'Indústria cadastrada com sucesso!', $response);
                }
                else {
                    return Handles::jsonResponse('true', 'error', 
                    'Seu CNPJ já existe em nosso banco de cadastro. Caso não tenha a senha, clique em ESQUECI MINHA SENHA. ', '');
                }
            }
            else {
                     return Handles::jsonResponse('true', 'error', 
                    'Seu E-MAIL já existe em nosso banco de cadastro. Caso não tenha a senha, clique em ESQUECI MINHA SENHA. ', '');
            }


        } catch (Exception $e) {
            print_r($e);
            // tratar erro de envio de e-mail
            return Handles::jsonResponse('true', 'error', 'Cadastro não realizado!', '');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function show(Int $id)
    {

        $pessoa = Pessoa::where('id', $id)->first();
        $contato = Contato::where('pessoa_id', $id)
            ->where('principalContato', 'S')
                ->first();
        $industria = Industria::where('pessoa_id', $id)->first();
        unset($pessoa['password']);
        $reponse = [
            "person" => $pessoa,
            "contact" => $contato,
            "industry" => $industria,
        ];
        return $this->Handles->handleList($reponse);

    }
    /**
     * Display the specified person description.
     *
     * @param  Array  $param
     * @return \Illuminate\Http\Response
     */
    public function findBySearch(Request $request)
    {

    $arrSearch = $request->all();
    $nome = $arrSearch['nome'];
    // Busca pela pessoa que tenha parte do nome encontrado na razão social
    $search = Pessoa::select('id', 'nome as descricao')
        ->where("nome", 'like', '%'.$nome.'%')
        ->orderby('nome')
        ->get();

    // retornar somete um resultado
    return $search;
}

    /**
     * Display the specified resource.
     *
     * @param  Int  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function findByEmail(String $email)
    {

        // Busca pela pessoa que tenha id igual a valor da coluna pessoa_id em contato
        $response =  DB::table('contato')
            ->select('pessoa.*', 'contato.email')
            ->join('pessoa', 'contato.pessoa_id', '=', 'pessoa.id')
            ->where('email', $email)
            ->where('contato.principalContato', 'S')
            ->take(1)
            ->get();

        // print_r($response);
        return $response;
    }

/**
 * Display the specified cnpjCpf.
 *
 * @param  Int  $cnpjCpf
 * @return \Illuminate\Http\Response
 */
    public function findByCnpjCpf(String $cnpjCpf)
    {

        // Busca pela pessoa que tenha cnpjCpf igual a valor da por parametro cnpjCpf
        $person = Pessoa::where("cnpjCpf", $cnpjCpf)->first();
        $industry = Industria::where('pessoa_id', $person->id)->first();
        $contact = Contato::where('pessoa_id', $person->id)->first();

        $response = array(
            'person' => $person,
            'contact' => $contact,
            'industry' => $industry,
        );
        // retornar somete um resultado
        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pessoa  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $payload = $request->all();

            $payload['contact']['pessoa_id'] = $id;
            $payload['contact']['principalContato'] = 'S';
            // print_r($payload);

            unset($payload['person']['id']);

            $industry = $payload['industry']; // Save industry to later
            unset($payload['industry']); // remove industry from person array

            $contact = $payload['contact']; // Save contac to later
            unset($payload['contact']); // remove industry from person array

            $person = $payload['person']; // Person data withou contact and industry
            $person['cep'] =  str_replace('-', '', $person['cep']);

            $personResponse = Pessoa::where('id', $id)
                ->update($person); // Store person into database

            $industryResponse = Industria::where('pessoa_id', $id)->update($industry);

            // $contactResponse = Contato::where('pessoa_id', $id)->update($contact);
            

            // verifica se já existe email cadastrado
            // $contactResponse = Contato::where('email', $contact['email'])
            //     ->where('principalContato', 'S')
            //     ->where('pessoa_id', '<>', $id)
            //     ->first();

            // if (empty($contactResponse)) {

            // update contact, if error insert
            $contactResponse = Contato::where('pessoa_id', $id)
            ->where('principalContato', 'S')->first();

            if (empty($contactResponse)) {
                $contactResponse = Contato::create($contact);
            } else {
                $contactResponse->nome = $contact['nome'];
                $contactResponse->fone = $contact['fone'];
                $contactResponse->email = $contact['email'];
                $contactResponse->principalContato = 'S';
                $contactResponse->pessoa_id = $id;
    
                $contactResponse->save();
                // $contactResponse = Contato::where('pessoa_id', $id)->update($contact);
            }

            // }
            // else {
            //     $contactResponse = 0;
            // }

            // Save response data
            $response = array(
                'personResponse' => $personResponse,
                'contactResponse' => $contactResponse,
                'industryResponse' => $industryResponse,
            );

            return Handles::jsonResponse('false', 'sucess', 'Indústria atualizada com sucesso!', $response);
        } catch (Exception $e) {
            print_r($e);
            // tratar erro de envio de e-mail
            return Handles::jsonResponse('true', 'error', 'Cadastro não realizado!', '');
        }
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function removeNewAccount(Int $id)
    {
        $person = array("newAccount" => 'N');
        return Pessoa::where('id', $id)->update($person);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $id)
    {
        return $this->Handles->handleDelete($id);
    }
}
