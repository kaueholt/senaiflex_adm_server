<?php

namespace App\Http\Controllers;

use App\Uf;
use App\Cidade;
use Illuminate\Http\Request;
use App\Http\Controllers\Utils\Handles;

class UfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $params = $request->all();
        $id =  array_key_exists('id', $params) ? $params['id'] : '';
        if ($id != '') {
            return Uf::where('id', $id)
            ->first();        }
        else{
            return Uf::all();
        }    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Uf::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $uf
     * @return \Illuminate\Http\Response
     */
    public function show($uf)
    {
        return Uf::where('uf', $uf)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Uf  $uf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uf)
    {
        $payload = $request->all();
        // Remove os campos cidade e região pois eles não são intâncias de microregiao
        unset($payload['pais']);

        return Uf::where('uf', $uf)
            ->update($payload);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return !Cidade::leftjoin('estado','estado.uf','cidade.estado_uf')->where('estado.id',$id)->first() ? Uf::where('id', $id)->delete() : Handles::jsonResponse('true', 'error', 'Este estado possui cidades vinculadas.', 'Erro de chave externa.', 400);
    }
}