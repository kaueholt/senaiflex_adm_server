<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MailRulesController;
use App\Proposta;
use App\PropostaConteudo;
use App\PropostaConteudoAvulso;
use App\PropostaCurso;

// Controllers
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class PropostaAdminController extends Controller
{
    public $Mailer;

    public function __construct()
    {
        $this->Mailer = new MailRulesController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $timeWindow=strtotime("-1 Months");
        $sql = "select DISTINCT p.id, pes.nome, p.contatoNome, RIGHT('0000000000'+CAST(p.totalValorVideos AS VARCHAR(10)),10) as totalValorVideos, RIGHT('0000000000'+CAST(p.totalValorAulas AS VARCHAR(10)),10) as totalValorAulas, p.necessidade, p.created_at ";
            $sql .= 'from proposta as p ';
            $sql .= 'left join pessoa pes on p.industria_pessoa_id = pes.id ';
			$sql .= 'left join propostaCurso t on t.proposta_id = p.id ';
            $sql .= "where p.created_at >= '". date("Y-m-d",$timeWindow) ."' ";
            $sql .= "and p.created_at <= '". date("Y-m-d") . "' ";
            $sql .= " order by p.created_at";
        return DB::select($sql);
        // return DB::table('proposta')
        // ->select('proposta.id', 'pessoa.nome', 'proposta.contatoNome', "RIGHT('0000000000'+CAST(proposta.totalValorVideos AS VARCHAR(10)),10) as totalValorVideos", 'proposta.necessidade', 'proposta.created_at')
        // ->join('pessoa', 'proposta.industria_pessoa_id', '=', 'pessoa.id')
        // ->where('proposta.created_at','>=',date("Y-m-d",$timeWindow))
        // ->where('proposta.created_at','<=',date("Y-m-d"))->get();
    }
    
    // public function index(Request $request){
    //     $param = $request->all();        
        
    //     //var_dump(strpos($decoded,'&'));
    //     $timeWindow=strtotime("-1 Months");
    //     $dataInicial = array_key_exists('dateStart', $param) ? $param['dateStart'] : date("Y-m-d",$timeWindow);
    //     $dataFinal = array_key_exists('dateLast', $param) ? $param['dateLast'] : date("Y-m-d");
    //     //var_dump($param);
    //     // $query = proposta::distinct()orderBy('created_at','asc')
    //     $sql = "select DISTINCT p.id, pes.nome, p.contatoNome, RIGHT('0000000000'+CAST(p.totalValorVideos AS VARCHAR(10)),10) as totalValorVideos, RIGHT('0000000000'+CAST(p.totalValorAulas AS VARCHAR(10)),10) as totalValorAulas, p.necessidade, p.created_at ";
    //     $sql .= 'from  as p ';
    //     // $sql .= 'left join propostaCurso t on t.proposta_id = p.id ';
    //     // $sql .= 'left join curso c on c.id = t.curso_id ';
    //     // $sql .= 'left join area a on a.id = c.area_id ';
    //     $sql .= 'left join pessoa pes on p.industria_pessoa_id = pes.id ';
    //     $sql .= (array_key_exists('vendor', $param)) ? 'left join industria as i on i.pessoa_id = p.industria_pessoa_id left join vendedorIndustria as vi on vi.industria_pessoa_id = i.pessoa_id ' : ''; 
    //     $sql .= (array_key_exists('manager', $param)) ? ' left join industria as ic on ic.pessoa_id = p.industria_pessoa_id left join vendedorIndustria as vic on vic.industria_pessoa_id = ic.pessoa_id left join coordenadorVendedor as cv on cv.vendedor_id = vic.vendedor_pessoa_id ' : '';
    //     //$sql .= "where 1=1 ";
    //     $sql .= "where p.created_at >= '". $dataInicial ."' ";
    //     $sql .= "and p.created_at <= '". $dataFinal . "' ";
    //     $sql .= (array_key_exists('vendor', $param)) ? "and vi.vendedor_pessoa_id = ". $param['vendor'] . " " :'';
    //     $sql .= (array_key_exists('manager', $param)) ? "and cv.coord_id = ". $param['manager'] . " " : '';
    //     if (array_key_exists('industry', $param)) {
    //         $sql .= "and p.industria_pessoa_id = ". $param['industry'] . " ";
    //     }
    //     $sql .= " order by p.created_at";
    //     $response = DB::paginate($sql);
    //     return $response;
    // }

    public function all(){
        return Proposta::all();
    }

    public function indexUnpaginated(){
        return Proposta::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $arrProposta = $request->all();
        //print_r($arrProposta);
        $order = $arrProposta;

        //$response['id'] = 3; // corrigir. To colocando qualquer coisa só pra teste;
        $response['status'] = 'A'; //A - Ativo I - Inativo;
        $response['numeroProfissionais'] = $arrProposta['numProfissionais'];
        $response['localCurso'] = $arrProposta['unidade']['id'];
        $response['necessidade'] = "{$arrProposta['necessidade']}";
        $response['insereVideo'] = $arrProposta['inserirVideoDidatico'] ? 'S' : 'N';
        $response['totalCargaHoraria'] = $arrProposta['cursoCargaHoraria'];
        $response['totalQuantidadeVideos'] = $arrProposta['videoQtdDesejada'];
        $response['totalValorVideos'] = $arrProposta['videoQtdDesejada']*$arrProposta['valorVideo']['ValorVideo'];
        $response['totalValorAulas'] = $arrProposta['price'];
        // Foreign keys
        $response['valorVideo_id'] = $arrProposta['valorVideo']['id'];
        $response['videoMinimoMaximo_id'] = $arrProposta['videoMinimoMaximo_id'];
        $response['videoQuantidadeMaxima_id'] = $arrProposta['videoQuantidadeMaxima_id'];
        $response['industria_pessoa_id'] = $arrProposta['pessoaId'];
        $response['contatoNome'] = $arrProposta['contatoNome'];
        $response['contatoFone'] = $arrProposta['contatoTelefone'];
        $response['contatoEmail'] = $arrProposta['contatoEmail'];
        //$response['created_at'] = date("Y-m-d H:i:s");

        // recebe
        /**
         * numProfissionais
         * unidade
         * necessidade
         * inserirVideoDidatico
         * videoCargaHoraria
         * videoQtdDesejada
         * personId
         *
         * areaTecnologica
         * conteudos
         * cursosBase
         * price
         * videoMaximo
         * videoMinimo
         */

        $propostaResponse = Proposta::create($response);
        // save propostaCurso

        $courseResponse = new PropostaCurso;

        // $courseResponse->proposta_id = $propostaResponse['id'];
        // $courseResponse->curso_id = $arrProposta['cursosBase'][0]['id'];
        for ($i = 0; $i < count($arrProposta['cursosBase']); $i++) {
            $arrCurso[$i]['proposta_id'] = $propostaResponse['id'];
            $arrCurso[$i]['curso_id'] = $arrProposta['cursosBase'][$i]['id'];
            $arrCurso[$i]['created_at'] = date('Y-m-d');
        }

        if (!$courseResponse->insert($arrCurso)) {
            return 'false';
        }

        // save propostaConteudo

        $conteudoResponse = new PropostaConteudo;

        for ($i = 0; $i < count($arrProposta['conteudos']); $i++) {
            $arrConteudo[$i]['proposta_id'] = $propostaResponse['id'];
            $arrConteudo[$i]['categoriaConteudo_id'] = $arrProposta['conteudos'][$i]['cat'];
            $arrConteudo[$i]['created_at'] = date('Y-m-d');
            $order['conteudos'][$i]['precoTotal'] = number_format($arrProposta['conteudos'][$i]['precoTotal'], 2, ',', '.');
        }

        if (!$conteudoResponse->insert($arrConteudo)) {
            return 'false';
        }

        // save propostaConteudoAvulso

        if (array_key_exists('cargaHoraria', $arrProposta)) {
            $conteudoAvulsoResponse = new PropostaConteudoAvulso;

            for ($i = 0; $i < count($arrProposta['cursosSolicitados']); $i++) {
                $arrConteudoAvulso[$i]['proposta_id'] = $propostaResponse['id'];
                $arrConteudoAvulso[$i]['conhecimento'] = $arrProposta['cursosSolicitados'][$i]['conteudo'];
                $arrConteudoAvulso[$i]['justificativa'] = $arrProposta['cursosSolicitados'][$i]['justificativa'];
                $arrConteudoAvulso[$i]['cargaHoraria'] = $arrProposta['cursosSolicitados'][$i]['cargaHoraria'];
                $arrConteudoAvulso[$i]['created_at'] = date('Y-m-d');
            }
   
            if (!$conteudoAvulsoResponse->insert($arrConteudoAvulso)) {
                return 'false';
            }
        }

        // incluir campos.
        // $response['totalCargaHoraria'] = $arrProposta['cursoCargaHoraria'];
        // $response['totalQuantidadeVideos'] = $arrProposta['videoQtdDesejada'];
        // $response['totalValorVideos'] = $arrProposta['videoQtdDesejada']*$arrProposta['valorVideo']['ValorVideo'];
        // $response['totalValorAulas'] = $arrProposta['price'];

        $order['totalValorVideos'] =  number_format($arrProposta['videoQtdDesejada']*$arrProposta['valorVideo']['ValorVideo'], 2, ',', '.');
        $order['totalValorAulas'] = number_format($arrProposta['price'], 2, ',', '.');
        $order['videoQtdDesejada'] = $arrProposta['videoQtdDesejada'];
        $order['cursoCargaHoraria'] = $arrProposta['cursoCargaHoraria'].'h';
        $order['totalCargaHoraria'] = ceil($arrProposta['cursoCargaHoraria']).'h';
        $order['extra'] = (ceil($arrProposta['cursoCargaHoraria']) > $arrProposta['cursoCargaHoraria']) ? '1' : '';
        $order['price'] = number_format($arrProposta['price'], 2, ',', '.');
        $order['totalGeral'] = number_format($arrProposta['price'] + ($arrProposta['videoQtdDesejada']*$arrProposta['valorVideo']['ValorVideo']), 2, ',', '.');
        //print_r($order);
        // send email to user


        // busca de email vendedor
        $responseVendedor = DB::table('proposta')
            ->select('proposta.id', 'vendedorIndustria.industria_pessoa_id', 'contato.email')
            ->leftjoin('vendedorIndustria', 'vendedorIndustria.industria_pessoa_id', '=', 'proposta.industria_pessoa_id')
            ->leftjoin('contato', 'contato.pessoa_id', '=', 'vendedorIndustria.vendedor_pessoa_id')
            ->where('proposta.id', $propostaResponse['id'])
            ->first();

        //$arrVendedor = json_decode($responseVendedor);
        // print_r($propostaResponse['id'].'Passou Vendedor==>>'.$responseVendedor->contatoEmail);

        // foreach ($responseVendedor as $contact)
        // {
        //     print_r('email==>'.$contact->email);
        // }        
        // busca de email coordenador se vendedor não estiver relacionado com cliente.
        if ($responseVendedor->email){
            $email = $responseVendedor->email;
        }
         else 
         {
            $responseCoordenador = DB::table('proposta')
                ->select('proposta.id', 'coordenadorRegiao.coordenador_pessoa_id', 'contato.email')
                ->leftjoin('pessoa', 'pessoa.id', '=', 'proposta.industria_pessoa_id')
                ->leftjoin('cidade', 'cidade.id', '=', 'pessoa.cidade_id')
                ->leftjoin('regiao', 'regiao.id', '=', 'cidade.regiao_id')
                ->leftjoin('coordenadorRegiao', 'coordenadorRegiao.regiao_id', '=', 'regiao.id')
                ->leftjoin('contato', 'contato.pessoa_id', '=', 'coordenadorRegiao.coordenador_pessoa_id')
                ->where('proposta.id', $propostaResponse['id'])
                ->first();
            $email = $responseCoordenador->email;
        }
        $this->Mailer->sendMailOrderRequested($propostaResponse['id'], $order, $email);
        // return "Resultado e-mail:{$response}";

        return $propostaResponse;

    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $Proposta
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Proposta::where('id', $id)->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $Proposta
     * @return \Illuminate\Http\Response
     */
    public function showByIndustria($id)
    {
        //return Proposta::where('industria_pessoa_id', $id)->get();
        $response = DB::table('proposta')
            ->select('proposta.id', 'area.descricao', 'proposta.created_at')
            ->leftjoin('propostaCurso', 'propostaCurso.proposta_id', '=', 'proposta.id')
            ->leftjoin('curso', 'curso.id', '=', 'propostaCurso.curso_id')
            ->leftjoin('area', 'area.id', '=', 'curso.area_id')
            ->where('industria_pessoa_id', $id)
            ->orderby('proposta.created_at', 'DESC')
            ->distinct()
            ->get();

        return $response;

    }


    /**
     * Display the specified resource.
     *
     * @param  Date $dateStart
     * @param  Date $dateLast
     * @param  Int  $industry
     * @param  Int  $vendor
     * @param  Int  $manager
     * @return \Illuminate\Http\Response
     */
	 public function showByParam(Request $request)
    {
        $param = $request->all();        
        $page = array_key_exists('page', $param) ? $param['page'] : 1;
        //var_dump(strpos($decoded,'&'));
        $timeWindow=strtotime("-1 Months");
        $dataInicial = array_key_exists('dateStart', $param) ? $param['dateStart'] : date("Y-m-d",$timeWindow);
        $dataFinal = array_key_exists('dateLast', $param) ? $param['dateLast'] : date("Y-m-d");
        //var_dump($param);
        $sql = "select DISTINCT p.id, pes.nome, p.contatoNome, RIGHT('0000000000'+CAST(p.totalValorVideos AS VARCHAR(10)),10) as totalValorVideos, RIGHT('0000000000'+CAST(p.totalValorAulas AS VARCHAR(10)),10) as totalValorAulas, p.necessidade, p.created_at ";
        $sql .= 'from proposta as p ';
        // $sql .= 'left join propostaCurso t on t.proposta_id = p.id ';
        // $sql .= 'left join curso c on c.id = t.curso_id ';
        // $sql .= 'left join area a on a.id = c.area_id ';
        $sql .= 'left join pessoa pes on p.industria_pessoa_id = pes.id ';
        $sql .= (array_key_exists('vendor', $param)) ? 'left join industria as i on i.pessoa_id = p.industria_pessoa_id left join vendedorIndustria as vi on vi.industria_pessoa_id = i.pessoa_id ' : ''; 
        $sql .= (array_key_exists('manager', $param)) ? ' left join industria as ic on ic.pessoa_id = p.industria_pessoa_id left join vendedorIndustria as vic on vic.industria_pessoa_id = ic.pessoa_id left join coordenadorVendedor as cv on cv.vendedor_id = vic.vendedor_pessoa_id ' : '';
        //$sql .= "where 1=1 ";
        $sql .= "where p.created_at >= '". $dataInicial ."' ";
        $sql .= "and p.created_at <= '". $dataFinal . "' ";
        $sql .= (array_key_exists('vendor', $param)) ? "and vi.vendedor_pessoa_id = ". $param['vendor'] . " " :'';
        $sql .= (array_key_exists('manager', $param)) ? "and cv.coord_id = ". $param['manager'] . " " : '';
        if (array_key_exists('industry', $param)) {
            $sql .= "and p.industria_pessoa_id = ". $param['industry'] . " ";
        }
        $sql .= " order by p.created_at";
        $colecao = collect(DB::select($sql));
        $total = $colecao->count();
        $itemsPorPagina = 6;
        $response = collect(DB::select($sql))->forPage($page,$itemsPorPagina);
        return response()->json(
            array(
                'code' => 200,
                'total' => $total,
                'to' => ((((($page-1)*$itemsPorPagina) + $itemsPorPagina) > $total) ? $total : ((($page-1)*$itemsPorPagina) + $itemsPorPagina)),
                'from' => ((((($page-1) * $itemsPorPagina) == 0 ) && ($total > 0)) ? 1 : (($page-1) * $itemsPorPagina)),
                'per_page' => $itemsPorPagina,
                'lastPage' => ceil($total/$itemsPorPagina),
                'page' => $page,
                'data' => $response));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Proposta  $Proposta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Proposta::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Proposta::where('id', $id)->delete();
    }

    /**
     * converte e formata minutos em horas
     * @param minutes
     */
    public function minutesToTime($minutes) {
        $h = floor($minutes / 60);
        $m = $minutes % 60;

        if ($minutes < 60) {
            $time = $minutes.' min';
        }else {
            if ($m === 0) {
                $time = $h.'h';
            } else {
                $time = $h.'h'.$m;
            }
        }
        return $time;
      }

    /**
     * Gera um arquivo pdf para a proposta equivalente ao id recebido
     * @param propostId
     *
     */
    public function exportToPDF($propostId)
    {
        
        /**
         * Buscar os dados da proposta, utilizando o id recebido via url
         */
        // $order = $this->showByIndustria(2);

        // $siteUrl = getenv('SITE_URL');
        $responseProposta = DB::table('proposta')
            ->select('proposta.*', 'videoValor.valorVideo', 'pessoa.cnpjCpf', 'pessoa.nome', 'cidade.nome as cidade')
            ->join('pessoa', 'pessoa.id', '=', 'proposta.industria_pessoa_id')
            ->join('cidade', 'cidade.id', '=', 'pessoa.cidade_id')
            ->join('videoValor', 'videoValor.id', '=', 'proposta.valorVideo_id')
            ->where('proposta.id', $propostId)
            ->first();
        // print_r($responseProposta);


        $order['status'] = 'valor de teste';
        $order['numProfissionais'] = $responseProposta->numeroProfissionais;
        $order['localCurso'] = 'valor de teste';
        $order['necessidade'] = $responseProposta->necessidade ? $responseProposta->necessidade : '-';
        $order['insereVideo'] = 'valor de teste';
        $order['totalQuantidadeVideos'] = $responseProposta->totalQuantidadeVideos;
        $order['totalValorVideos'] =  number_format($responseProposta->totalValorVideos, 2, ',', '.');
        $order['totalValorAulas'] = number_format($responseProposta->totalValorAulas, 2, ',', '.');
        $order['videoQtdDesejada'] = $responseProposta->totalQuantidadeVideos;
        $order['cursoCargaHoraria'] =  $this->minutesToTime($responseProposta->totalCargaHoraria * 60);
        $order['totalCargaHoraria'] = ceil($responseProposta->totalCargaHoraria).'h';
        $order['extra'] = (ceil($responseProposta->totalCargaHoraria) > $responseProposta->totalCargaHoraria) ? '1' : '';
        $order['price'] = number_format($responseProposta->totalValorAulas, 2, ',', '.');
        $order['totalGeral'] = number_format($responseProposta->totalValorAulas + $responseProposta->totalValorVideos, 2, ',', '.');
        $order['contatoNome'] = $responseProposta->contatoNome;
        $order['contatoFone'] = $responseProposta->contatoFone;
        $order['contatoEmail'] = $responseProposta->contatoEmail;


        $order['user']['cnpj'] = $responseProposta->cnpjCpf;
        $order['user']['name'] = $responseProposta->nome;
        $order['user']['city'] = $responseProposta->cidade;
        // print_r($order);

        $responseArea = DB::table('proposta')
            ->select('area.descricao as descarea','curso.*')
            ->leftjoin('propostaCurso', 'propostaCurso.proposta_id', '=', 'proposta.id')
            ->leftjoin('curso', 'curso.id', '=', 'propostaCurso.curso_id')
            ->leftjoin('area', 'area.id', '=', 'curso.area_id')
            ->where('proposta.id', $propostId)
            ->orderby('proposta.created_at', 'DESC')
            ->first();
        // print_r($responseArea);

        // $i = 0;
        // foreach ($responseArea as $curso) {
        //     $order['areaTecnologica']['descricao'] = $curso->descarea;

        //     $order['cursosBase'][$i]['descricao'] = $curso->descricao;
        //     $i++;
        // }

        $order['areaTecnologica']['descricao'] = $responseArea->descarea;

        // busca curso
        $responsePropostaCurso = DB::table('propostaCurso')
            ->select('curso.descricao')
            ->leftjoin('curso', 'propostaCurso.curso_id', '=', 'curso.id')
            ->where('propostaCurso.proposta_id', $propostId)
            ->get();
        // print_r($responsePropostaCurso);
        $i = 0;
        foreach ($responsePropostaCurso as $curso) {
            $order['cursosBase'][$i]['descricao'] = $curso->descricao;
            $i++;
        }

        // busca conteudo
        $responsePropostaConteudo = DB::table('propostaConteudo')
            ->select('conteudo.nome', 'categoriaConteudo.cargaHoraria', 'categoriaConteudo.valorHora')
            ->join('categoriaConteudo', 'categoriaConteudo.id', '=', 'propostaConteudo.categoriaConteudo_id')
            ->join('conteudo', 'conteudo.id', '=', 'categoriaConteudo.conteudo_id')
            ->where('propostaConteudo.proposta_id', $propostId)
            ->get();
        $i = 0;
        // print_r($responsePropostaConteudo);
        foreach ($responsePropostaConteudo as $conteudo) {
            $order['conteudos'][$i]['nome'] = $conteudo->nome;
            $order['conteudos'][$i]['duracaoFormatado'] = $this->minutesToTime($conteudo->cargaHoraria);
            $order['conteudos'][$i]['valor'] = number_format($conteudo->valorHora, 2, ',', '.');
            $i++;
        }
        // print_r($order);


        // // busca conteudo avulso
        $responsePropostaConteudoAvulso = DB::table('propostaConteudoAvulso')
            ->select('propostaConteudoAvulso.conhecimento', 'propostaConteudoAvulso.cargaHoraria', 'propostaConteudoAvulso.justificativa')
            ->where('propostaConteudoAvulso.proposta_id', $propostId)
            ->get();
        // print_r($responsePropostaConteudoAvulso);
        $i = 0;
        foreach ($responsePropostaConteudoAvulso as $avulso) {
            $order['cursosSolicitados'][$i]['conteudo'] = $avulso->conhecimento;
            $order['cursosSolicitados'][$i]['justificativa'] = $avulso->justificativa;
            $order['cursosSolicitados'][$i]['cargaHoraria'] = $avulso->cargaHoraria;
            $i++;
        }
        // print_r($order);
        // *******
        // { id: 1, nome: 'Unidade Senai' },
        // { id: 2, nome: 'Minha empresa' },
        // { id: 3, nome: 'Parceria local (prefeitura, igreja, ginásio etc.)' }
        switch ($responseProposta->localCurso) {
            case 1: $order['unidade']['nome'] = 'Unidade Senai';
                    break;
            case 2: $order['unidade']['nome'] = 'Minha empresa';
                    break;
            case 3: $order['unidade']['nome'] = 'Parceria local (prefeitura, igreja, ginásio etc.)';
                    break;
        }
        $order['valorVideo']['ValorVideo'] = number_format($responseProposta->valorVideo, 2, ',', '.');
        $pdf = PDF::loadView('mail.orderRequestedPdfAdmin', array('order' => $order));
        return $pdf->stream();
    }
}
