<?php

namespace App\Http\Controllers;

use App\Coordenador;
use Illuminate\Http\Request;

class CoordenadorAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $vendedor =  array_key_exists('vendedor', $params) ? $params['vendedor'] : '';
        $coordenador =  array_key_exists('coordenador', $params) ? $params['coordenador'] : '';
        if ($vendedor != '') { 
            return Coordenador::orderBy('pessoa.nome')
                ->join('pessoa', 'pessoa.id', '=', 'coordenador.pessoa_id')
                ->rightjoin('coordenadorVendedor','coordenadorVendedor.coord_id','=','pessoa.id')
                ->where('pessoa.tipoPessoa', '3')
                ->where('coordenadorVendedor.vendedor_id', $vendedor)
                ->get();
        }
        else if ($coordenador != '') { 
            return Coordenador::orderBy('pessoa.nome')
                ->join('pessoa', 'pessoa.id', '=', 'coordenador.pessoa_id')
                ->where('pessoa.tipoPessoa', '3')
                ->where('pessoa.id', $coordenador)
                ->get();
        }else{
            return Coordenador::orderBy('pessoa.nome')
                ->join('pessoa', 'pessoa.id', '=', 'coordenador.pessoa_id')
                ->where('pessoa.tipoPessoa', '3')
                ->get();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Coordenador::create($request->all());
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Coordenador  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Coordenador::where('id', $id)->first();
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coordenador  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Coordenador::where('id', $id)
            ->update($request->all());
    }
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Coordenador::where('id', $id)->delete();
    }
}
