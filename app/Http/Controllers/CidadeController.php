<?php

namespace App\Http\Controllers;

use App\Cidade;
use App\Sede;
use App\Http\Controllers\Utils\Handles;
use Illuminate\Http\Request;

class CidadeController extends Controller
{
    private $Handles;
    public function __construct()
    {
        $this->Handles = new Handles(\App\Cidade::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $params = $request->all();
        $name =  array_key_exists('name', $params) ? $params['name'] : '';
        // $response = [];
        // $cidades = Cidade::all();
        // foreach ($cidades as $cidade) {
        //     $uf = new UfController();
        //     $dadosUf = $uf->show($cidade->estado_uf);
        //     $cidade['uf'] = $dadosUf == null ? ['err' => 'notFound'] : $dadosUf;
        //     $response[] = $cidade;
        // }
        // return $this->Handles->handleList($response);

        return Cidade::orderBy('nome')
            ->where("nome", 'like', $name.'%')
            ->paginate();
        ;
    }
    public function indexUnpaginated()
    {     
        return Cidade::orderBy('nome', 'asc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payload = $request->all();
        return Cidade::create($payload);
        // return $this->Handles->handleStore($payload);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cidade  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->Handles->handleShow($id);
    }
    /**
     * Display the specified city description.
     *
     * @param  Array  $param
     * @return \Illuminate\Http\Response
     */
    public function findBySearch(Request $request)
    {

    $arrSearch = $request->all();
    $nome = $arrSearch['nome'];
    // Busca pela pessoa que tenha parte do nome encontrado na razão social
    $cidade = Cidade::select('id', 'nome as descricao')
        ->where("nome", 'like', '%'.$nome.'%')
        ->orderby('nome')
        ->get();

    // retornar somete um resultado
    return $cidade;
}

    /**
     * Display the specified resource.
     *
     * @param  \App\Cidade  $id
     * @return \Illuminate\Http\Response
     */
    public function getByName($cityName)
    {
        return Cidade::where('nome', strtolower($cityName))->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cidade  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payload = $request->all();
        unset($payload['uf']);
        return $this->Handles->handleUpdate($id, $payload);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return !Sede::where('cidade_id',$id)->first() ? $this->Handles->handleDelete($id) : Handles::jsonResponse('true', 'error', 'Esta cidade possui sedes vinculadas.', 'Erro de chave externa.', 400);
    }

    public function destroyPaginated($id){
        try {
            Cidade::where('id',$id)->delete();
            return Cidade::orderBy('nome')->paginate();
        }catch(\Illuminate\Database\QueryException $e){
            return Handles::jsonResponse('true', 'error', 'Esta cidade possui sedes vinculadas.', $e->errorInfo[2], 400);
        }
    }
}