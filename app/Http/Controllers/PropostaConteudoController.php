<?php

namespace App\Http\Controllers;

use App\PropostaConteudo;
use Illuminate\Http\Request;

class PropostaConteudoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PropostaConteudo::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return PropostaConteudo::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $PropostaConteudo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return PropostaConteudo::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PropostaConteudo  $PropostaConteudo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return PropostaConteudo::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return PropostaConteudo::where('id', $id)->delete();
    }
}