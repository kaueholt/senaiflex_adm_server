<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Utils\Handles;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Categoria::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Categoria::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $Categoria
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Categoria::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categoria  $Categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Categoria::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        try{
            return Categoria::where('id', $id)->delete();
        }catch(\Illuminate\Database\QueryException $e){
            return Handles::jsonResponse('true', 'error', 'Esta categoria possui vínculos.', $e->errorInfo[2], 400);
        }
    }
}