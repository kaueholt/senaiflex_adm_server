<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MailRulesController;
use App\Http\Controllers\PessoaController;
use App\Pessoa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserAuthController extends Controller
{
    private $Pessoa;

    /**
     * UserAuthController constructor.
     */
    public function __construct()
    {
        $this->Pessoa = new PessoaController();
    }
    /**
     * Search person with cnpjCpf and password.
     *
     * @param  varchar $cnpjCpf
     * @param  varchar $password
     * @return \Illuminate\Http\Response
     */
    public function Login(Request $request)
    {
        $payload = $request->all();
        // Busca pela pessoa que tenha cnpjCpf igual a valor da por parametro cnpjCpf
        $person = Pessoa::select('pessoa.id', 'pessoa.nome', 'cnpjCpf', 'cep', 'logradouro', 'bairro', 'cidade_id', 'complemento', 'email', 'estado_uf', 'fone', 'newAccount', 'cidade.nome as cidade', 'numero', 'password', 'tipoPessoa')
            ->join('contato', 'pessoa.id', '=', 'contato.pessoa_id')
            ->join('cidade', 'pessoa.cidade_id', '=', 'cidade.id')
            ->where("cnpjCpf", $payload['cnpjCpf'])
            ->where("password", $payload['password'])
            ->first();
        // retornar somete um resultado
        return $person;
    }
    public function loginAdmin(Request $request)
    {
        // Busca pela pessoa que tenha cnpjCpf igual a valor da por parametro cnpjCpf
        $person = $this->Login($request);
        // verifica resultado
        if (!empty($person)) {
            switch ($person->tipoPessoa){
                case '1': //indústria : sem acesso
                    $response = response()->json(                       
                        array(
                            'code' => 25000,
                            'id' => '',
                            'type' => '',
                            'data' => array('token' => '')                                       
                        )
                    );
                    break; 
                case '2': //vendedor
                    $response = response()->json(                       
                        array(
                            'code' => 20000,
                            'id' => $person->id,
                            'type' => $person->tipoPessoa,
                            'name' => $person->nome,
                            'data' => array('token' => 'vendor-token')                    
                        )
                    );
                    break;
                case '3': //coordenador
                    $response = response()->json(    
                        array(
                            'code' => 20000,
                            'id' => $person->id,
                            'type' => $person->tipoPessoa,
                            'name' => $person->nome,
                            'data' => array('token' => 'manager-token')                    
                        ) 
                    );
                    break;
                case '9': //admin
                    $response = response()->json(  
                        array(
                            'code' => 20000,
                            'id' => $person->id,
                            'type' => $person->tipoPessoa,
                            'name' => $person->nome,
                            'data' => array('token' => 'admin-token')                    
                        )
                    );
                    break;
                default: //nenhum destes -> erro
                    $response = response()->json(  
                        array(
                            'code' => 25500,
                            'id' => '',
                            'type' => '',
                            'data' => array('token' => '')                    
                        )
                    );
                    break;
            }
            return $response;
        }else{
            return $response = response()->json(
                array(
                    'code' => 60204,
                    'id' => 0,
                    'type' => '',
                    'data' => array('token' => '')                    
                )
            );

        }    

        // retornar somete um resultado
    }

    /**
     * @param Request $mail
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $mail)
    {
        //print_r($mail);
        $person = $this->Pessoa->findByEmail($mail[0]);
        $mailer = new MailRulesController();
        
        $sendMail = $mailer->sendMailResetPassword($person[0]->email, $person[0]->id);
        //print_r($sendMail);
        return "Status envio: {$sendMail}";

    }

    /**
     * @param Request $mail
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $payload)
    {
        //print_r($payload);

        $sql = "update pessoa ";
        $sql .= "set password = '".$payload->password."'";
        $sql .= " where id = ". $payload->userId;

        print_r($sql);

        $affected = DB::update($sql);

        return $affected;

        // // Buscar pela pessoa com o id recebido
        // $person = Pessoa::find($payload->userId);
        // // Insere nova senha
        // $person->password = $payload->password;
        // // salva os dados, retornando 1 para sucesso e 0 para erro
        // return $person->save() ? 1 : 0;
    }
    /**
     * info user with id.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function infoUser(Request $request)
    {
        $payload = $request->all();
        $sql = "SELECT id, nome, cnpjCpf, tipopessoa, ";
        $sql .= "CASE WHEN tipoPessoa = '2' THEN id ELSE (select coord_id from coordenadorVendedor where vendedor_id = id) END AS coordenador, ";
        $sql .= "CASE WHEN tipoPessoa = '3' THEN id ELSE 0 END AS vendedor ";
        $sql .= "from pessoa ";
        $sql .= "left join coordenador on coordenador.pessoa_id=pessoa.id ";
        $sql .= "left join vendedor on vendedor.pessoa_id=pessoa.id ";
        $sql .=  "where id=". $payload['id'];

        $response = DB::select($sql);
        return $response;
    }



}
