<?php

namespace App\Http\Controllers;

use App\Vendedor;
use Illuminate\Http\Request;

class VendedorIndustriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $params = $request->all();
        $id =  array_key_exists('industria_pessoa_id', $params) ? $params['industria_pessoa_id'] : '';
        if ($id != '') {
            return VendedorIndustria::where('industria_pessoa_id', $id)
            ->first();        }
        else{
            return VendedorIndustria::all();
        }    

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return VendedorIndustria::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $industria_pessoa_id
     * @return \Illuminate\Http\Response
     */
    public function show($industria_pessoa_id)
    {
        return VendedorIndustria::where('industria_pessoa_id', $industria_pessoa_id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Uf  $industria_pessoa_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $industria_pessoa_id)
    {
        $payload = $request->all();

        return VendedorIndustria::where('industria_pessoa_id', $industria_pessoa_id)
            ->update($payload);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($industria_pessoa_id)
    {
        return VendedorIndustria::where('industria_pessoa_id', $industria_pessoa_id)->delete();
    }
}