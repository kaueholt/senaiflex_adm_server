<?php

namespace App\Http\Controllers;

use App\VideoValor;
use Illuminate\Http\Request;

class VideoValorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // retorna sempre somente o valor do vídeo dentro do período vigente
        return VideoValor::where('dataVigenciaInicial', '<=', date('Y-m-d'))
            ->where('dataVigenciaFinal', '>=', date('Y-m-d'))
            ->first();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return VideoValor::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $VideoValor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return VideoValor::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VideoValor  $VideoValor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return VideoValor::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return VideoValor::where('id', $id)->delete();
    }
}