<?php

namespace App\Http\Controllers;

use App\Vendedor;
use Illuminate\Http\Request;

class VendedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_2(Request $request)
    {
        $params = $request->all();
        $coordenador =  array_key_exists('coordenador', $params) ? $params['coordenador'] : '';
        if ($coordenador != '') {            
            return Vendedor::select('pessoa.nome','pessoa.id','coordenadorVendedor.coord_id','coordenadorVendedor.vendedor_id')->orderBy('pessoa.nome')
            ->join('pessoa', 'pessoa.id', '=', 'vendedor.pessoa_id')
            ->join('coordenadorVendedor', 'coordenadorVendedor.vendedor_id', '=', 'vendedor.pessoa_id')
            ->where('pessoa.tipoPessoa', '2')
            ->where('coordenadorVendedor.coord_id', $coordenador)
            ->get();
        }
        else {
            return Vendedor::select('vendedor.*','pessoa.nome','pessoa.id')->orderBy('pessoa.nome')
            ->join('pessoa', 'pessoa.id', '=', 'vendedor.pessoa_id')
            ->where('pessoa.tipoPessoa', '2')
            ->get();
        }    
    }

    public function index(Request $request)
    {

        $params = $request->all();
        $coordenador =  array_key_exists('coordenador', $params) ? $params['coordenador'] : '';

        if ($coordenador != '') {
            return Vendedor::select('pessoa.*')
            ->join('pessoa', 'pessoa.id', '=', 'vendedor.pessoa_id')
            ->join('coordenadorVendedor', 'coordenadorVendedor.vendedor_id', '=', 'vendedor.pessoa_id')
            ->where('pessoa.tipoPessoa', '3')
            ->where('coordenadorVendedor.coord_id', $coordenador)
            ->get();
        }
        else {
            return Vendedor::select('pessoa.*')
            ->join('pessoa', 'pessoa.id', '=', 'vendedor.pessoa_id')
            ->where('pessoa.tipoPessoa', '3')
            ->get();
        }    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Vendedor::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vendedor  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Vendedor::where('pessoa_id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vendedor  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Vendedor::where('pessoa_id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Vendedor::where('pessoa_id', $id)->delete();
    }
}
