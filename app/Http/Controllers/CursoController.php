<?php

namespace App\Http\Controllers;

use App\Curso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Utils\Handles;

class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexUnpaginated()
    {
        // $sql = "select DISTINCT p.id, pes.nome, p.contatoNome, RIGHT('0000000000'+CAST(p.totalValorVideos AS VARCHAR(10)),10) as totalValorVideos, RIGHT('0000000000'+CAST(p.totalValorAulas AS VARCHAR(10)),10) as totalValorAulas, p.necessidade, p.created_at ";
        // $sql .= 'from proposta as p ';
        // $sql .= 'left join pessoa pes on p.industria_pessoa_id = pes.id ';
        // $sql .= 'left join propostaCurso t on t.proposta_id = p.id ';
        // $sql .= "where p.created_at >= '". date("Y-m-d",$timeWindow) ."' ";
        // $sql .= "and p.created_at <= '". date("Y-m-d") . "' ";
        // $sql .= " order by p.created_at";
        // return DB::select($sql);       
        return Curso::orderBy('descricao', 'asc')->get(); //FUNCIONA
    }

     //ORIGINAL
    public function index(Request $request){
        $params = $request->all();
        $descricao =  array_key_exists('descricao', $params) ? $params['descricao'] : '';
        return Curso::orderBy('descricao')
            ->where("descricao", 'like', $descricao.'%')
            ->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Curso::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $Curso
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Curso::where('id', $id)->first();
    }
        /**
     * Display the specified course description.
     *
     * @param  Array  $param
     * @return \Illuminate\Http\Response
     */
    public function findBySearch(Request $request)
    {

    $arrSearch = $request->all();
    $nome = $arrSearch['nome'];
    // Busca pela pessoa que tenha parte do nome encontrado na razão social
    $search = Curso::select('id', 'descricao')
        ->where("descricao", 'like', '%'.$nome.'%')
        ->orderby('descricao')
        ->get();

    // retornar somete um resultado
    return $search;
}

    /**
     * Display the specified resource.
     *
     * @param  Int  $Curso
     * @return \Illuminate\Http\Response
     */
    public function getByArea($areaId)
    {
        return Curso::where('area_id', $areaId)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Curso  $Curso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Curso::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // return Curso::where('id', $id)->delete();
        return Curso::where('id', $id)->delete();
    }
    public function destroyPaginated($id)
    {
        try{
            Curso::where('id', $id)->delete();
            return Curso::orderBy('descricao','asc')->paginate();
        }catch(\Illuminate\Database\QueryException $e){
            return Handles::jsonResponse('true', 'error', 'Este curso possui vínculos.', $e->errorInfo[2], 400);
        }
    }
}