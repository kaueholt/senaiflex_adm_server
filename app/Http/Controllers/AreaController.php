<?php

namespace App\Http\Controllers;

use App\Area;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$area = Area::all();
        //print_r($area);
        //$created = $area->created_at->format('Y-m-d H:i:s');
        return Area::orderBy('status', 'asc')->orderBy('descricao', 'asc')->get();
        
        //PARA TENTAR ARRUMAR A ORDENAÇÃO DO CAMPO DESCRIÇÃO ÁREA - não funcionou.
        //0000000005 vem depois do 0000000021 na ordenação
        //$sql = "select area.descricao, RIGHT('0000000000'+CAST(area.id AS VARCHAR(10)),10) as id, area.created_at, area.updated_at from area";
        //return DB::select($sql);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Area::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $Area
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Area::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Area  $Area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Area::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Area::where('id', $id)->delete();
    }
}