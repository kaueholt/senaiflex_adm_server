<?php

namespace App\Http\Controllers;

use App\Regiao;
use App\MicroRegiao;
use Illuminate\Http\Request;
use App\Http\Controllers\Utils\Handles;

class RegiaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = [];
        // $regions = Regiao::all();
        $regions = Regiao::orderby('nome','asc')->get();

        foreach ($regions as $region) {
            $headquarter = new SedeController();
            $regionData = $headquarter->show($region->sede_id);
            $region['sede'] = $regionData == null ? ['err' => 'notFound'] : $regionData;
            $response[] = $region;
        }

        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        return Regiao::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Regiao  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Regiao::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Regiao  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payload = $request->all();
        unset($payload['cidade']);

        return Regiao::where('id', $id)
            ->update($payload);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return !MicroRegiao::where('regiao_id', $id)->first() ? Regiao::where('id', $id)->delete() : Handles::jsonResponse('true', 'error', 'Esta região possui micro-regiões vinculadas.', 'Erro de chave externa.', 400);
    }
}
