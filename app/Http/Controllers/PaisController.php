<?php

namespace App\Http\Controllers;

use App\Pais;
use App\Uf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Utils\Handles;

class PaisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Pais::orderby('nome','asc')->get();
        $sql = "select DISTINCT p.id, p.nome, p.reduzido, p.created_at, p.updated_at, RIGHT('0000000000'+CAST(p.codigo AS VARCHAR(10)),10) as codigo ";
        $sql .= "from pais p ";
        $sql .= "order by p.nome asc ";
        return DB::select($sql);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        //dd($request->all());
        return Pais::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $pais
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Pais::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        return Pais::where('id', $id)
            ->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return !Uf::where('pais_id',$id)->first() ? Pais::where('id', $id)->delete() : Handles::jsonResponse('true', 'error', 'Este país possui estados vinculados.', 'Erro de chave externa.', 400);
    }
}