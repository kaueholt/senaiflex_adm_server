<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoMinimoMaximo extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'videoMinimoMaximo';
    protected $fillable = ['id', 'quantidadeMinimaVideo', 'quantidadeMaximaVideo', 'cargaHoraria','dataVigenciaInicial', 'dataVigenciaFinal']; // Liberação para gravação

}