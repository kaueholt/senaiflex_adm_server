<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industria extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'industria';
    protected $fillable = [
        'status',
        'nomeFantasia',
        'numeroFuncionarios',
        'pessoa_id'
    ];
}
