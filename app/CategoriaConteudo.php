<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaConteudo extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'categoriaConteudo';
    protected $fillable = ['id', 'categoria_id', 'counteudo_id','dataVigenciaInicial', 'dataVigenciaFinal', 'cargaHoraria', 'valorHora']; // Liberação para gravação

}