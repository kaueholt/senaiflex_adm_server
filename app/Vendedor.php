<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendedor extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'vendedor';
    protected $fillable = ['pessoa_id', 'dataVigenciaInicial', 'dataVigenciaFinal'];
}