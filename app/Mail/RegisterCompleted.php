<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegisterCompleted extends Mailable
{
    use Queueable, SerializesModels;
    private $userId;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userId)
    {
        $this->userId = $userId;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $siteEnv = "http://ec2-18-228-189-213.sa-east-1.compute.amazonaws.com:8080";
        $siteEnv = getenv('SITE_URL');
        $siteUrl = "{$siteEnv}/#/alterar-senha/{$this->userId}";
        // $apiUrl = "http://ec2-18-228-189-213.sa-east-1.compute.amazonaws.com";
        $apiUrl = getenv('API_URL');
        $sender = getenv('MAIL_SENDER_NORESPONSE');

        try {
            $this->from($sender)
                ->subject('Senai Flex - Novo Cadastro')
                ->view('mail.registerCompleted', [
                    "siteUrl" => $siteUrl,
                    "apiUrl" => $apiUrl,
                ]);
            return 'true';
        } catch (\Throwable $th) {
            return 'false';
        }
    }
}