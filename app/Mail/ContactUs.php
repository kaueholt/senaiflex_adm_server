<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;
    private $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $message, $cnpj, $emailContact, $telefoneContact, $nameBusiness, $cityBusiness, $telephoneBusiness)
    {
        $this->name = $name;
        $this->message = $message;
        $this->cnpj = $cnpj;
        $this->emailContact = $emailContact;
        $this->telefoneContact = $telefoneContact;
        $this->nameBusiness = $nameBusiness;
        $this->cityBusiness = $cityBusiness;
        $this->telephoneBusiness = $telephoneBusiness;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $siteUrl = "http://ec2-18-228-189-213.sa-east-1.compute.amazonaws.com";
        $siteUrl = getenv('SITE_URL');
        $sender = getenv('MAIL_SENDER_NORESPONSE');


        try {
            $this->from($sender)
                ->subject('Fale conosco - Senai Flex')
                ->view('mail.contactUs')
                ->with('data', 
                ["name" => $this->name, 
                "message" => $this->message, 
                "cnpj" => $this->cnpj,
                "emailContact" => $this->emailContact,
                "telefoneContact" => $this->telefoneContact,
                "nameBusiness" => $this->nameBusiness, 
                "cityBusiness" => $this->cityBusiness, 
                "telephoneBusiness" => $this->telephoneBusiness,
                "siteUrl" => $siteUrl]);
            return 'true';
        } catch (\Throwable $th) {
            // print_r($th);
            return 'false';
        }        
        //  return $this->from('carla.bailer@sistemafiep.org.br')
        //     ->subject('Fale conosco - Senai Flex')
        //     ->view('mail.contactUs')
        //     ->with('data', 
        //     ["name" => $this->name, 
        //     "message" => $this->message, 
        //     "cnpj" => $this->cnpj, 
        //     "nameBusiness" => $this->nameBusiness, 
        //     "cityBusiness" => $this->cityBusiness, 
        //     "telephoneBusiness" => $this->telephoneBusiness], 
        //     "imgUrl", $imgUrl,
        //     "siteUrl", $siteUrl);
    }
}