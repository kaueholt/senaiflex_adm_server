<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderRequestedPdf extends Mailable
{

    use Queueable, SerializesModels;
    private $order;
    private $numOrder;
    /**
     * Create a new message instance.
     * $propostaResponse, $courseResponse, $conteudoResponse, $conteudoAvulsoResponse
     * @return void
     */
    public function __construct($numOrder, $order)
    {
        $this->numOrder = $numOrder;
        $this->order = $order;
        // $this->curse = $curse;
        // $this->content = $content;
        // $this->singleContent = $singleContent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $siteUrl = "http://ec2-18-228-189-213.sa-east-1.compute.amazonaws.com";
        $siteUrl = getenv('SITE_URL');
        $sender = getenv('MAIL_SENDER_NORESPONSE');
        
        return $this->from($sender)
            ->subject('Nova proposta de curso')
            ->view('mail.orderRequestedPdf')->with([
            'numOrder' => $this->numOrder,
            'order' => $this->order,
            'siteUrl' => $siteUrl,
        ]);
    }
}