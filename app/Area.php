<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    //protected $dates = ['created_at'];
    // protected $casts = [
    //     'created_at' => 'datetime:Y-m-d H:i:s'
    // ];
    //protected $dateFormat = 'Y-m-d H:i:s';
    
    
    // //protected $dateFormat = 'M j Y h:i:s:000A';
    // protected $dateFormat = 'Y-m-d H:i:s.000';

    protected $dateFormat = 'Y-m-d H:i:s.v';

    protected $table = 'area';
    protected $fillable = ['id', 'descricao', 'status']; // Liberação para gravação

}