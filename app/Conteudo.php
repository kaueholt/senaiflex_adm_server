<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conteudo extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'conteudo';
    protected $fillable = ['id', 'nome', 'dataVigenciaInicial', 'dataVigenciaFinal', 'curso_id']; // Liberação para gravação

}