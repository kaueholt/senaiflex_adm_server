<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'categoria';
    protected $fillable = ['id', 'descricao', 'dataVigenciaInicio', 'dataVigenciaFinal']; // Liberação para gravação

}