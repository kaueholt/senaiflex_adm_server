<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regiao extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'regiao';
    protected $fillable = ['id','nome', 'sede_id'];
}
