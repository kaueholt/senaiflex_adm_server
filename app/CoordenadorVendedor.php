<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoordenadorVendedor extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'coordenadorVendedor';
    protected $fillable = ['dataVigenciaInicial', 'dataVigenciaFinal', 'coord_id', 'vendedor_id'];

}