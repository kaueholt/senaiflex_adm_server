<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropostaCurso extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'propostaCurso';
    protected $fillable = ['proposta_id', 'curso_id']; // Liberação para gravação

}