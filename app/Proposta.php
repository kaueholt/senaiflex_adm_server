<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposta extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'proposta';
    protected $fillable = [
        'id', 
        'status', 
        'numeroProfissionais', 
        'localCurso', 
        'necessidade', 
        'insereVideo', 
        'totalCargaHoraria',
        'totalQuantidadeVideos',
        'totalValorVideos',
        'totalValorAulas',
        'valorVideo_id',
        'videoMinimoMaximo_id',
        'videoQuantidadeMaxima_id',
        'industria_pessoa_id',
        'contatoNome',
        'contatoFone',
        'contatoEmail',
        'created_id'
    ]; // Liberação para gravação

}