<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropostaConteudoAvulso extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'propostaConteudoAvulso';
    protected $fillable = ['id', 'proposta_id', 'conhecimento', 'justificativa', 'cargaHoraria']; // Liberação para gravação

}