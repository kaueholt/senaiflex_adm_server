<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'pessoa';
    protected $fillable = [
        'nome',
        'cnpjCpf',
        'cep',
        'logradouro',
        'numero',
        'bairro',
        'complemento',
        'tipoPessoa',
        'password',
        'cidade_id', 
        'newAccount',
        'created_at',
        'updated_at'
    ];
} 
