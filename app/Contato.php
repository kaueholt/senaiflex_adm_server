<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
  protected $dateFormat = 'Y-m-d H:i:s.v';
  protected $table = 'contato';
  protected $fillable = [
    'nome',
    'fone',
    'email',
    'pessoa_id',
    'principalContato'
  ];
}
