<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'cidade';
    protected $fillable = ['nome', 'estado_uf', 'microRegiao_uf']; // Liberação para gravação

}