<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uf extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'estado';
    // protected $primaryKey = 'uf';
    protected $fillable = ['uf', 'nome', 'pais_id']; // Liberação para gravação
}