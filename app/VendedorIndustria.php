<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendedorIndustria extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'vendedorIndustria';
    protected $fillable = ['vendedor_pessoa_id', 'industria_pessoa_id', 'dataVigenciaIncial', 'dataVigenciaFinal']; 
    //erro de português no campo dataVigenciaIncial
}