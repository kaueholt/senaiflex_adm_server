<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'pais';
    protected $fillable = ['id', 'nome', 'reduzido', 'codigo']; // Liberação para gravação

}