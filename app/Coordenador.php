<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordenador extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'coordenador';
    protected $fillable = ['pessoa_id', 'dataVigenciaInicial', 'dataVigenciaFinal'];

}