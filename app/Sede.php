<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sede extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'sede';
    protected $fillable = ['nome', 'cidade_id']; // Liberação para gravação
}