<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'curso';
    protected $fillable = ['descricao', 'dataVigenciaInicial', 'dateVigenciaFinal', 'area_id']; // Liberação para gravação

}