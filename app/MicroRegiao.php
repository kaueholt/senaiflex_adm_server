<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MicroRegiao extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';
    protected $table = 'microRegiao';
    protected $fillable = ['nome','regiao_id'];

}
