<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Senai Flex</title>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
  <style>
  body {
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
  }

  ul {
    margin: 0;
  }

  .container {
    max-width: 800px;
    margin: 0 auto;
    background: white;
    padding: 10px;
  }

  .company {}

  .boxes {
    color: #035e7f;
    width: 100%;
  }

  .box {
    padding: 15px;
    list-style: inside;
    border-radius: 10px;
    font-weight: bold;
    border: 5px solid white;
    box-sizing: border-box;
  }

  .box--blue {
    background: #e0eaf0;
  }

  .box--orange {
    background: #faa625;
  }

  .box--dark-blue {
    background: #035e7f;
  }

  .box-align {
    display: inline;
  }

  /*TABLE STYLES*/
  .table,
  .alert {
    color: white;
    width: 100%;
  }

  .table {
    border: 1px solid #7397aa;
    margin-top: 25px;
    margin-bottom: 25px;
    border-radius: 10px;
    padding: 10px;
  }

  .table__labels th {
    color: #7397aa;
  }

  th {
    color: #035e7f;
  }

  td {
    vertical-align: top;
  }

  .btn-total {
      background: #004a70;
      color: #faa625;
      width: 100%;
      padding: 5px 15px;
      width: 100%;
      border-radius: 5px;
      font-size: 1.4rem;
      font-weight: bolder;
      display: block;
      text-align: center;
  }

  .curse-set {
    min-height: 100px;
  }

  .total-result {
    height: 130px;
  }

  .total-curse {
      position: relative;
      width: 300px;
      left: 400px;
  }
  </style>
</head>

<body>
  <div class="container">
  <img src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAAeAAD/4QONaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzE0NSA3OS4xNjM0OTksIDIwMTgvMDgvMTMtMTY6NDA6MjIgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6YjA4NmZhODUtMTA2My1jODQ0LWIzNjMtZGI3NTNkYTFhMjllIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjYzMEY5Njk0NUNDNTExRTlBMjkwRkJFOTJFMDk4MDJFIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjYzMEY5NjkzNUNDNTExRTlBMjkwRkJFOTJFMDk4MDJFIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE5IChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjViODRmZDIxLTZiOWYtMTA0ZC05ODNkLTc2YzZiZTU1NDYyZCIgc3RSZWY6ZG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOjEyNWY2YzdmLTk5NzAtMTU0Mi1iODY4LTI0YzFlODAxOGIzMyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/uAA5BZG9iZQBkwAAAAAH/2wCEABALCwsMCxAMDBAXDw0PFxsUEBAUGx8XFxcXFx8eFxoaGhoXHh4jJSclIx4vLzMzLy9AQEBAQEBAQEBAQEBAQEABEQ8PERMRFRISFRQRFBEUGhQWFhQaJhoaHBoaJjAjHh4eHiMwKy4nJycuKzU1MDA1NUBAP0BAQEBAQEBAQEBAQP/AABEIALcEHgMBIgACEQEDEQH/xACpAAEBAQEBAQEBAAAAAAAAAAAAAQIDBAUGBwEBAQEBAQEAAAAAAAAAAAAAAAECAwQFEAACAQMCAwUEBQcHCwUAAAAAAQIRAwQhEjFBBVFxIhMGYZEyFIGhsUJScrIjVBU1FsHRgjNzkzThksLS4oOjs8PjJPDxYqJTEQACAgIBAgUCBAYDAQAAAAAAARECEgMhMUFRYSITBJEycYFCUqHB0XIjFLFiMwX/2gAMAwEAAhEDEQA/APz7+J95Cy4vvIe0+WUBApAVECBSkDTQBAACFAKACFUmiMgBtXGaV05AFO6uo2riPKWoB6lM1uPIpNGlcYIeqoqedXTSuoA7FOSuI0poA2DO4tQCglSgAAAAAAAAAAAAoAAABQCEBQAQFABAUAEAAAAAAAAAAAAAAAKQoKCFABlnOR1ZzkQHPmbM8zQAIUAENIhUUhQAQoFSVJUoNgymaBAAAAQoAICkABGUUAMgtBQpBVJVeiRwnc3P2Lgi3Z18KORCotWzSjpV8DK9h0jab1lqQpluvDgahB1qzqoGlEFOUqpdpzb7UemhiUKgHn0IdnbObg0CQZABQAAADUZOLqjIAPVFqSquBpHntT2yo/hZ6AZZpFIUFBCigAAABQSpHJIAoM70PMQBshneu0u5AFAqAAAAAAACAoAICkAAAKQAAAgKACFAIU8UvifeQ01q+8gQOlnGv5FVZtyubdZbVWlTr+zeofq8/cTDz8jCc3YaTmkpVSfDvPV+3+o/ih/mI8+23y837VdLp2dm0z0a18bFe5bard8UoPK+nZ0U5SsTSSq3Tkjgke+fXc+cJQk4bZJp+FcGeBG9L3tP3q0r4YNv/kxuWnj2nd+OagrO2L0/OzG/lbE71OLhFtLvfA4wjvnGFabmlXvP6jenjdH6ZKdu2/IxYeG3BatL/wBav6TWy+MJKWxq1K8tuFU/ns/T3W4Krwrr7lX7DwXbV2xcdu9CVu5HjCacWvoZ+wXr63XxYLS9l1P/AEEfGy8uz171BjzcJW7V+dq1OLdXSqTo0K2v+pQoFqa4WFsm3EM8OL0rqWZHfjY1y7D8ai9vveh2l6f61BVeFd+iO77Kn7/q/UrXRsD5jyt8YuNu3ajSC14Lg6JJdh+f/j+P6g/73/tmVsu+VXg3bTqrxa7k/K4+Hl5V12cezO7dim5Qim2ktHUmTh5WHNW8q1OzOS3JTTTa7T9J6Kueb1nMu0p5luUqcabrkWfe9T9I/afTn5ca5WPWdmnF/ih/SX10K9kXxa4JXTlryT554P53j49/Juqzj25Xbsq0hFVbpqzeVhZmHKMcqzOy5qsVNNVSPq+jv39Z/Jufms9/r7/E4f5E/tRXd5qvkZWte07zynB+WtWrt65G1Zg7lybpGEVVt+xI9l7ofV8e0713EuRtxVZS21ou104H6L0FhxfzWbKNWttq3Ls+9P8A0T9LidRt5WXmYsI0eHKEZSrVS3x3fU6ozbY02kpjqb16FaqbcO3Q/lR7LPSOqZFqN6xi3blqfwzjFtOmmh069iLC6vlWIxUYKe6EVwUZreku6p+89MfuHD/If50i3vFU13M69WV7VbjE/mj00fEhq5/WT/Kf2nu6BiW83rGLj3Vutyk5Ti+DUE50ffQ23Ck5pS0l3cGMbpHVcqCuY+LcuW3wmo0i+5s6z6H1u2nKWHdotXSNfsqfu+udatdFxrdx2nddyWyFtPYqJa60dPcfDfr+NP8AAv8Avf8AtnNXu+VVQdratVeLXcn5rExc3M3fK2J3tlN+yLlSvCpL9u/i3XZybcrVxJNwmqOj4H6f0B8Od32v+ofL9Z/vyf8AZw+w0rPN18DD1paleeWz5Suo9mL0/qGXHfjY9y5D8Si9vv4Hk6Zjwyuo42Nc+C7dhGf5Lep/RurdSsdF6er/AJW6MXG3atQpFVa0XDRJLsJe7q0kpbLq1qydrOK1PxcuidYjxxLn0Kv2HHHwc3KjKWPYndjF7ZOMW6PsPt/x/H9Qf97/ANs+T0HrrwOpu5c0xsmW29GvwpvSX9GvuqE7w5quA66pSVm0+vkefIx8jFmreRblam1uUZqjo+ZcfFycqTjjWpXZRVWoJuiP2vqTpf7RwHOyt2TYrO1T7y+9DTt5e0en+nw6X0zzMj9HduLzciUtNiSqouvDauP0k930z38DX+u84n0xMn4vIw8rF2/M2pWt9du9UrTicT1dX6o+pZ1zI1Vv4bMXyguHv4s8akjopjnqcbRLjlGimdxalMlBKgAoBACgAAAAAAgAKCAApCgAgKACAAAAFAAABQAACMxI6GJEBy5micygAFIUAAAhSAAAzzNGeYBUbRhG0UhQAQoBSAAAAAAAAxclti3z5Gzz35Vlt5R+0A51KlV0JFVZ6LduiIUQtJHVKgAKCFIACFBARoy4I2QFOMrZycWj1NGJQqWSQecG5QaMFIAAAD02JboUfFaHmOlme24ux6MA9aAFQQCplyObuCRB1ckZc6HPeYcyFOjnUy5HJyJVgHTzGR3DnUVAN72N77TAKDrG9JcTtbuqXeeQJtOqAPeDz27/ACkehNMEAAAAAAAAAAAAAAABCgAgKADxPiyMsuL7zIB7umYuHkyuLKveSopOHijGrb1+I9/7J6P+uL+8tnxIxlL4U33F8u5+F+5nl26dlru1fkX1p/pUcHp17aVok9Fdj/c5PsXel9IjbnKGYpSjFuK8y26tLQ+KXy5/hfuZDpo12onlttun90cfQ57r1u1jrrqjw7lP3vQ/VWJm2oY2dJWcum1ylRQu+1Pgm+z3H4I+l/DvWnCM1iTlGaUouNHo1VcGdL1q1y48Car3q26rLxR+t6t6QwM3ddxf/FyHr4V+jk/bHl9HuPyeBiZGF6hxcbJjsu28i2mv6So17GfrPSVjrGPj3bXUYyjYjtWPG46yXHdTnt4fyHg6/O1/FnTFFrfF2Vcpyrce1M5Vs03WclDO96VarsSweSlH6PqmDhZ+MrGc6WVJST3bPEqpa/SfJ/hf01+L/jf5T0eq8LJzuleVi23duRuRnsXGiTTp7z8X/D3Wv1O77iUUr78fIu20W/8APPjqfX9ERUerZKjwVqSXdvifp49Uiusz6Xdom7UbthrjLjvi/bpVew/L+h4yh1TIjJUlGy00+KanEx6tv3cb1BbyLMtt21btyg/anItq5bGvIzS+Gmtv+3J9SPSVgerrGRaVMfMjdlFL7txRbnH+X/2PB6+/xOH+RP7Ufq+m51nqWFazLXCaq48XCa0lH6D8t65tyu52DagqznGUYr2uSSJRt3U9lBvZVLVbHpZq31PtelMZYvQrDkqSvbr0v6T8P/1SPj+lcjKfXMyd+1chDMU51cWluUt0dX7Gz9Lk5FjpHTHdknKziwjFRVKtKkInyMb1t0/IyLVhWLsHdnGClLbROTpV+Iil5NKcivGronbHHt49j5XrvF2Z2PlJUV624N9srb/mkj9J6Y/cOH+Q/wA6R4vW2L53SFfS8WPcjJv/AOMvA/raPb6Y/cOH+Q/zpBuda8nAqo3W86yfzW5/WT/Kf2n1vSX7/wAX/ef8uZ8m5/WT739p9f0l+/8AF/3n/Lmd7fY/7TyU/wDSv9yP3HVum9O6hbtxz3SMJNwe/Zq0fJv+mPTcLFycZeKMJOP6bmkcvXn+ExP7SX5p+Loc9dLOsqzXkd92ytbtOit5n7D0B8Od32v+ofL9Z/vyf9nD7D6foKcU823XxPy5Jexb1/KcvVnRep5XVPmMaxK9anCKUoa0cdGmXhbnPHBGm/j1hTz2/E+H0H984X9tD7T+idUwMLPxlZztLKkpp7tniSaWv0n4Tp3Tc7A6tgXMyxOxCV+EYymqJuvA/W+rMDKzulq1iw825C7G44Li4pSTp7zOzm9YceZdKa13ms/9Tz/wt6a/F/xv8p+CfF0Po/w91v8AUrvuOHT+n38/Ot4VtNTnKkm/uJfE33HSvEt2yOV3lCWvD+Z+59H5WVkdIisiL2WZO3ZuP78F/q8Dj60ysmz0+3YtRatZEtt24uSj4lD+l/IfQz8vG6B0mLtwTjaUbVi1Wm6T7XTvbOk44nXOlaa2cmFYvnCS/ljI4T6s49OR6mvR7eXrxP5ntY8SPVlYt3EyLmNeVLlp7ZfyNexnFo9J4HxwznvaNK4HEztKSTorhpTRx2smqAk9G5F3Hn3NFVxgsnoqDirhpXADqDmpou4A2DNS1AKQVKAQoIAUEABQQoBCgAAAAoAAAMSNmJEBzfEpHxNIAEKCkICgAgBQCE5lJzAKjRlGykABQAACFBCgAgBQDMnRV7Dxttur4s9V50ts8qAR2sQ5noOdpUidCGgQoAIAAAAAAQoICEKAUy0jhchzR6GYktADzgslQhoyAAAeq3c3QTfHg+8OZ54So6dppsgNubObkRsyAacjNQCgAAAAAAAAAAAAHW3da0fA5AA90ZVRo8lm7tdHwPUtQQoAAICgAgKACAoAICgAAFAPBLi+8hX8T7yFB7On9RuYDuOEIz8xJPdXSnce3+Jcn/8AGHvl/OePpvTvn5XF5qteWk9VWtfpR7v4bf6zH/N/2j5vyH/8/wB23vx7nGXFvDjoe/Qvne3X2Z9vmOa+PPU53PUWRctytuzBKacW6y5qnafIPs3PTzt253PmYvZFypt40VfxHxjv8T/Vi3+t0lZdfy+44/KXyZr/ALHXnHp/IH6vE9czs2IWbuGpu3FRUozcdIqnBxkflD1R6bnyt+bHHm4UrWj4HfY9aS9x1r4ZODlrexN+2m/GFJ+iyfXl+VtxxsSNub+/Oe9L+ioxPzF/Jv5N+eTfm53rj3Sm+NTkDVaVr0Rm2y9vucn6rD9dZVq0oZePG/NKnmRl5bf5S2yXuO8vXqp4cHXlW7p+Yfjymfbp4G/f2RGX8D6vSeuS6bnX8xWVdd9SThu203SU+NH2HLrXVH1bL+adpWmoKG1PdwrrWi7TwIpcVM9znnbHGeOp9TofqHI6MrsI21fs3aS8tycdslpuT14rjp2G8/1M83qWHnTxYxWG9yt729zru1lTk12HzbGHkZUnGxDe4qstUkl7W6I884uMnF8U6PvRP8bu1Kd11U8qfI3lsVF1wb44448z73WfVd3quE8P5dWYylGUpKe6qjyptXM+BFuMlJcVqjpbxr121cvW47rdmjuS08O7RHLXjyLXFTWrXD5U9H5ku7uLWnlcOOx+nzvWbzcK9iXMKKV6Di5eY9G+EqbeT1M9N9Yz6fg2cNYiuKymt+9xrVt8Nr7T80Ce3SIgvvbJmeYjoak90nLhV1PV0nqD6bn2s1QV12t3gb213RceOvaeQqRuJ4MS05XVcn2uueopdZtWrcsdWfKk5VUt1aqn4UfGCOrsXY2Y33GlqbajLtceJUq1SXFZcKX1ZLO127P1QueOx16d1HK6ZkrJxmlNKkovWMovjGR+kj6+pFb8GsubV2i+uB+SZlkvrq+WjVNt6qKvg+3131NLq9q1ajY+X8qfmKW/dKtKfhjQ9uF66yrVqNvLx1kTiqeZGWxv2yW2SPy5DPt1iIKt18naeWfsJev1Tw4OvKt3/YPjdG6/DpV6/f8AlVevX38bnt2xrucV4XxZ8g9Nvp2ZOcbcbTcp2/NiqrWH4uJmy1UXqaqn+60dDS2bbtNTZ16RWep7Otdcv9ZvW5zgrVu1FqFtPdq/ilWi46Ho6J6iyOkwuWVbV6zN7lBvbtlwbTo+J8eMTtdsXLFx2rsds1Sq71VcDTVOKcc8pd+DCveXsUz3t+J9DrPVodWu273y6sXYJxnJS3b192ui4anzmWKK0VJJQiWbs5fVnJsVLNGSmDRNCApCtE2ioqUSTaKM1UCBJnVFUmUqRIKmTcyq4NplxEFk6KaLvRwoxVoQJPTuFTz72iq6Qp6KgxF1NoAoAAAAABSFBQAACGZGjMgDk+JpEfEpAAAUgAABAUAEIaJzAKjREaKQAAAAAhQAAAACg45D8FPacIrU7ZHI52/iS7WQqPVBUiijgjnK60Qp0IcfPfYaV1PjoAdARMtQACNmXNIA2DhK6+Rlzk+LAPQ2lzIpRfM4qMny95pQX4kn3gHRkZnc486o0mmqohTlOJyZ6JLQ4yVGVEZkAFIDerMHazRpogOe1koelxSRxkgUwC0ICAhSFAAAAAAAAAAAAAPbalWCPEejGfFAjPQAAAAAAAAAAAAAAAAADwvi+8hXxfeQAFPZ03NsYjuO9ZV7ekkmk6U/KTPd+2sH9Sj7of6p5tu7dW7rT49tlV+pWSn6nfXp1WqnbfXW/wBrq2fEB9m51nCnbnBYcYuUWk6Q0bXH4T4x007Nl089T0x0lpz9DO2lKNYbFtnrCaj6nbChGeZYhNVjK5CMl2pySPR1bIvS6hfrOVIycUquiR4oSlCcZwdJRacX2Nao+hfzen5Unev401fkvFsnSDdKVo0zGytlvrsweyvtunpiU20+8dTWuyem1M1S2atzPKjyMZWPZhYwZQjSV6Ddx1fie6h7bmHg2snqG6zutY6g7cFKSpWldas8kc/Elj2LeTYlcuY1VBxntUk3XxaGr3U4XZ5svLa+bjBLVeHbTjoed0+Q/TGxRknbKJnbVqIc/bJ3VtC9U63OLSx6Rra54/cdsXHxb+NcvWMaN69vblYc5Jwh93brWRys2sRWcjNu2G4wmrdvH3NJN8dz46HHEyMOzsnchd86DruhNRTXZTadV1O3cnkrJs7rGTJTcIukoSXBp/aLa9ytdVW21Jq5zatjlzWvq8O/DItmp1o7PWrQ1GMrKOLW48e3J3ji9PnexL7Tt4+Sp1tOTopw0pu40bPP1G0rLjbeIseVW1NTlOM17N1Rezce9OzbdqUcSxFxhbjLxVl95yafMZWbbu41vFsxmrduTluuSUpcKUVEqIuum9bNbstjXKatbitZePR8uITlMmy+l02JOifDTrXm1oWXVcKZiGj0dPu2V0/L3WVLYou54pLzE5OieulPYatYFlY1m6sOWU79ZTpNxVuNaKMdftPDj5UbONkWXFt31FJ14bXU6Ry8W5j27OXalJ2U1buW5KL2t1o6pk2adqvstTOL7U3jZy64f3V/V5/wLr3a8aK+E11NKarh5/2vt5HS90/Hsx6lFLe8fynZlV6KbrydHpoby7lm703ChGxGHnOcbXik1ae9Jta619p5Y5dm3Zy7Fq3JQyNihukm47HXXRVI8y1LBs2JQkr+M5OzOLW3xS3eJND2dzdLWzs6batOY49qG4n93X8y+7qSsq4Vy12URPPuTEx+3oe1YvTbmbPpcLMozinGOS5vdviq1ceFDGNi48sCF61jLMuxcvmI75RnHXSkYvsMPq2Mrssy3juObONHJyrbUmqOSjQ44WXhYrt3vLuvIhxcZpQl3rbUx7fyMHxulYel3nK6Tz5VpVXx3/I1nozXOrnL1KsY0bWPDrzZcnnsqxPKj5n6OxKa3JOu2DfafQ6hjW7Pgt4kYQuSSs5MZylFpv2trVHz3djdyXevRrGc3OcIunF1aTdT2XM6xDDni40JqNySk3ckpbdrr4aJHq2U2vZqtVXcQrVy9K85lS15pyefXbWqbFZ15l1ePqflEOE/yg9XyvTvnf2Z5L302/Mbnu3uO6u3hQx5lmz0rG86yr6dy5ROTjSjVabTP7Wx/N+c+Xfzu2m7d+j3U27tpxt5mLLFt42VanLypSkpQklXd21R51q3tUzrusq2o7rP1O6rZXtV5cLlHd7dKdsLak7K6o8PSqt1xrbjrwzHUse1j5CVmqt3IRuRi9XFSXCp7Y2Onxu4ePLG3zybcJSnvkqbqqqVeNUfOzst5d93duyKSjCC12xjwR1fUYPJxL+x0xrcIONfica6/WddmvfbVqq3fKuu+WN4eWPplp88nKmzTXZsaVMXeuOVZWM+qE+nB2vWcGeNmK1Ydq5iOKjc3uTlWex1T0PVHpePblasSxJXYyivNylNrbKXGi4aHzlmR2Zsdj/8tpx1+Gk9+vadp5mHkqE8uxN3oxUZTtzUVNLhVNM431fIXCe3HLtZ2tzSvP3J8Wnv+R1rt0dWteUftVVxZ8fa+0dgsXGxMa7ev2/mJxvysQjucY+FfF4dT2XLNjIysdTt/o1hKUYJtUo5UVVroeGzl4/kTxb9qUrDn5ltxlScHw40o9PYdl1K0siFyNqXl27HkRi5KvPVuntGzV8htuNjus4tlCh19OKnhim3QklNFR4TXHmU/VLjlEVvFxsOxduWfPu5G56ycVFRdPunfLg7nVb8Vj/MvbCkatbfBHXwnlhl488a3j5VuUvJb8ucJbZUk6tOqaOz6lauXsl3LT8nJUE1GVJR8tUVHQtte7O1sL2slsU58NWvXHGLKIr+BK7NOKrlStX7bjHma1eWXH7vxGfj27VuxcjbdmV1S32925Jxa4N9tTxs9GRk2rtmzZtQcI2d9Nz3N7mn2LsPOer4yutSWycsrfd1jJ49328zzfId
  HsbpGMV6dJjnsu/kc5GaG5GTucSUI0aIykMMhWKFIKhMUCVWCFNoiiaBUDDNNmQCMhSFBKGaanRIUMwWTdvgdUc4HRENFAAAAAAKACgAAEMyNGZAHN8Soj4lRAAAUgAAAAAAJzKTmAaRoyjRSAAoBAUgAAAAAIAeeU3ca0XsLB7pqKk17VREa8tzXHRJP2MWE/MT5akNHXZHm5P6WHBdsveG6OpzlffJEDNStdj96/mObg46tadq1HnTfYFelzoUG7b7HU61PPJVTktGuwRldfwtshTuznP2uneYlO8vibQUaavVvmAVQT4Jv2vRfzmtlOLp7I6GXJow3JqutO0EO3lw5qvexst/hRw3SXBmldkuOoLJ18u3+GncZlaoqxb7jUZVVUUgOCnPt95tQdyO6qT7CQgm23wTaRXKUFSL0KDnKLi6MhZTlL4iFIDdmW2a7GYCAPZJaHBnbdWCfajg+JCkZk0zIICFBQQAAAAAAAAAAAA74q1ZwPRi8wRnoAAAAAAAAAAAAAAAAAB4ZcX3kLLi+8gAKQAhSAoAjGU5KMU5Sk6JLVts9V3pmfZtu7csSjCOsnpp30OWJ/irP9pH7Ufahav2+r5V+5GUcVRuOc2motbdKN6PU8vyfkX12iuEKjvFutocY156nq0aK7Ky8ubqnp6VlTkz5NrpudetK9asynbdWpKmtNNFxM2cLLv7latSm4NRmktU3yfuPo4eFC2sK9GF69cuNT3W3S3CkvveGXDnqjWQ5Rs9VcXT9Nb1WnFyOT+Xd3tWuD9SVW1ETfDlZf0Nr4tFRWtmvS3ZT4Uy4cf1PBDpefci5QsylFNqqpxi6PmeZxcW4tNNaNPimfXxMKFtYd6ML16d1qblBpW4eLhLwy+nVHi6jX9o36cfMdKdtTpp+Ra+21JrZKraaWP2vHnl/wAjG3RWuut4tVuyTTeXVT4IT6Zn27TuzsSjBKrb5L2ric5Y1+M7duUGp3lF21+JT+H3n08mz85ZvZN6zcxMq3Hdcck1buU5eLmauWL93M6ddt25Stxt2XKaT2ra6urOVfmXj1vXPry7JOtZSmWnJ0t8Ss+nOHjj3bTcNxCg+bawMy7OcLdpylae2a00f09xzu2btmbt3YuE1xiz6VzEt3J5mRKN27KN9wVqy0nze6Xhlp9BjrSauYyacWrEVtlrJavRvtOmr5Vr7a09LVq8woasqp+PP0MbPjVrrtb1J1fdymm2vDj6nnliu5bxlYszVy6payaam0/udxi70/MtWvOuWZRtri2uHej6mKvF0n/efazz47nKHUdzcv0b468JIxX5OxTGONXzlLbnbanDnyNv4+txOU2UKISUa1fpHmeTD6XlZE7U3ak8eUkpT4eGviaqS7hqGTee2fydm87c5qjklX201ofQvWci7ldOu2oynYULK3RTcY7X4q04C7auXrHUoWouc/mU9sVV0rLkjC+Vd3VnaqrZJYr9E3x55+40/j0VGlWztVty/wBUUy48jwXcW5cuwlj48oWr9fIjrJyUeL/nLLpefCUYysyTm6R4aule3sPoT+axYdOdu1KV+EblbW1uVHxVFrwZ5+oYtqOPDLtRnYU57ZWLnJ0rujXWh01/Ju3SqdFW+Va2ad23W1kp9U9F17+Rzv8AHolezV3auNrLiiSsk3HpjuccnpWTYyI2ILzfM/q5LRScVulSvYSGHO2rqybFzerTuQo0tqrTdJPlU+o4TXqK3JxeyS8MmtHS3rRngwZSlbz5Sbk/Jlq9XxRmvydttSbtV/4tV7NSm3ezXZ9o5NW+PqrsaSsv8myiT5UUrPdeZ5odMz7ltXYWJShJVi1SrXsXE8yR9zDwoWL2HOML12c4xuO7F0tQ3cn4Xw7zwZePd82/kbf0XnThu0+KrdDtp+VnstVuuPWr+3u1ES/5fgctvx8NdbJWnpZfd2meiPKom0gkU9R5iUKUgAoVIFQBpFIgCmZGTUjIIwRlFCkMMoaKUyyGoolDUQEaoRo0Rg0YZk2zNCkIKGqEoAEgVEZAbibRiJtGTaKAAAUAAAAFAAAISRoywDk+JSPiVEAABSAAAAAAAnMpOYBtGjKNFIAAAAAACFIACFAB57jrcklxSX1G7NHJtcEqI5uiuSk+Uvqeh1sRcYtPjWnuIzSJdg2tDmrUlrxPS0YaZCnn8uVew0oU+ntOu19o2oSIOck426cW9EdbcUlTsMU3TouEfzjtSiAOV2NY09xiGsV7jtJV0OS8Mtr4PVd5ASUU+LLtqqV0N0G1FBz8lE8pHXagopAQYjb26m+Gpoxc+HauMtP5yFJbXgXt195ma0OtKacjE+AB5wHxBoyAAAdrTrFokkZtOkjrJaEKcWQ0yFIQJN8CpN6HaKUdCFSODTXEh6ZRUkcJRo6CQ0ZABSAAAAAAA74vM4HoxVo32gjPQAUAgKQAAEAKQpGAAQpSFABCnhl8TIWXF95AQAAoKAQA6WZq3et3GqqElJpex1O+bnXcq9ckpzVmcqxtyk2l9HA8oOb10d1dqbVWKNrZZUdE4q3LOkb9+MPLjcnGHHapNKvcR3brUk5yam6zq34mub7TINY16wvHoZyt0l/U3HIvwh5cbko2612qTSr3GZSlKTlJuUnq29WyAKtU20kmxk2obfB2nk5FyOy5dnOK+7KTa+sRyL8YK3G5NQTqoqTSr3HIpMKRGNY6xHcudpnK09Op0hfvwlKULkoyn8TUmm+8kpznTfJyoqKrrRGQVVqnKSnxgmVmolwbV26ttJyWz4KN+GvZ2BTmlJKTW7SVHxXtMGkMV4IS/Fm438i3Fxt3Zwi+KjJpfUYt3b1ubnbnKE3xlFtN/SCUGFefSvV146/iM7cep+npz0OivXqxfmSrGu11eleNCXbt2663Zym1wcm39pkpVWqaeKld4DtZpqXD8y/MZHg/Sz/R/B4n4a6eHsMqcopqMmtypKjpVdjI9DnKQxquiX0E2fVv6neOTfjFQjcmoJ1UVJpJ9xHem4uLk3Fvc026OXb3nn3MVZnGvWF49CzbpL8Op2UjaZwTNKRqSQdgc1I2gQpUQqANEqAARkDAABQUhkApTLJzNIyVAI0RsVMtg02VkAKQpGUjIAgwgwDUToc4nQybQAKAAAAAACgAAAyzRlgHN8QJcQiApCgpCApAAAAAZ5micyg0jRlGgQoIACggAAAABAADzXvjftO1mfhUZaN/C/xf5TjcTdH2ps62aStJPVc0ZZpHYNVMbWvhk17K1X1krc/F9SBTdEYuS2+GPxv6vayPfzm/oojKiq0QB0txSijZlNLQVIAzE0pKj0fJmmycQUzCVdHpJcfb7UbOc4qoSnym/p1AOhTnS9+Ne4bbj4zf0aAhuTUVWTojKq3uapyiuxCMEnXn2vVlBQzEjTMSIDlStTJvhHvMGkRgAFIWLo6noWsTzHosusSMqOclRmTrcWphLUA3CIo95pGZOhCm2c5rU3F1RmYBzcTB1RiSKRoyACkAAAB7bMdsEjxJVaR748ARlKQAFIAARkqVkKQtQZKgClIUhQAADwy4vvIWXF95ACghQQAAAAAAoIAClIAClIUAFIUApTJQCgAACoMyYBmcjnxNUqzrG2STSRxoy0Z6PLQ8pCSwecVPR5KHkoA86k0ztBmvJiTbtEkaNAIFMlAABGAwVBlBmpalIAUgIACNgCpDLkNwksSaqWpioTEoYm6ggBAgEADcTojnE6IybQKQoAAAABKkqCmgZqWpAUyykZQc5cSokiogAAKQgKQAFIKgAhakqUhpGjKNAgAAKAAAAAAQjKRgHN8n2afQTH0jJdjOjVU0YtKk5rtVSMqOhKgzJ0Mmw3yRUiRXM2AZ1TrXTsLUMlADMtz4OhI71z3GtpUgCUb1Y4GzLQBUxUxWhpOoBSAlSAMxI2YlxAMtJ8TEqcjq6HKT1NIjIACkB1sS1a+lHIsHSSIDvMyuJZPmZQKdkc7y4UNJ6FcakKZgmlqZmdWjlJ1YBESS0KUA4g1JGaFMgAFBq2qzR7lwPHYXjPWCPqUlQQpCggQBSFIwDLKgwgDSKAQoBSAHhlxfeQsvifeQEBSAApAAAUhQAACgAAgNIpkoBQAADRCoBlABSAzM2YmQqM21qeiKONtHZGWbRopEygAoBQDE0bMTIDCKRFNGCigRaAGWRmmYZpEZGwmRkNGTpuLU51LUkFNNnKUyykcm6sjNJGqtm4wbFqNT0JGDZy8sjg0d6BoA89aFqbnA5VKmZaNojBGaMHSB1RygdUZNooAABGwYlIFJKQqZWptIwaJqVNo1QjQghUwyIM0iGJFRJFQAAJUpARsNnKUgU05k3nOoqQG95VI5lqUHeLNpnCMjakUzB1qDG4tQQ0DNS1BSglSgEIUgAZEqTr2ooXEhUQ5vWXcde05pVk0ZNm40oNyOc4yim1w5oxarK5tbKQ77ibjXlxrwCtxoBKM7kNxpWlzIrSq6gShuQqiK3o22zlJSXNkKdGROj+05Rc26J1qdVGipxYgGmRFfAlCAHK7yOpxuvUqD6GHJsAGjIAAAAAB1TqgYg+R0IU1Fm00ck6GqgGpy00OVDXEUIUgKADLRlxNgEOdCUOlCUKILZ0kepHmjoztGWhpdDD6mwQoBCgAAjKRgCgSBQClM1LUhSgAA8clqzDR0lxfeYZpoymZKKAhSFAIAAAAAUoIChEAKAACggKVGkZRpAjKCFKQqMyRUWhlm6oiVCOdDdDMoEKRXTrG4mcPLZ0txoUHXcTeg1oeaW6pAeneiN1OEVKp2jUdwKArIdDDNIpEw2ZBmTMNiUtTNTZkpCpFoAZDYZGUIzJmQyI5s2j02uB2TPNCVDopmTR2BlOpG2gCyOE9Gbc6nKbANIMzFmmbRhrk6QOqOVs6ohSkKRgEbOLdWbmzCRlmkbibRzoaVQU0GSpHIECKRMoQZiRUSRUUgMs0ZaKDDZykztJHNxIDmU04kSALGJrabhE1t0JJYODqipm5Q1CgWSNBNlqNtBQ0jLG40mRRqbUCkJuFS7CUIUVBaChCkCDCKQ0ZpSVTdNDJlo2nwWhycKNNaNcGdiNVAMRnJNuXGmlFob8yNNTL0JUCDSuxpqPNXY2ZqKsCCbpuLTWr4Mji5cTVGUFJGKiOZQQEfYGAyAyzjN1Z1kzjIqIyAA0QAAAAAAsXRnSpyOkXoRlQ5m0YKmAaKQpCghSAAhRQEIShqgLAkhuMjIKQ7Jlqc4s1U11MdDZKkBAUEKARVqaIWgKAUpCkBSAHjk9X3kD4vvBoyQpCgpAC0JAIAAAUhQAECogBQAUpllIwAjaMI2gGAAUyWJowWpGbTOiLQwmaTMlK0Eg2Z3UAOhhwTZVJMtQDKgkaoKlAMtGTdCNG5MtEMyZsxJCSQcwQpoyaRSIoBloxI6M5zIyowEqkNQ4mTZ1jA2rdDUKUNGShItKkrQqYBnYjE7eh1qZk9ADzLR0NVMv4imkRo62zsjjbOyBARlMSYKYmxExJ6iMjJo7IphMu4A0Rom4tUAEikTKQGJFRJFRoywASpSEaM0NMzUjKhJEUTTYiQ0aSKDLYBeZaIwmWoRGJEDZDZhnSKOiRzg9DogEKGWjZGQpmgoUoBzZEakZXEEOiI1zKihlRkhIyTbXOOjRSGiMhoAGdAUAEBQQpAwACEboG0jOrdWAR9phxcpUNs3bhSNXxkERnnlHayHpnbqjzyi4vU0QgAAAAABqLMhEBssTJuKBTRURFIAChIsCSUBWEikJQUNUFADANbRtICRepsxQqkVOCNSaNJGKnRcCySCMlSvgc9wB1TLU5QlU6oApSAhSk5lJzBTxP4n3gA0jLAABAAAUAAgAAAKVAEAAAKUywAAjaABGCgFIRgAjNIpVUAyaK2zm6gFBqFTYBChVqdEAERlIwCkIRpAAhxklUgBtGWaRQAQjOcwAzSOZY8QDJo7wbOlWAZKR1CqACmtTE26cAADguJoApk62zsgCkIzlNsAjKjlJssQCFOiKACkZmrAAOkTQBAZkQA2jLI2yLiAUyaojnLR6AGWaRG2WLYBCm6sywAU0kgwCoyzDJVgGjJ0tt1OyAARSMAhSBgAGJEQAIzaNAAHnvVVxSt6y5panSMt0a0a7UwCM0jRAAUEAABACFBlt8kAAZ56lAIUkUnLxOiWuvM7gGkZZDldjFrVoApDztUdAAAAAAAAAaibAIU0jSAAAAKQGkl2gAGtCAAEZACAMgAARtVoAVEYbfYcmwABB6neIABoIAApOYAKf/Z" width="100%" alt="Senai Flex"/>
  <table class="boxes" style="width: 100%"> 
    <tr>
        <td>
          <div class="curse-set box box--blue">
              <p>Cpnj: {{$order['user']['cnpj']}}</p>
              <p>Empresa: {{$order['user']['name']}}</p>
              <p>Cidade: {{$order['user']['city']}}</p>
          </div>
        </td>
        <td>
          <div class="curse-set box box--blue">
              <p>Nome do Contato: {{$order['contatoNome']}}</p>
              <p>Telefone: {{$order['contatoFone']}}</p>
              <p>E-mail: {{$order['contatoEmail']}}</p>
          </div>
        </td>
    </tr>
  </table>
    
    <table class="boxes">
      <tr>
        <th>Necessidade</th>
        <th>Área</th>
        <th>Cursos utilizados como base</th>
      </tr>
      <tr>
        <td>
          <div class="curse-set box box--orange">{{$order['necessidade']}}</div>
        </td>
        <td>
          <div class="curse-set box box--blue">{{$order['areaTecnologica']['descricao']}}</div>
        </td>
        <td width="30%">
          <ul class="curse-set box box--blue">
          @foreach ($order['cursosBase'] as $course)
            <li>{{ $course['descricao'] }}</li>
          @endforeach          
          </ul>
        </td>
      </tr>
    </table>
    <!-- TABELA DE CONTEÚDOS -->
    <table class="table" style="page-break-after: auto;">
      <tr>
        <th width="50%">Conteúdo</th>
        <th>Carga Horária</th>
        <th>Valor</th>
      </tr>

      @foreach ($order['conteudos'] as $content)
        <tr>
          <td width="50%">
            <div class="box box--dark-blue"> {{ $content['nome'] }}</div>
          </td>
          <td>
            <div class="box box--dark-blue"> {{ $content['duracaoFormatado'] }} </div>
          </td>
          <td>
            <div class="box box--dark-blue"> R$ {{ $content['valor'] }} </div>
          </td>
        </tr>
     @endforeach

    </table>
    <!-- TABELA DE CONTEÚDOS NÃO LISTADOS -->
    @if(!empty($order['cursosSolicitados']))
    <table class="table" style="page-break-after: auto;">
      <tr>
        <th>Conteúdo não listado</th>
        <th>Justificativa</th>
        <th nowrap>Carga horária</th>
      </tr>
      @foreach ($order['cursosSolicitados'] as $single)
        <tr>
          <td width="40%">
            <div class="box box--dark-blue"> {{ $single['conteudo'] }}</div>
          </td>
          <td>
            <div class="box box--dark-blue"> {{ $single['justificativa'] }}</div>
          </td>
          <td>
            <div class="box box--dark-blue"> {{ $single['cargaHoraria'] }}</div>
          </td>
        </tr>
     @endforeach
     </table>
     @endif
    <!-- OUTRAS INFORMAÇÕES -->
    <table class="boxes" style="page-break-after: auto;">
      <tr>
        <th>Local</th>
        <th>Quantidade</th>
        <th>Precificação</th>
      </tr>
      <tr>
        <td>
          <div class="total-result box box--blue">{{ $order['unidade']['nome'] }}</div>
        </td>
        <td>
          <div>
            <ul class="total-result box box--blue">
              <li>
                <span><b>{{ $order['numProfissionais'] }} -</b></span> <span> Número de profissionais capacitados</span></li>
              <li>
                <span><b>{{ $order['videoQtdDesejada'] }} -</b></span> <span> Quantidade desejada de vídeos</span></li>
              <li>
                <span><b>{{ $order['cursoCargaHoraria'] }} -</b></span> <span> Carga horária escolhida</span></li>
              <li>
                <span><b>{{ $order['totalCargaHoraria'] }} -</b></span> <span> Carga horária total
                @if(!empty($order['extra']))*@endif  
                </span> </li>
            </ul>
          </div>
        </td>
        <td width="30%">
          <div class="total-result box box--orange">
          <span><b>Estimativa das aulas presenciais: </b></span><br>R$ {{ $order['price'] }}
          <br><br><span><b>Estimativa dos vídeos: </b></span> <br>R$ {{ $order['totalValorVideos'] }}
          </div>
        </td>
      </tr>
    </table>
    @if(!empty($order['extra']))
      <div>
          <p>*Seu curso conterá uma atividade completementar de 30 minutos sem custo adicional</p>
      </div>
    @endif  
    <div class="total-curse">
        <p class="btn-total">Valor total: R$ {{ $order['totalGeral'] }}</p>
    </div>

    <!-- alert -->
    <div class="box box--dark-blue alert">Caso algum dos itens tenha ficado sem precificação, confirme a solicitação e
      nossa equipe enterá em contato. Os valores mencionados na proposta são estimados e poderão sofrer alterações.
    </div>

  
  </div>
</body>

</html>
