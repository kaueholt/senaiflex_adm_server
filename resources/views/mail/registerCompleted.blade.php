<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Senai Flex</title>
  <style>
  body {
    background-color: #035e7f;
  }

  .mail {
    display: flex;
    width: 100%;
    background-color: #035e7f;
    background: linear-gradient(180deg, #faa625 200px, #035e7f 200px);
    padding-bottom: 50px;
    text-align: center;
  }

  .mail-container {
    background: white;
    padding: 60px;
    margin: 50px auto;
    width: 90%;
    max-width: 700px;
  }

  .title {
    font-size: 22px;
  }

  .blue {
    color: #035e7f;
  }

  .grey {
    color: #a1a1a1;
  }

  .btn {
    background: #035e7f;
    padding: 5px 20px;
    color: white;
    border-radius: 5px;
    margin-bottom: 20px;
  }
  </style>
</head>

<body>
  <div class="mail">
    <div class="mail-container">
      <p><img src="{{asset('images/header-mail.jpg')}}" alt="Senai Flex" style="width:100%"></p>
      <p>
        <img src="{{asset('images/titulo-email.jpg')}}" alt="Senai Flex" style="width:100%">
      </p>
      <h1 class="title blue">Seu cadastro foi concluído com sucesso.</h1><br>
      <p>O <strong>Senai Flex</strong> permite escolher a dedo (e com alguns cliques) os melhores programas de
        desenvolvimento profissional para seus colaboradores e sua indústria.</p>
      <p>Da operação de máquinas complexas à gestão de projetos, de técnicas de negociação a procedimentos de inspeção e
        manutenção. Seja qual for a sua demanda, são criados e ofertados cursos exclusivos com aulas presenciais para
        tornar sua empresa mais produtiva e competitiva.</p>
      <h3><strong class="blue">TÁ FÁCIL, NÃO É MESMO?</strong></h3>
      <hr>
      <p>Acesse a plataforma com seu login (CNPJ) e senha cadastrada, assista ao vídeo tutorial e escolha
        entre opções que podem <b>transformar o presente e o futuro da sua indústria.</b></p>
      <a href="{{$siteUrl}}" style="color:white;" class="btn">ACESSE AQUI.</a>
      <p class="grey">Atenciosamente,<br>Equipe Senai Flex</p>
    </div>
  </div>
</body>

</html>