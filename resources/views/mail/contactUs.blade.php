<html>

<body style="background-color: #035e7f;">
    <div
        style="display: flex; width: 100%; background-color: #035e7f; background: linear-gradient(180deg, #faa625 200px, #035e7f 200px); padding-bottom: 200px; text-align: left;">
        <div style="background: white; padding: 60px; margin: 0 auto; width: 700px; margin-top: 50px;">
            <!-- p><img src="http://ec2-18-228-189-213.sa-east-1.compute.amazonaws.com/images/header-mail.jpg" border="0" -->
            <p><img src="{{asset('images/header-mail.jpg')}}" border="0"
                    alt="Senai Flex" style="width:100%"></p>
            <!-- p><img src="http://ec2-18-228-189-213.sa-east-1.compute.amazonaws.com/images/contactUsEmail.png" border="0" -->
            <p><img src="{{asset('images/contactUsEmail.png')}}" border="0"
            alt="Senai Flex" style="width:50%"></p>
            <p style="font-family: 'Source Sans Pro', sans-serif; font-size: 18px;"><b>CNPJ:</b> {{ $data['cnpj'] }}</p>
            <p style="font-family: 'Source Sans Pro', sans-serif; font-size: 18px;"><b>Empresa:</b> {{ $data['nameBusiness'] }}
            </p>
            <p style="font-family: 'Source Sans Pro', sans-serif; font-size: 18px;"><b>Cidade:</b> {{ $data['cityBusiness'] }}</p>
            <p style="font-family: 'Source Sans Pro', sans-serif; font-size: 18px;"><b>Telefone:</b>
                {{ $data['telephoneBusiness'] }}</p>
            <p style="font-family: 'Source Sans Pro', sans-serif; font-size: 18px;"><b>Remetente:</b> {{ $data['name'] }}</p>
            <p style="font-family: 'Source Sans Pro', sans-serif; font-size: 18px;"><b>E-mail Remetente:</b> {{ $data['emailContact'] }}</p>
            <p style="font-family: 'Source Sans Pro', sans-serif; font-size: 18px;"><b>Telefone Remetente:</b> {{ $data['telefoneContact'] }}</p>
            <div
                style="border-style: groove; border-width: 1px 1px 1px 1px; border-color: #004a70; border-radius: 10px; max-width: 750px; max-height: 300px; padding: 15px; overflow: hidden; word-wrap: break-word;">
                <span style="font-family: 'Source Sans Pro', sans-serif; font-size: 18px;"><b>Mensagem</b></span>
                <p style="position: relative; padding: 5px; font-family: 'Source Sans Pro', sans-serif; font-size: 18px;">
                    {{ $data['message'] }}</p>
            </div>
        </div>
    </div>
</body>

</html>