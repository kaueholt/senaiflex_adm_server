<html>

<body style="background-color: #035e7f;">
    <div
        style=" display: flex; width: 100%; background-color: #035e7f; background: linear-gradient(180deg, #faa625 200px, #035e7f 200px); padding-bottom: 200px; text-align: center">
        <div style="background: white; padding: 40px; margin: 0 auto; width: 700px; margin-top: 50px;">
            <p>
                <img src="{{asset('images/header-mail.jpg')}}" alt="Senai Flex" style="width: 550px; height: 200px;" />
            </p>
            <p>
                <img src="{{asset('images/titulo-email-senha.jpg')}}" alt="Senai Flex"
                    style="width: 600px; height: 44px;" />
            </p>
            <h1 style="font-size: 22px; font-weight: 900; color: #035e7f;">Não tem problema.</h1>
            <p style="color: #000000; font-size: large;">Clique no link abaixo e recupere seu acesso.</p>
            <br /><br />
            <a href="{{$siteUrl}}"><b
                    style="background: #035e7f; padding: 10px 40px; color: #FFFFFF; font-size: 20px; font-weight: 900; margin-bottom: 20px; border-radius: 10px; text-decoration: none;">Alterar
                    senha</b></a>
            <p style="color: #a1a1a1; font-size: 16px;"><br /><br />Atenciosamente,<br />Equipe SENAI FLEX</p>
        </div>
    </div>
</body>

</html>