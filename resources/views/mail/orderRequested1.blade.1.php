<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Senai Flex</title>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
  <style>
  body {
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
  }

  ul {
    margin: 0;
  }

  .container {
    max-width: 800px;
    margin: 0 auto;
    background: white;
    padding: 10px;
  }

  .company {}

  .boxes {
    color: #035e7f;
    width: 100%;
  }

  .box {
    padding: 15px;
    list-style: inside;
    border-radius: 10px;
    font-weight: bold;
    border: 5px solid white;
    box-sizing: border-box;
  }

  .box--blue {
    background: #e0eaf0;
  }

  .box--orange {
    background: #faa625;
  }

  .box--dark-blue {
    background: #035e7f;
  }

  /*TABLE STYLES*/
  .table,
  .alert {
    color: white;
    width: 100%;
  }

  .table {
    border: 1px solid #7397aa;
    margin-top: 25px;
    margin-bottom: 25px;
    border-radius: 10px;
    padding: 10px;
  }

  .table__labels th {
    color: #7397aa;
  }

  th {
    color: #035e7f;
  }

  td {
    vertical-align: top;
  }
  </style>
</head>

<body>
  <div class="container">
    <!-- img src="http://ec2-18-228-189-213.sa-east-1.compute.amazonaws.com/images/header-bg.jpg" width="100%" -->
    <img src="images/header-bg.jpg" width="100%"
      alt="Senai Flex" />
    <div class="company">
      <p><b>Cpnj</b>: 00.000.000/0001-00</p>
      <p><b>Empresa</b>: 00.000.000/0001-00</p>
      <p><b>Cidade</b>: 00.000.000/0001-00</p>
    </div>
    <table class="boxes">
      <tr>
        <th>Necessidade</th>
        <th>Área</th>
        <th>Cursos utilizados como base</th>
      </tr>
      <tr>
        <td>
          <div class="box box--orange">{{$order['necessidade']}}</div>
        </td>
        <td>
          <div class="box box--blue">{{$order['areaTecnologica']}}</div>
        </td>
        <td width="30%">
          <ul class="box box--blue">
          <!--@foreach-->
              <li>{{$order['cursosBase']}}</li>
          <!--@endforeach-->
          </ul>
        </td>
      </tr>
    </table>
    <!-- TABELA DE CONTEÚDOS -->
    <table class="table" style="page-break-after: auto;">
      <tr>
        <th>Conteúdo</th>
        <th colspan="3" align="right">Carga horária e valor</th>
      </tr>
      <tr class="table__labels">
        <th>-</th>
        <th>Básico</th>
        <th>Intermediário</th>
        <th>Avançado</th>
      </tr>

     <!--@foreach($order['conteudos'])-->
        <tr>
          <td width="40%">
            <div class="box box--dark-blue"> $content->descricao </div>
          </td>
          <td>
            <div class="box box--dark-blue"> 30/R$1000 </div>
          </td>
          <td>
            <div class="box box--dark-blue"> 1h/2000 </div>
          </td>
          <td>
            <div class="box box--dark-blue"> 3h/50000 </div>
          </td>
        </tr>
     <!--@endforeach-->

    </table>
    <!-- TABELA DE CONTEÚDOS NÃO LISTADOS -->
    <table class="table" style="page-break-after: auto;">
      <tr>
        <th>Conteúdo não listado</th>
        <th>Justificativa</th>
        <th nowrap>Carga horária</th>
      </tr>
      <tr>
        <td width="40%">
          <div class="box box--dark-blue">Conteúdo</div>
        </td>
        <td>
          <div class="box box--dark-blue"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis veniam natus
            consequuntur ex dolores at eius adipisci provident perferendis temporibus. </div>
        </td>
        <td>
          <div class="box box--dark-blue">1h</div>
        </td>
      </tr>
    </table>
    <table class="boxes" style="page-break-after: auto;">
      <tr>
        <th>Local</th>
        <th>Quantidade</th>
        <th>Precificação</th>
      </tr>
      <tr>
        <td>
          <div class="box box--blue">{{$order['unidade']}}</div>
        </td>
        <td>
          <div class="box box--blue">
            <ul>
              <li>
                <b>{{ $order['numProfissionais'] }}</b> Número de profissionais capacitados </li>
              <li>
                <b>{{ $order['videoQtdDesejada'] }}</b> Quantidade desejada de vídeos</li>
              <li>
                <b>{{ $order['cursoCargaHoraria'] }}</b>Carga horária escolhida </li>
              <li>
                <b>{{ $order['cursoCargaHoraria'] }}</b>Carga horária total* </li>
            </ul>
          </div>
        </td>
        <td width="30%">
          <div class="box box--orange">
            <p>
              <b>R${{ $order['price'] }}</b> Estimativa das aulas presenciais </p>
              <!--<b>R$12.00</b> Estimativa das aulas presenciais </p>-->
            <p>
              <b>R${{ $order['valorVideo']['valorVideo'] }}</b> Estimativa dos vídeos </p>
              <!--<b>R$152.55</b> Estimativa dos vídeos </p>-->
          </div>
        </td>
      </tr>
    </table>
    <!-- alert -->
    <div class="box box--dark-blue alert">Caso algum dos itens tenha ficado sem precificação, confirme a solicitação e
      nossa equipe enterá em contato. Os valores mencionados na proposta são estimados e poderão sofrer alterações.
    </div>
  </div>
</body>

</html>
