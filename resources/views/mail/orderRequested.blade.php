<html>
<head>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Roboto');
    </style>
</head>

<body style="font-family: 'Roboto', sans-serif; font-size: 14px;">
    <div style="max-width: 800px; margin: 0 auto; background: white; padding: 10px;">
        <!-- img src="http://ec2-18-228-189-213.sa-east-1.compute.amazonaws.com/images/header-bg.jpg" border="0" -->
        <img src="{{asset('images/header-bg.jpg')}}" border="0"
            width="100%" alt="Senai Flex" />
        <table style="width: 100%; color: #035e7f; width: 100%;"> 
            <tr>
                <td>
                    <div style="min-height: 100px; padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold; border: 5px solid white; box-sizing: border-box; background: #e0eaf0;">
                        <p>Cpnj: {{$order['user']['cnpj']}}</p>
                        <p>Empresa: {{$order['user']['name']}}</p>
                        <p>Cidade: {{$order['user']['city']}}</p>
                    </div>
                </td>
                <td>
                    <div style="min-height: 100px; padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold; border: 5px solid white; box-sizing: border-box; background: #e0eaf0;">
                        <p>Nome do Contato: {{$order['contatoNome']}}</p>
                        <p>Telefone: {{$order['contatoTelefone']}}</p>
                        <p>E-mail: {{$order['contatoEmail']}}</p>
                    </div>
                </td>
            </tr>
        </table>
        <table style="color: #035e7f; width: 100%;">
            <tr>
                <th style="color: #035e7f;">Necessidade</th>
                <th style="color: #035e7f;">Área</th>
                <th style="color: #035e7f;">Cursos utilizados como base</th>
            </tr>
            <tr>
                <td style="vertical-align: top;">
                    <div
                        style="padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold;border: 5px solid white; box-sizing: border-box; background: #faa625;">
                        {{$order['necessidade']}}</div>
                </td>
                <td style="vertical-align: top;">
                    <div
                        style="padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold;border: 5px solid white; box-sizing: border-box; background: #e0eaf0;">
                        {{$order['areaTecnologica']['descricao']}}</div>
                </td>
                <td width="30%" style="vertical-align: top;">
                    <ul
                        style="padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold;border: 5px solid white; box-sizing: border-box; background: #e0eaf0;">
                        @foreach ($order['cursosBase'] as $course)
                        <li>{{ $course['descricao'] }}</li>
                        @endforeach
                    </ul>
                </td>
            </tr>
        </table>

        <table width="100%"
            style="border: 1px solid #7397aa; margin-top: 25px; margin-bottom: 25px; border-radius: 10px; padding: 10px; color: #7397aa;">
            <tr>
                <th width="50%" style="color: #035e7f;">Conteúdo</th>
                <th style="color: #035e7f;">Carga Horária</th>
                <th style="color: #035e7f;">Valor</th>
            </tr>

            @foreach ($order['conteudos'] as $content)
            <tr>
                <td width="50%" style="vertical-align: top;">
                    <div
                        style="padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold;border: 5px solid white; box-sizing: border-box; background: #035e7f;">
                        {{ $content['nome'] }}</div>
                </td>
                <td style="vertical-align: top;">
                    <div
                        style="padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold;border: 5px solid white; box-sizing: border-box; background: #035e7f;">
                        {{ $content['duracaoFormatado'] }} </div>
                </td>
                <td style="vertical-align: top;">
                    <div
                        style="padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold;border: 5px solid white; box-sizing: border-box; background: #035e7f;">
                        R$ {{ $content['precoTotal'] }} </div>
                </td>
            </tr>
            @endforeach

        </table>

        @if(!empty($order['cursosSolicitados']))
        <table width="100%"
            style="page-break-after: auto; border: 1px solid #7397aa; margin-top: 25px; margin-bottom: 25px; border-radius: 10px; padding: 10px; color: #7397aa;">
            <tr>
                <th style="color: #035e7f;">Conteúdo não listado</th>
                <th style="color: #035e7f;">Justificativa</th>
                <th nowrap style="color: #035e7f;">Carga horária</th>
            </tr>
            @foreach ($order['cursosSolicitados'] as $single)
            <tr>
                <td style="vertical-align: top;">
                    <div
                        style="padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold;border: 5px solid white; box-sizing: border-box; background: #035e7f;">
                        {{ $single['conteudo'] }}</div>
                </td>
                <td style="vertical-align: top;">
                    <div
                        style="padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold;border: 5px solid white; box-sizing: border-box; background: #035e7f;">
                        {{ $single['justificativa'] }}</div>
                </td>
                <td style="vertical-align: top;">
                    <div
                        style="padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold;border: 5px solid white; box-sizing: border-box; background: #035e7f;">
                        {{ $single['cargaHoraria'] }}</div>
                </td>
            </tr>
            @endforeach
        </table>
        @endif

        <table style="page-break-after: auto; color: #035e7f; width: 100%;">
            <tr>
                <th style="color: #035e7f;">Local</th>
                <th style="color: #035e7f;">Quantidade</th>
                <th style="color: #035e7f;">Precificação</th>
            </tr>
            <tr>
                <td style="vertical-align: top;">
                    <div
                        style="height: 130px; padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold; border: 5px solid white; box-sizing: border-box; background: #e0eaf0;">
                        {{ $order['unidade']['nome'] }}</div>
                </td>
                <td style="vertical-align: top;">
                    <div>
                        <ul style="height: 130px; padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold; border: 5px solid white; box-sizing: border-box; background: #e0eaf0;">
                            <li>
                                <span>{{ $order['numProfissionais'] }} -</span><span>Número de
                                    profissionais
                                </span></li>
                            <li>
                                <span>{{ $order['videoQtdDesejada'] }} -</span><span>Quantidade desejada
                                    de vídeos</span>
                            </li>
                            <li>
                                <span>{{ $order['cursoCargaHoraria'] }} - </span><span>Carga horária
                                    escolhida</span></li>
                            <li>
                                <span>{{ $order['totalCargaHoraria'] }} -</span><span>Carga horária total 
                                @if(!empty($order['extra']))*@endif  </span>
                            </li>
                        </ul>
                    </div>
                </td>
                <td style="vertical-align: top;"  width="30%">
                    <div
                        style="min-height: 130px; padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold; border: 5px solid white; box-sizing: border-box; background: #faa625;">
                        <p>
                            R$ {{ $order['price'] }} - Estimativa das aulas presenciais </p>
                        <p>
                            R$ {{ $order['totalValorVideos']}} - Estimativa dos vídeos </p>
                    </div>
                </td>
            </tr>
        </table>
        @if(!empty($order['extra']))
        <div>
            <p>*Seu curso conterá uma atividade completementar de 30 minutos sem custo adicional</p>
        </div>
        @endif  

        <div
            style="padding: 15px; list-style: inside; border-radius: 10px; font-weight: bold; border: 5px solid white; box-sizing: border-box; background: #035e7f; color: white; width: 100%;">
            Caso algum dos itens tenha ficado sem precificação, confirme a solicitação
            e
            nossa equipe enterá em contato. Os valores mencionados na proposta são estimados e poderão sofrer
            alterações.
        </div>
    </div>
</body>

</html>