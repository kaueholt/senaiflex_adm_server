<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Senai Flex</title>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
  <style>
  body {
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
  }

  ul {
    margin: 0;
  }

  .container {
    max-width: 800px;
    margin: 0 auto;
    background: white;
    padding: 10px;
  }

  .company {}

  .boxes {
    color: #035e7f;
    width: 100%;
  }

  .box {
    padding: 15px;
    list-style: inside;
    border-radius: 10px;
    font-weight: bold;
    border: 5px solid white;
    box-sizing: border-box;
  }

  .box--blue {
    background: #e0eaf0;
  }

  .box--orange {
    background: #faa625;
  }

  .box--dark-blue {
    background: #035e7f;
  }

  .box-align {
    display: inline;
  }

  /*TABLE STYLES*/
  .table,
  .alert {
    color: white;
    width: 100%;
  }

  .table {
    border: 1px solid #7397aa;
    margin-top: 25px;
    margin-bottom: 25px;
    border-radius: 10px;
    padding: 10px;
  }

  .table__labels th {
    color: #7397aa;
  }

  th {
    color: #035e7f;
  }

  td {
    vertical-align: top;
  }

  .btn-total {
      background: #004a70;
      color: #faa625;
      width: 100%;
      padding: 5px 15px;
      width: 100%;
      border-radius: 5px;
      font-size: 1.4rem;
      font-weight: bolder;
      display: block;
      text-align: center;
  }

  .curse-set {
    min-height: 100px;
  }

  .total-result {
    height: 130px;
  }

  .total-curse {
      position: relative;
      width: 300px;
      left: 400px;
  }
  </style>
</head>

<body>
  <div class="container">
    <!-- img src="http://ec2-18-228-189-213.sa-east-1.compute.amazonaws.com/images/header-bg.jpg" width="100%" -->
    <img src="{{asset('images/header-bg.jpg')}}" width="100%"
      alt="Senai Flex" />
    <!-- <div class="company">
      <p><b>Cpnj</b>: {{$order['user']['cnpj']}}</p>
      <p><b>Empresa</b>: {{$order['user']['name']}}</p>
      <p><b>Cidade</b>: {{$order['user']['city']}}</p>
    </div> -->

  <table class="boxes" style="width: 100%"> 
    <tr>
        <td>
          <div class="curse-set box box--blue">
              <p>Cpnj: {{$order['user']['cnpj']}}</p>
              <p>Empresa: {{$order['user']['name']}}</p>
              <p>Cidade: {{$order['user']['city']}}</p>
          </div>
        </td>
        <td>
          <div class="curse-set box box--blue">
              <p>Nome do Contato: {{$order['contatoNome']}}</p>
              <p>Telefone: {{$order['contatoFone']}}</p>
              <p>E-mail: {{$order['contatoEmail']}}</p>
          </div>
        </td>
    </tr>
  </table>
    
    <table class="boxes">
      <tr>
        <th>Necessidade</th>
        <th>Área</th>
        <th>Cursos utilizados como base</th>
      </tr>
      <tr>
        <td>
          <div class="curse-set box box--orange">{{$order['necessidade']}}</div>
        </td>
        <td>
          <div class="curse-set box box--blue">{{$order['areaTecnologica']['descricao']}}</div>
        </td>
        <td width="30%">
          <ul class="curse-set box box--blue">
          @foreach ($order['cursosBase'] as $course)
            <li>{{ $course['descricao'] }}</li>
          @endforeach          
          </ul>
        </td>
      </tr>
    </table>
    <!-- TABELA DE CONTEÚDOS -->
    <table class="table" style="page-break-after: auto;">
      <tr>
        <th width="50%">Conteúdo</th>
        <th>Carga Horária</th>
        <th>Valor</th>
      </tr>

      @foreach ($order['conteudos'] as $content)
        <tr>
          <td width="50%">
            <div class="box box--dark-blue"> {{ $content['nome'] }}</div>
          </td>
          <td>
            <div class="box box--dark-blue"> {{ $content['duracaoFormatado'] }} </div>
          </td>
          <td>
            <div class="box box--dark-blue"> R$ {{ $content['valor'] }} </div>
          </td>
        </tr>
     @endforeach

    </table>
    <!-- TABELA DE CONTEÚDOS NÃO LISTADOS -->
    @if(!empty($order['cursosSolicitados']))
    <table class="table" style="page-break-after: auto;">
      <tr>
        <th>Conteúdo não listado</th>
        <th>Justificativa</th>
        <th nowrap>Carga horária</th>
      </tr>
      @foreach ($order['cursosSolicitados'] as $single)
        <tr>
          <td width="40%">
            <div class="box box--dark-blue"> {{ $single['conteudo'] }}</div>
          </td>
          <td>
            <div class="box box--dark-blue"> {{ $single['justificativa'] }}</div>
          </td>
          <td>
            <div class="box box--dark-blue"> {{ $single['cargaHoraria'] }}</div>
          </td>
        </tr>
     @endforeach
     </table>
     @endif
    <!-- OUTRAS INFORMAÇÕES -->
    <table class="boxes" style="page-break-after: auto;">
      <tr>
        <th>Local</th>
        <th>Quantidade</th>
        <th>Precificação</th>
      </tr>
      <tr>
        <td>
          <div class="total-result box box--blue">{{ $order['unidade']['nome'] }}</div>
        </td>
        <td>
          <div>
            <ul class="total-result box box--blue">
              <li>
                <span><b>{{ $order['numProfissionais'] }} -</b></span> <span> Número de profissionais capacitados</span></li>
              <li>
                <span><b>{{ $order['videoQtdDesejada'] }} -</b></span> <span> Quantidade desejada de vídeos</span></li>
              <li>
                <span><b>{{ $order['cursoCargaHoraria'] }} -</b></span> <span> Carga horária escolhida</span></li>
              <li>
                <span><b>{{ $order['totalCargaHoraria'] }} -</b></span> <span> Carga horária total
                @if(!empty($order['extra']))*@endif  
                </span> </li>
            </ul>
          </div>
        </td>
        <td width="30%">
          <div class="total-result box box--orange">
            <p>Estimativa das aulas presenciais </p> <b>R$ {{ $order['price'] }}</b>
            <p>Estimativa dos vídeos </p> <b>R$ {{ $order['totalValorVideos'] }}</b>
          </div>
        </td>
      </tr>
    </table>
    @if(!empty($order['extra']))
      <div>
          <p>*Seu curso conterá uma atividade completementar de 30 minutos sem custo adicional</p>
      </div>
    @endif  
    <div class="total-curse">
        <p class="btn-total">Valor total: R$ {{ $order['totalGeral'] }}</p>
    </div>

    <!-- alert -->
    <div class="box box--dark-blue alert">Caso algum dos itens tenha ficado sem precificação, confirme a solicitação e
      nossa equipe enterá em contato. Os valores mencionados na proposta são estimados e poderão sofrer alterações.
    </div>
  </div>
</body>

</html>
