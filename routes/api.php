<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {
    // Rotas dos países
    Route::get('/pais', 'PaisController@index');
    Route::get('/pais/{id}', 'PaisController@show');
    Route::post('/pais', 'PaisController@store');
    Route::put('/pais/{id}', 'PaisController@update');
    Route::delete('/pais/{id}', 'PaisController@destroy');

    // Rotas dos estados
    Route::get('/uf', 'UfController@index');
    Route::get('/uf/{uf}', 'UfController@show');
    Route::post('/uf', 'UfController@store');
    Route::put('/uf/{uf}', 'UfController@update');
    Route::delete('/uf/{uf}', 'UfController@destroy');

    // Rotas das cidades
    Route::get('/cidade', 'CidadeController@index');
    Route::get('/cidade/unpaginated', 'CidadeController@indexUnpaginated');
    Route::get('/cidade/search', 'CidadeController@findBySearch');
    Route::get('/cidade/name/{cityName}', 'CidadeController@getByName');
    Route::get('/cidade/{id}', 'CidadeController@show');
    Route::post('/cidade', 'CidadeController@store');
    Route::put('/cidade/{id}', 'CidadeController@update');
    Route::delete('/cidade/{id}', 'CidadeController@destroy');
    Route::delete('/cidade/paginated/{id}', 'CidadeController@destroyPaginated');

    // Rotas dos areas
    Route::get('/area', 'AreaController@index');
    Route::get('/area/{id}', 'AreaController@show');
    Route::post('/area', 'AreaController@store');
    Route::put('/area/{id}', 'AreaController@update');
    Route::delete('/area/{id}', 'AreaController@destroy');

    // Rotas dos cursos
    Route::get('/curso', 'CursoController@index');
    Route::get('/curso/unpaginated', 'CursoController@indexUnpaginated');
    Route::get('/curso/search', 'CursoController@findBySearch');
    Route::get('/curso/{id}', 'CursoController@show');
    Route::get('/curso/area/{areaId}', 'CursoController@getByArea');
    Route::post('/curso', 'CursoController@store');
    Route::put('/curso/{id}', 'CursoController@update');
    Route::delete('/curso/{id}', 'CursoController@destroy');
    Route::delete('/curso/paginated/{id}', 'CursoController@destroyPaginated');
    
    // Rotas dos conteudo
    Route::get('/conteudo', 'ConteudoController@index');
    Route::get('/conteudo/unpaginated', 'ConteudoController@indexUnpaginated');
    Route::get('/conteudo/{id}', 'ConteudoController@show');
    Route::get('/conteudo/curso/{cursoId}', 'ConteudoController@getByCurso');
    Route::post('/conteudo', 'ConteudoController@store');
    Route::put('/conteudo/{id}', 'ConteudoController@update');
    Route::delete('/conteudo/{id}', 'ConteudoController@destroy');
    Route::delete('/conteudo/paginated/{id}', 'ConteudoController@destroyPaginated');

    // Rotas dos categoria
    Route::get('/categoria', 'CategoriaController@index');
    Route::get('/categoria/{id}', 'CategoriaController@show');
    Route::post('/categoria', 'CategoriaController@store');
    Route::put('/categoria/{id}', 'CategoriaController@update');
    Route::delete('/categoria/{id}', 'CategoriaController@destroy');

    // Rotas dos categoriaXconteudo
    Route::get('/categoriaConteudo', 'CategoriaConteudoController@index');
    Route::get('/categoriaConteudo/{id}', 'CategoriaConteudoController@show');
    Route::post('/categoriaConteudo', 'CategoriaConteudoController@store');
    Route::put('/categoriaConteudo/{id}', 'CategoriaConteudoController@update');
    Route::delete('/categoriaConteudo/{id}', 'CategoriaConteudoController@destroy');

    // Rotas das videoMinimoMaximo
    Route::get('/videoMinimoMaximo', 'VideoMinimoMaximoController@index');
    Route::get('/videoMinimoMaximo/{id}', 'VideoMinimoMaximoController@show');
    Route::get('/videoMinimoMaximo/carga-horaria/{ch}', 'VideoMinimoMaximoController@showByWorload');
    Route::post('/videoMinimoMaximo', 'VideoMinimoMaximoController@store');
    Route::put('/videoMinimoMaximo/{id}', 'VideoMinimoMaximoController@update');
    Route::delete('/videoMinimoMaximo/{id}', 'VideoMinimoMaximoController@destroy');

    // Rotas das videoQuantidadeMaxima
    Route::get('/videoQuantidadeMaxima', 'VideoQuantidadeMaximaController@index');
    Route::get('/videoQuantidadeMaxima/{id}', 'VideoQuantidadeMaximaController@show');
    Route::post('/videoQuantidadeMaxima', 'VideoQuantidadeMaximaController@store');
    Route::put('/videoQuantidadeMaxima/{id}', 'VideoQuantidadeMaximaController@update');
    Route::delete('/videoQuantidadeMaxima/{id}', 'VideoQuantidadeMaximaController@destroy');

    // Rotas das videoDelimitador
    Route::get('/videoDelimitador', 'VideoDelimitadorController@index');
    Route::get('/videoDelimitador/{id}', 'VideoDelimitadorController@show');
    Route::post('/videoDelimitador', 'VideoDelimitadorController@store');
    Route::put('/videoDelimitador/{id}', 'VideoDelimitadorController@update');
    Route::delete('/videoDelimitador/{id}', 'VideoDelimitadorController@destroy');

    // Rotas das videoValor
    Route::get('/videoValor', 'VideoValorController@index');
    Route::get('/videoValor/{id}', 'VideoValorController@show');
    Route::post('/videoValor', 'VideoValorController@store');
    Route::put('/videoValor/{id}', 'VideoValorController@update');
    Route::delete('/videoValor/{id}', 'VideoValorController@destroy');

    // Rotas das regiões
    Route::get('/regiao', 'RegiaoController@index');
    Route::get('/regiao/{id}', 'RegiaoController@show');
    Route::post('/regiao', 'RegiaoController@store');
    Route::put('/regiao/{id}', 'RegiaoController@update');
    Route::delete('/regiao/{id}', 'RegiaoController@destroy');

    // Rotas das micro regiões
    Route::get('/micro-regiao', 'MicroRegiaoController@index');
    Route::get('/micro-regiao/{id}', 'MicroRegiaoController@show');
    Route::post('/micro-regiao', 'MicroRegiaoController@store');
    Route::put('/micro-regiao/{id}', 'MicroRegiaoController@update');
    Route::delete('/micro-regiao/{id}', 'MicroRegiaoController@destroy');

    // Rotas das sedes
    Route::get('/sede', 'SedeController@index');
    Route::get('/sede/{id}', 'SedeController@show');
    Route::post('/sede', 'SedeController@store');
    Route::put('/sede/{id}', 'SedeController@update');
    Route::delete('/sede/{id}', 'SedeController@destroy');

    // Rotas das coordenadores
    Route::get('/coordenador', 'CoordenadorController@index');
    Route::get('/coordenador/{id}', 'CoordenadorController@show');
    Route::post('/coordenador', 'CoordenadorController@store');
    Route::put('/coordenador/{id}', 'CoordenadorController@update');
    Route::delete('/coordenador/{id}', 'CoordenadorController@destroy');

	// Rotas das coordenadores
	Route::get('/coordenador-admin', 'CoordenadorAdminController@index');
	Route::get('/coordenador-admin/{id}', 'CoordenadorAdminController@show');
    Route::post('/coordenador-admin', 'CoordenadorAdminController@store');
    Route::put('/coordenador-admin/{id}', 'CoordenadorAdminController@update');
    Route::delete('/coordenador-admin/{id}', 'CoordenadorAdminController@destroy');

    // Rotas das vendedores
    Route::get('/vendedor', 'VendedorController@index');
    Route::get('/vendedor/{id}', 'VendedorController@show');
    Route::post('/vendedor', 'VendedorController@store');
    Route::put('/vendedor/{id}', 'VendedorController@update');
    Route::delete('/vendedor/{id}', 'VendedorController@destroy');

    // Rotas das vendedores
    Route::get('/vendedor-admin', 'VendedorAdminController@index');
    Route::get('/vendedor-admin/{id}', 'VendedorController@show');
    Route::post('/vendedor-admin', 'VendedorController@store');
    Route::put('/vendedor-admin/{id}', 'VendedorController@update');
    Route::delete('/vendedor-admin/{id}', 'VendedorController@destroy');

    // Rotas das industrias
    Route::get('/industria', 'IndustriaController@index');
    Route::get('/industria/search', 'IndustriaController@findBySearch');
    Route::get('/industria/search_2', 'IndustriaController@findBySearch_2');  
    Route::get('/industria/{id}', 'IndustriaController@show');
    Route::post('/industria', 'IndustriaController@store');
    Route::put('/industria/{id}', 'IndustriaController@update');
    Route::delete('/industria/{id}', 'IndustriaController@destroy');

    // Rotas das pessoas
    Route::get('/pessoa-admin', 'PessoaAdminController@index');
    Route::get('/pessoa-admin/unpaginated', 'PessoaAdminController@indexUnpaginated');
    Route::get('/pessoa-admin/search', 'PessoaAdminController@findBySearch');
    Route::get('/pessoa-admin/info', 'UserAuthController@infoUser');
    Route::get('/pessoa-admin/{id}', 'PessoaAdminController@show');
    Route::get('/pessoa-admin/cnpjcpf/{cnpjCpf}', 'PessoaAdminController@findByCnpjCpf');
    Route::post('/pessoa-admin/admin', 'PessoaAdminController@storeAdmin');
    Route::post('/pessoa-admin/updateAdmin', 'PessoaAdminController@updateAdmin');
    Route::post('/pessoa-admin', 'PessoaAdminController@store');
    Route::put('/pessoa-admin/{id}', 'PessoaAdminController@update');
    Route::put('/pessoa-admin/admin/{id}', 'PessoaAdminController@update');
    Route::put('/pessoa-admin/remover-nova-conta/{id}', 'PessoaAdminController@removeNewAccount');
    Route::delete('/pessoa-admin/{id}', 'PessoaAdminController@destroy');
    Route::delete('/pessoa-admin/paginated/{id}', 'PessoaAdminController@destroyPaginated');

	// Rotas das pessoas
    Route::get('/pessoa', 'PessoaController@index');
    Route::get('/pessoa/search', 'PessoaController@findBySearch');
    Route::get('/pessoa/info', 'UserAuthController@infoUser');
    Route::get('/pessoa/{id}', 'PessoaController@show');
    Route::get('/pessoa/cnpjcpf/{cnpjCpf}', 'PessoaController@findByCnpjCpf');
    Route::post('/pessoa/admin', 'PessoaController@storeAdmin');
    Route::post('/pessoa', 'PessoaController@store');
    Route::put('/pessoa/{id}', 'PessoaController@update');
    Route::put('/pessoa/admin/{id}', 'PessoaController@update');
    Route::put('/pessoa/remover-nova-conta/{id}', 'PessoaController@removeNewAccount');
    Route::delete('/pessoa/{id}', 'PessoaController@destroy');

    // Rotas das proposta
    Route::get('/proposta', 'PropostaController@index');
    Route::get('/proposta/industria/{id}', 'PropostaController@showByIndustria');
    Route::get('/proposta/param', 'PropostaController@showByParam');
    Route::get('/proposta/{id}', 'PropostaController@show');
    Route::post('/proposta', 'PropostaController@store');
    Route::put('/proposta/{id}', 'PropostaController@update');
    Route::delete('/proposta/{id}', 'PropostaController@destroy');
    //  Exportar proposta para PDF
    Route::get('/proposta/export-pdf/{id}', 'PropostaController@exportToPDF');

	// Rotas das proposta
    Route::get('/proposta-admin', 'PropostaAdminController@index');
    Route::get('/proposta-admin/unpaginated', 'PropostaAdminController@indexUnpaginated');
    Route::get('/proposta-admin/industria/{id}', 'PropostaAdminController@showByIndustria');
    Route::get('/proposta-admin/param', 'PropostaAdminController@showByParam');
    Route::get('/proposta-admin/{id}', 'PropostaAdminController@show');
    Route::post('/proposta-admin', 'PropostaAdminController@store');
    Route::put('/proposta-admin/{id}', 'PropostaAdminController@update');
    Route::delete('/proposta-admin/{id}', 'PropostaAdminController@destroy');
    //  Exportar proposta para PDF
    Route::get('/proposta-admin/export-pdf/{id}', 'PropostaAdminController@exportToPDF');

    // Rotas das propostaCurso
    Route::get('/propostaCurso', 'PropostaCursoController@index');
    Route::get('/propostaCurso/{id}', 'PropostaCursoController@show');
    Route::post('/propostaCurso', 'PropostaCursoController@store');
    Route::put('/propostaCurso/{id}', 'PropostaCursoController@update');
    Route::delete('/propostaCurso/{id}', 'PropostaCursoController@destroy');

    // Rotas das propostaConteudo
    Route::get('/propostaConteudo', 'PropostaConteudoController@index');
    Route::get('/propostaConteudo/{id}', 'PropostaConteudoController@show');
    Route::post('/propostaConteudo', 'PropostaConteudoController@store');
    Route::put('/propostaConteudo/{id}', 'PropostaConteudoController@update');
    Route::delete('/propostaConteudo/{id}', 'PropostaConteudoController@destroy');

    // Rotas das propostaConteudoAvulso
    Route::get('/propostaConteudoAvulso', 'PropostaConteudoAvulsoController@index');
    Route::get('/propostaConteudoAvulso/{id}', 'PropostaConteudoAvulsoController@show');
    Route::post('/propostaConteudoAvulso', 'PropostaConteudoAvulsoController@store');
    Route::put('/propostaConteudoAvulso/{id}', 'PropostaConteudoAvulsoController@update');
    Route::delete('/propostaConteudoAvulso/{id}', 'PropostaConteudoAvulsoController@destroy');

    //Disparo de e-mail
    Route::post('/mail/contact', 'MailRulesController@sendMailContactUs');

    // Rota para autentição
    Route::post('/auth/login', 'UserAuthController@Login');
    Route::post('/auth/login-admin', 'UserAuthController@loginAdmin');
    Route::post('/auth/reset-password', 'UserAuthController@resetPassword');
    Route::put('/auth/change-password', 'UserAuthController@changePassword');
    Route::put('/auth/info', 'UserAuthController@infoUser');

});
